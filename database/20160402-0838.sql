-- MySQL dump 10.13  Distrib 5.7.11, for Win64 (x86_64)
--
-- Host: localhost    Database: desaster_julian
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backend_layout`
--

DROP TABLE IF EXISTS `backend_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backend_layout` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `config` text NOT NULL,
  `icon` text NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_layout`
--

LOCK TABLES `backend_layout` WRITE;
/*!40000 ALTER TABLE `backend_layout` DISABLE KEYS */;
/*!40000 ALTER TABLE `backend_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_groups`
--

DROP TABLE IF EXISTS `be_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `be_groups` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL DEFAULT '',
  `non_exclude_fields` text,
  `explicit_allowdeny` text,
  `allowed_languages` varchar(255) NOT NULL DEFAULT '',
  `custom_options` text,
  `db_mountpoints` text,
  `pagetypes_select` varchar(255) NOT NULL DEFAULT '',
  `tables_select` text,
  `tables_modify` text,
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `groupMods` text,
  `file_mountpoints` text,
  `file_permissions` text,
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `description` varchar(2000) NOT NULL DEFAULT '',
  `lockToDomain` varchar(50) NOT NULL DEFAULT '',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `TSconfig` text,
  `subgroup` text,
  `hide_in_lists` tinyint(4) NOT NULL DEFAULT '0',
  `workspace_perms` tinyint(3) NOT NULL DEFAULT '1',
  `category_perms` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_groups`
--

LOCK TABLES `be_groups` WRITE;
/*!40000 ALTER TABLE `be_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `be_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_sessions`
--

DROP TABLE IF EXISTS `be_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `be_sessions` (
  `ses_id` varchar(32) NOT NULL DEFAULT '',
  `ses_name` varchar(32) NOT NULL DEFAULT '',
  `ses_iplock` varchar(39) NOT NULL DEFAULT '',
  `ses_hashlock` int(11) NOT NULL DEFAULT '0',
  `ses_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `ses_tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `ses_data` longtext,
  `ses_backuserid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ses_id`,`ses_name`),
  KEY `ses_tstamp` (`ses_tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_sessions`
--

LOCK TABLES `be_sessions` WRITE;
/*!40000 ALTER TABLE `be_sessions` DISABLE KEYS */;
INSERT INTO `be_sessions` VALUES ('371b59286534b5e27d566fda6def3972','be_typo_user','127.0.0.1',63645884,1,1459578981,'a:5:{s:26:\"formProtectionSessionToken\";s:64:\"9f5d8bad0943ed6afa5de8c947127258860df590f24c0a8b72b96b9dbc88f562\";s:27:\"core.template.flashMessages\";N;s:31:\"TYPO3\\CMS\\Recordlist\\RecordList\";a:1:{s:12:\"search_field\";N;}s:80:\"extbase.flashmessages.tx_extensionmanager_tools_extensionmanagerextensionmanager\";N;s:51:\"extbase.flashmessages.tx_realurl_web_realurlrealurl\";N;}',0);
/*!40000 ALTER TABLE `be_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_users`
--

DROP TABLE IF EXISTS `be_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `be_users` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(2000) NOT NULL DEFAULT '',
  `avatar` int(11) unsigned NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL DEFAULT '',
  `admin` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `usergroup` varchar(255) NOT NULL DEFAULT '',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` char(2) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `db_mountpoints` text,
  `options` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `realName` varchar(80) NOT NULL DEFAULT '',
  `userMods` text,
  `allowed_languages` varchar(255) NOT NULL DEFAULT '',
  `uc` mediumtext,
  `file_mountpoints` text,
  `file_permissions` text,
  `workspace_perms` tinyint(3) NOT NULL DEFAULT '1',
  `lockToDomain` varchar(50) NOT NULL DEFAULT '',
  `disableIPlock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `TSconfig` text,
  `lastlogin` int(10) unsigned NOT NULL DEFAULT '0',
  `createdByAction` int(11) NOT NULL DEFAULT '0',
  `usergroup_cached_list` text,
  `workspace_id` int(11) NOT NULL DEFAULT '0',
  `workspace_preview` tinyint(3) NOT NULL DEFAULT '1',
  `category_perms` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_users`
--

LOCK TABLES `be_users` WRITE;
/*!40000 ALTER TABLE `be_users` DISABLE KEYS */;
INSERT INTO `be_users` VALUES (1,0,1459573801,'admin','',0,'$P$CvqxIP2Gp.K.rlVChawkUKs0ONxyWA1',1,'',0,0,0,'','',NULL,0,1459573801,0,'',NULL,'','a:19:{s:14:\"interfaceSetup\";s:0:\"\";s:10:\"moduleData\";a:8:{s:10:\"web_layout\";a:2:{s:8:\"function\";s:1:\"0\";s:8:\"language\";s:1:\"0\";}s:8:\"web_list\";a:0:{}s:13:\"system_config\";a:2:{s:8:\"function\";s:1:\"0\";s:6:\"node_0\";a:0:{}}s:6:\"web_ts\";a:1:{s:8:\"function\";s:88:\"TYPO3\\CMS\\Tstemplate\\Controller\\TypoScriptTemplateConstantEditorModuleFunctionController\";}s:29:\"tx_realurl_web_realurlrealurl\";a:1:{s:10:\"controller\";s:8:\"Overview\";}s:57:\"TYPO3\\CMS\\Backend\\Utility\\BackendUtility::getUpdateSignal\";a:0:{}s:10:\"FormEngine\";a:2:{i:0;a:3:{s:32:\"cbf9fcb9646172755056b7c156c1c7ac\";a:4:{i:0;s:19:\"<em>[No title]</em>\";i:1;a:7:{s:4:\"edit\";a:1:{s:5:\"pages\";a:1:{i:1;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;s:24:\"editRegularContentFromId\";N;s:9:\"workspace\";N;}i:2;s:101:\"&edit[pages][1]=edit&defVals=&overrideVals=&columnsOnly=&noView=&editRegularContentFromId=&workspace=\";i:3;a:5:{s:5:\"table\";s:5:\"pages\";s:3:\"uid\";s:1:\"1\";s:3:\"pid\";s:1:\"0\";s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}}s:32:\"aed843b62800472404e9a9615083a929\";a:4:{i:0;s:17:\"www.juli-an.ch.lo\";i:1;a:7:{s:4:\"edit\";a:1:{s:10:\"sys_domain\";a:1:{i:1;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;s:24:\"editRegularContentFromId\";N;s:9:\"workspace\";N;}i:2;s:106:\"&edit[sys_domain][1]=edit&defVals=&overrideVals=&columnsOnly=&noView=&editRegularContentFromId=&workspace=\";i:3;a:5:{s:5:\"table\";s:10:\"sys_domain\";s:3:\"uid\";s:1:\"1\";s:3:\"pid\";s:1:\"1\";s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}}s:32:\"0eaa25358e9ceeb3d00af423a49c8b8a\";a:4:{i:0;s:18:\"Julian Artist Page\";i:1;a:7:{s:4:\"edit\";a:1:{s:12:\"sys_template\";a:1:{i:1;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;s:24:\"editRegularContentFromId\";N;s:9:\"workspace\";N;}i:2;s:108:\"&edit[sys_template][1]=edit&defVals=&overrideVals=&columnsOnly=&noView=&editRegularContentFromId=&workspace=\";i:3;a:5:{s:5:\"table\";s:12:\"sys_template\";s:3:\"uid\";s:1:\"1\";s:3:\"pid\";s:1:\"1\";s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}}}i:1;s:32:\"aed843b62800472404e9a9615083a929\";}s:42:\"TYPO3\\CMS\\Belog\\Controller\\ToolsController\";s:646:\"O:39:\"TYPO3\\CMS\\Belog\\Domain\\Model\\Constraint\":20:{s:14:\"\0*\0userOrGroup\";s:1:\"0\";s:9:\"\0*\0number\";i:500;s:15:\"\0*\0workspaceUid\";i:-99;s:12:\"\0*\0timeFrame\";i:0;s:9:\"\0*\0action\";i:0;s:14:\"\0*\0groupByPage\";b:0;s:17:\"\0*\0startTimestamp\";i:0;s:15:\"\0*\0endTimestamp\";i:0;s:18:\"\0*\0manualDateStart\";N;s:17:\"\0*\0manualDateStop\";N;s:18:\"\0*\0isInPageContext\";b:0;s:9:\"\0*\0pageId\";i:0;s:8:\"\0*\0depth\";i:0;s:6:\"\0*\0uid\";N;s:16:\"\0*\0_localizedUid\";N;s:15:\"\0*\0_languageUid\";N;s:16:\"\0*\0_versionedUid\";N;s:6:\"\0*\0pid\";N;s:61:\"\0TYPO3\\CMS\\Extbase\\DomainObject\\AbstractDomainObject\0_isClone\";b:0;s:69:\"\0TYPO3\\CMS\\Extbase\\DomainObject\\AbstractDomainObject\0_cleanProperties\";a:0:{}}\";}s:19:\"thumbnailsByDefault\";i:1;s:14:\"emailMeAtLogin\";i:0;s:11:\"startModule\";s:29:\"help_AboutmodulesAboutmodules\";s:18:\"hideSubmoduleIcons\";i:0;s:8:\"titleLen\";i:50;s:8:\"edit_RTE\";s:1:\"1\";s:20:\"edit_docModuleUpload\";s:1:\"1\";s:17:\"navFrameResizable\";i:0;s:15:\"resizeTextareas\";i:1;s:25:\"resizeTextareas_MaxHeight\";i:500;s:24:\"resizeTextareas_Flexible\";i:0;s:4:\"lang\";s:0:\"\";s:19:\"firstLoginTimeStamp\";i:1459573884;s:17:\"BackendComponents\";a:1:{s:6:\"States\";a:3:{s:17:\"typo3-module-menu\";O:8:\"stdClass\":1:{s:5:\"width\";i:242;}s:8:\"Pagetree\";O:8:\"stdClass\":1:{s:9:\"stateHash\";O:8:\"stdClass\":4:{s:1:\"0\";i:1;s:1:\"1\";i:1;s:4:\"root\";i:1;s:16:\"lastSelectedNode\";s:2:\"p1\";}}s:25:\"typo3-navigationContainer\";O:8:\"stdClass\":1:{s:5:\"width\";i:312;}}}s:15:\"moduleSessionID\";a:8:{s:10:\"web_layout\";s:32:\"371b59286534b5e27d566fda6def3972\";s:8:\"web_list\";s:32:\"371b59286534b5e27d566fda6def3972\";s:13:\"system_config\";s:32:\"371b59286534b5e27d566fda6def3972\";s:6:\"web_ts\";s:32:\"371b59286534b5e27d566fda6def3972\";s:29:\"tx_realurl_web_realurlrealurl\";s:32:\"371b59286534b5e27d566fda6def3972\";s:57:\"TYPO3\\CMS\\Backend\\Utility\\BackendUtility::getUpdateSignal\";s:32:\"371b59286534b5e27d566fda6def3972\";s:10:\"FormEngine\";s:32:\"371b59286534b5e27d566fda6def3972\";s:42:\"TYPO3\\CMS\\Belog\\Controller\\ToolsController\";s:32:\"371b59286534b5e27d566fda6def3972\";}s:10:\"modulemenu\";s:2:\"{}\";s:17:\"systeminformation\";s:45:\"{\"system_BelogLog\":{\"lastAccess\":1459578659}}\";}',NULL,NULL,1,'',0,0,NULL,1459573884,0,NULL,0,1,'');
/*!40000 ALTER TABLE `be_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_md5params`
--

DROP TABLE IF EXISTS `cache_md5params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache_md5params` (
  `md5hash` varchar(20) NOT NULL DEFAULT '',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(3) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`md5hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_md5params`
--

LOCK TABLES `cache_md5params` WRITE;
/*!40000 ALTER TABLE `cache_md5params` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_md5params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_treelist`
--

DROP TABLE IF EXISTS `cache_treelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache_treelist` (
  `md5hash` char(32) NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL DEFAULT '0',
  `treelist` mediumtext,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`md5hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_treelist`
--

LOCK TABLES `cache_treelist` WRITE;
/*!40000 ALTER TABLE `cache_treelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_treelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_hash`
--

DROP TABLE IF EXISTS `cf_cache_hash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_hash` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_hash`
--

LOCK TABLES `cf_cache_hash` WRITE;
/*!40000 ALTER TABLE `cf_cache_hash` DISABLE KEYS */;
INSERT INTO `cf_cache_hash` VALUES (1,'1062fdc4fae6c932cda8e04241e42873',2145909600,'a:2:{i:0;a:3:{s:8:\"TSconfig\";a:4:{s:8:\"options.\";a:13:{s:15:\"enableBookmarks\";s:1:\"1\";s:10:\"file_list.\";a:3:{s:28:\"enableDisplayBigControlPanel\";s:9:\"activated\";s:23:\"enableDisplayThumbnails\";s:10:\"selectable\";s:15:\"enableClipBoard\";s:10:\"selectable\";}s:9:\"pageTree.\";a:5:{s:31:\"doktypesToShowInNewPageDragArea\";s:21:\"1,6,4,7,3,254,255,199\";s:19:\"showPathAboveMounts\";s:1:\"1\";s:19:\"showPageIdWithTitle\";s:1:\"1\";s:12:\"showNavTitle\";s:1:\"1\";s:23:\"showDomainNameWithTitle\";s:1:\"1\";}s:12:\"contextMenu.\";a:2:{s:8:\"options.\";a:1:{s:9:\"leftIcons\";s:1:\"1\";}s:6:\"table.\";a:3:{s:13:\"virtual_root.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:6:\"items.\";a:2:{i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:7:\"history\";s:5:\"label\";s:42:\"LLL:EXT:lang/locallang_misc.xlf:CM_history\";s:8:\"iconName\";s:29:\"actions-document-history-open\";s:16:\"displayCondition\";s:19:\"canShowHistory != 0\";s:14:\"callbackAction\";s:16:\"openHistoryPopUp\";}}}s:11:\"pages_root.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:6:\"items.\";a:7:{i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:4:\"view\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.view\";s:8:\"iconName\";s:21:\"actions-document-view\";s:16:\"displayCondition\";s:16:\"canBeViewed != 0\";s:14:\"callbackAction\";s:8:\"viewPage\";}i:200;s:4:\"ITEM\";s:4:\"200.\";a:5:{s:4:\"name\";s:3:\"new\";s:5:\"label\";s:38:\"LLL:EXT:lang/locallang_core.xlf:cm.new\";s:8:\"iconName\";s:16:\"actions-page-new\";s:16:\"displayCondition\";s:22:\"canCreateNewPages != 0\";s:14:\"callbackAction\";s:13:\"newPageWizard\";}i:300;s:7:\"DIVIDER\";i:400;s:4:\"ITEM\";s:4:\"400.\";a:5:{s:4:\"name\";s:7:\"history\";s:5:\"label\";s:42:\"LLL:EXT:lang/locallang_misc.xlf:CM_history\";s:8:\"iconName\";s:29:\"actions-document-history-open\";s:16:\"displayCondition\";s:19:\"canShowHistory != 0\";s:14:\"callbackAction\";s:16:\"openHistoryPopUp\";}}}s:6:\"pages.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:6:\"items.\";a:18:{i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:4:\"view\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.view\";s:8:\"iconName\";s:21:\"actions-document-view\";s:16:\"displayCondition\";s:16:\"canBeViewed != 0\";s:14:\"callbackAction\";s:8:\"viewPage\";}i:200;s:7:\"DIVIDER\";i:300;s:4:\"ITEM\";s:4:\"300.\";a:5:{s:4:\"name\";s:7:\"disable\";s:5:\"label\";s:41:\"LLL:EXT:lang/locallang_common.xlf:disable\";s:8:\"iconName\";s:17:\"actions-edit-hide\";s:16:\"displayCondition\";s:52:\"getRecord|hidden = 0 && canBeDisabledAndEnabled != 0\";s:14:\"callbackAction\";s:11:\"disablePage\";}i:400;s:4:\"ITEM\";s:4:\"400.\";a:5:{s:4:\"name\";s:6:\"enable\";s:5:\"label\";s:40:\"LLL:EXT:lang/locallang_common.xlf:enable\";s:8:\"iconName\";s:19:\"actions-edit-unhide\";s:16:\"displayCondition\";s:52:\"getRecord|hidden = 1 && canBeDisabledAndEnabled != 0\";s:14:\"callbackAction\";s:10:\"enablePage\";}i:500;s:4:\"ITEM\";s:4:\"500.\";a:5:{s:4:\"name\";s:4:\"edit\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.edit\";s:8:\"iconName\";s:17:\"actions-page-open\";s:16:\"displayCondition\";s:16:\"canBeEdited != 0\";s:14:\"callbackAction\";s:18:\"editPageProperties\";}i:600;s:4:\"ITEM\";s:4:\"600.\";a:5:{s:4:\"name\";s:4:\"info\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.info\";s:8:\"iconName\";s:21:\"actions-document-info\";s:16:\"displayCondition\";s:16:\"canShowInfo != 0\";s:14:\"callbackAction\";s:13:\"openInfoPopUp\";}i:700;s:4:\"ITEM\";s:4:\"700.\";a:5:{s:4:\"name\";s:7:\"history\";s:5:\"label\";s:42:\"LLL:EXT:lang/locallang_misc.xlf:CM_history\";s:8:\"iconName\";s:29:\"actions-document-history-open\";s:16:\"displayCondition\";s:19:\"canShowHistory != 0\";s:14:\"callbackAction\";s:16:\"openHistoryPopUp\";}i:800;s:7:\"DIVIDER\";i:900;s:7:\"SUBMENU\";s:4:\"900.\";a:19:{s:5:\"label\";s:51:\"LLL:EXT:lang/locallang_core.xlf:cm.copyPasteActions\";i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:3:\"new\";s:5:\"label\";s:38:\"LLL:EXT:lang/locallang_core.xlf:cm.new\";s:8:\"iconName\";s:16:\"actions-page-new\";s:16:\"displayCondition\";s:22:\"canCreateNewPages != 0\";s:14:\"callbackAction\";s:13:\"newPageWizard\";}i:200;s:7:\"DIVIDER\";i:300;s:4:\"ITEM\";s:4:\"300.\";a:5:{s:4:\"name\";s:3:\"cut\";s:5:\"label\";s:38:\"LLL:EXT:lang/locallang_core.xlf:cm.cut\";s:8:\"iconName\";s:16:\"actions-edit-cut\";s:16:\"displayCondition\";s:53:\"isInCutMode = 0 && canBeCut != 0 && isMountPoint != 1\";s:14:\"callbackAction\";s:13:\"enableCutMode\";}i:400;s:4:\"ITEM\";s:4:\"400.\";a:5:{s:4:\"name\";s:3:\"cut\";s:5:\"label\";s:38:\"LLL:EXT:lang/locallang_core.xlf:cm.cut\";s:8:\"iconName\";s:24:\"actions-edit-cut-release\";s:16:\"displayCondition\";s:32:\"isInCutMode = 1 && canBeCut != 0\";s:14:\"callbackAction\";s:14:\"disableCutMode\";}i:500;s:4:\"ITEM\";s:4:\"500.\";a:5:{s:4:\"name\";s:4:\"copy\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.copy\";s:8:\"iconName\";s:17:\"actions-edit-copy\";s:16:\"displayCondition\";s:36:\"isInCopyMode = 0 && canBeCopied != 0\";s:14:\"callbackAction\";s:14:\"enableCopyMode\";}i:600;s:4:\"ITEM\";s:4:\"600.\";a:5:{s:4:\"name\";s:4:\"copy\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.copy\";s:8:\"iconName\";s:25:\"actions-edit-copy-release\";s:16:\"displayCondition\";s:36:\"isInCopyMode = 1 && canBeCopied != 0\";s:14:\"callbackAction\";s:15:\"disableCopyMode\";}i:700;s:4:\"ITEM\";s:4:\"700.\";a:5:{s:4:\"name\";s:9:\"pasteInto\";s:5:\"label\";s:44:\"LLL:EXT:lang/locallang_core.xlf:cm.pasteinto\";s:8:\"iconName\";s:27:\"actions-document-paste-into\";s:16:\"displayCondition\";s:85:\"getContextInfo|inCopyMode = 1 || getContextInfo|inCutMode = 1 && canBePastedInto != 0\";s:14:\"callbackAction\";s:13:\"pasteIntoNode\";}i:800;s:4:\"ITEM\";s:4:\"800.\";a:5:{s:4:\"name\";s:10:\"pasteAfter\";s:5:\"label\";s:45:\"LLL:EXT:lang/locallang_core.xlf:cm.pasteafter\";s:8:\"iconName\";s:28:\"actions-document-paste-after\";s:16:\"displayCondition\";s:86:\"getContextInfo|inCopyMode = 1 || getContextInfo|inCutMode = 1 && canBePastedAfter != 0\";s:14:\"callbackAction\";s:14:\"pasteAfterNode\";}i:900;s:7:\"DIVIDER\";i:1000;s:4:\"ITEM\";s:5:\"1000.\";a:5:{s:4:\"name\";s:6:\"delete\";s:5:\"label\";s:41:\"LLL:EXT:lang/locallang_core.xlf:cm.delete\";s:8:\"iconName\";s:19:\"actions-edit-delete\";s:16:\"displayCondition\";s:38:\"canBeRemoved != 0 && isMountPoint != 1\";s:14:\"callbackAction\";s:10:\"removeNode\";}}i:1000;s:7:\"SUBMENU\";s:5:\"1000.\";a:8:{s:5:\"label\";s:48:\"LLL:EXT:lang/locallang_core.xlf:cm.branchActions\";i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:15:\"mountAsTreeroot\";s:5:\"label\";s:49:\"LLL:EXT:lang/locallang_core.xlf:cm.tempMountPoint\";s:8:\"iconName\";s:26:\"actions-pagetree-mountroot\";s:16:\"displayCondition\";s:49:\"canBeTemporaryMountPoint != 0 && isMountPoint = 0\";s:14:\"callbackAction\";s:15:\"mountAsTreeRoot\";}i:200;s:7:\"DIVIDER\";i:300;s:4:\"ITEM\";s:4:\"300.\";a:5:{s:4:\"name\";s:12:\"expandBranch\";s:5:\"label\";s:47:\"LLL:EXT:lang/locallang_core.xlf:cm.expandBranch\";s:8:\"iconName\";s:23:\"actions-pagetree-expand\";s:16:\"displayCondition\";s:0:\"\";s:14:\"callbackAction\";s:12:\"expandBranch\";}i:400;s:4:\"ITEM\";s:4:\"400.\";a:5:{s:4:\"name\";s:14:\"collapseBranch\";s:5:\"label\";s:49:\"LLL:EXT:lang/locallang_core.xlf:cm.collapseBranch\";s:8:\"iconName\";s:25:\"actions-pagetree-collapse\";s:16:\"displayCondition\";s:0:\"\";s:14:\"callbackAction\";s:14:\"collapseBranch\";}}}}}}s:14:\"disableDelete.\";a:2:{s:17:\"sys_file_metadata\";s:1:\"1\";s:8:\"sys_file\";s:1:\"1\";}s:11:\"saveDocView\";s:1:\"1\";s:10:\"saveDocNew\";s:1:\"1\";s:11:\"saveDocNew.\";a:3:{s:5:\"pages\";s:1:\"0\";s:8:\"sys_file\";s:1:\"0\";s:17:\"sys_file_metadata\";s:1:\"0\";}s:17:\"createFoldersInEB\";s:1:\"1\";s:11:\"alertPopups\";s:3:\"254\";s:11:\"clearCache.\";a:2:{s:5:\"pages\";s:1:\"1\";s:3:\"all\";s:1:\"1\";}s:15:\"popupWindowSize\";s:7:\"900x900\";s:4:\"RTE.\";a:1:{s:15:\"popupWindowSize\";s:7:\"900x900\";}}s:6:\"setup.\";a:1:{s:8:\"default.\";a:1:{s:8:\"edit_RTE\";s:1:\"1\";}}s:9:\"admPanel.\";a:1:{s:7:\"enable.\";a:4:{s:7:\"preview\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:4:\"info\";s:1:\"1\";s:3:\"all\";s:1:\"1\";}}s:12:\"TCAdefaults.\";a:1:{s:9:\"sys_note.\";a:2:{s:6:\"author\";s:0:\"\";s:5:\"email\";s:0:\"\";}}}s:8:\"sections\";a:0:{}s:5:\"match\";a:0:{}}i:1;s:32:\"1c4ac2b244429d2f004ea11fad0bb50f\";}'),(2,'5ab8edb9df273e87604b97e3b5b3e6f6',2145909600,'a:5:{s:4:\"mod.\";a:6:{s:9:\"web_list.\";a:4:{s:28:\"enableDisplayBigControlPanel\";s:9:\"activated\";s:15:\"enableClipBoard\";s:10:\"selectable\";s:22:\"enableLocalizationView\";s:10:\"selectable\";s:18:\"tableDisplayOrder.\";a:11:{s:9:\"be_users.\";a:1:{s:5:\"after\";s:9:\"be_groups\";}s:15:\"sys_filemounts.\";a:1:{s:5:\"after\";s:8:\"be_users\";}s:17:\"sys_file_storage.\";a:1:{s:5:\"after\";s:14:\"sys_filemounts\";}s:13:\"sys_language.\";a:1:{s:5:\"after\";s:16:\"sys_file_storage\";}s:23:\"pages_language_overlay.\";a:1:{s:6:\"before\";s:5:\"pages\";}s:9:\"fe_users.\";a:2:{s:5:\"after\";s:9:\"fe_groups\";s:6:\"before\";s:5:\"pages\";}s:13:\"sys_template.\";a:1:{s:5:\"after\";s:5:\"pages\";}s:15:\"backend_layout.\";a:1:{s:5:\"after\";s:5:\"pages\";}s:11:\"sys_domain.\";a:1:{s:5:\"after\";s:12:\"sys_template\";}s:11:\"tt_content.\";a:1:{s:5:\"after\";s:33:\"pages,backend_layout,sys_template\";}s:13:\"sys_category.\";a:1:{s:5:\"after\";s:10:\"tt_content\";}}}s:8:\"wizards.\";a:2:{s:10:\"newRecord.\";a:1:{s:6:\"pages.\";a:1:{s:5:\"show.\";a:3:{s:10:\"pageInside\";s:1:\"1\";s:9:\"pageAfter\";s:1:\"1\";s:18:\"pageSelectPosition\";s:1:\"1\";}}}s:18:\"newContentElement.\";a:1:{s:12:\"wizardItems.\";a:4:{s:7:\"common.\";a:1:{s:6:\"header\";s:81:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common\";}s:8:\"special.\";a:1:{s:6:\"header\";s:82:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special\";}s:6:\"forms.\";a:2:{s:6:\"header\";s:80:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:forms\";s:9:\"elements.\";a:1:{s:9:\"mailform.\";a:1:{s:21:\"tt_content_defValues.\";a:1:{s:8:\"bodytext\";s:176:\"enctype = application/x-www-form-urlencoded\r\nmethod = post\r\nprefix = tx_form\r\npostProcessor {\r\n    1 = mail\r\n    1 {\r\n        recipientEmail =\r\n        senderEmail =\r\n    }\r\n}\r\";}}}}s:8:\"plugins.\";a:3:{s:6:\"header\";s:82:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins\";s:9:\"elements.\";a:1:{s:8:\"general.\";a:4:{s:14:\"iconIdentifier\";s:14:\"content-plugin\";s:5:\"title\";s:96:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins_general_title\";s:11:\"description\";s:102:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins_general_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"list\";}}}s:4:\"show\";s:1:\"*\";}}}}s:9:\"web_view.\";a:1:{s:19:\"previewFrameWidths.\";a:11:{s:5:\"1280.\";a:1:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";}s:5:\"1024.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:tablet\";}s:4:\"960.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"800.\";a:1:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";}s:4:\"768.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:tablet\";}s:4:\"600.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:tablet\";}s:4:\"640.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"480.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"400.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"360.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"300.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}}}s:7:\"SHARED.\";a:2:{s:20:\"defaultLanguageLabel\";s:7:\"Deutsch\";s:19:\"defaultLanguageFlag\";s:2:\"ch\";}s:10:\"file_list.\";a:1:{s:28:\"enableDisplayBigControlPanel\";s:9:\"activated\";}s:11:\"web_layout.\";a:1:{s:5:\"menu.\";a:1:{s:9:\"function.\";a:2:{i:0;s:1:\"0\";i:3;s:1:\"0\";}}}}s:4:\"RTE.\";a:4:{s:8:\"default.\";a:20:{s:4:\"skin\";s:35:\"EXT:t3skin/rtehtmlarea/htmlarea.css\";s:3:\"FE.\";a:20:{s:4:\"skin\";s:35:\"EXT:t3skin/rtehtmlarea/htmlarea.css\";s:3:\"FE.\";a:18:{s:4:\"skin\";s:35:\"EXT:t3skin/rtehtmlarea/htmlarea.css\";s:5:\"proc.\";a:15:{s:12:\"overruleMode\";s:6:\"ts_css\";s:21:\"dontConvBRtoParagraph\";s:1:\"1\";s:19:\"preserveDIVSections\";s:1:\"1\";s:16:\"allowTagsOutside\";s:69:\"address, article, aside, blockquote, footer, header, hr, nav, section\";s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:15:\"keepPDIVattribs\";s:67:\"id, title, dir, lang, xml:lang, itemscope, itemtype, itemprop,style\";s:26:\"transformBoldAndItalicTags\";s:1:\"1\";s:14:\"dontUndoHSC_db\";s:1:\"1\";s:11:\"dontHSC_rte\";s:1:\"1\";s:18:\"entryHTMLparser_db\";s:1:\"1\";s:19:\"entryHTMLparser_db.\";a:5:{s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:5:\"tags.\";a:28:{s:4:\"img.\";a:2:{s:14:\"allowedAttribs\";s:1:\"0\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:5:\"span.\";a:3:{s:10:\"fixAttrib.\";a:1:{s:6:\"style.\";a:0:{}}s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:2:\"p.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}s:3:\"hr.\";a:1:{s:14:\"allowedAttribs\";s:11:\"class,style\";}s:2:\"b.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"bdo.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"big.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:11:\"blockquote.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"cite.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"code.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"del.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"dfn.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"em.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"i.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"ins.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"kbd.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"label.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"q.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"samp.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"small.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strike.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strong.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sub.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sup.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"tt.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"u.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"var.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"div.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}}s:10:\"removeTags\";s:63:\"center, font, link, meta, o:p, sdfield, strike, style, title, u\";s:18:\"keepNonMatchedTags\";s:7:\"protect\";}s:14:\"HTMLparser_db.\";a:1:{s:8:\"noAttrib\";s:2:\"br\";}s:17:\"exitHTMLparser_db\";s:1:\"1\";s:18:\"exitHTMLparser_db.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";}s:14:\"allowedClasses\";s:301:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail,align-left, align-center, align-right, align-justify,csc-frame-frame1, csc-frame-frame2,component-items, action-items,component-items-ordered, action-items-ordered,important, name-of-person, detail,indent\";}s:15:\"enableWordClean\";s:1:\"1\";s:16:\"removeTrailingBR\";s:1:\"1\";s:14:\"removeComments\";s:1:\"1\";s:10:\"removeTags\";s:37:\"center, font, o:p, sdfield, strike, u\";s:21:\"removeTagsAndContents\";s:32:\"link, meta, script, style, title\";s:11:\"showButtons\";s:493:\"blockstylelabel, blockstyle, textstylelabel, textstyle,formatblock, bold, italic, subscript, superscript,orderedlist, unorderedlist, outdent, indent, textindicator,insertcharacter, link, table, findreplace, chMode, removeformat, undo, redo, about,toggleborders, tableproperties,rowproperties, rowinsertabove, rowinsertunder, rowdelete, rowsplit,columninsertbefore, columninsertafter, columndelete, columnsplit,cellproperties, cellinsertbefore, cellinsertafter, celldelete, cellsplit, cellmerge\";s:23:\"keepButtonGroupTogether\";s:1:\"1\";s:13:\"showStatusBar\";s:1:\"0\";s:8:\"buttons.\";a:7:{s:12:\"formatblock.\";a:1:{s:11:\"removeItems\";s:11:\"pre,address\";}s:11:\"blockstyle.\";a:1:{s:5:\"tags.\";a:3:{s:4:\"div.\";a:1:{s:14:\"allowedClasses\";s:72:\"align-left, align-center, align-right,csc-frame-frame1, csc-frame-frame2\";}s:6:\"table.\";a:1:{s:14:\"allowedClasses\";s:34:\"csc-frame-frame1, csc-frame-frame2\";}s:3:\"td.\";a:1:{s:14:\"allowedClasses\";s:37:\"align-left, align-center, align-right\";}}}s:10:\"textstyle.\";a:1:{s:5:\"tags.\";a:1:{s:5:\"span.\";a:1:{s:14:\"allowedClasses\";s:33:\"important, name-of-person, detail\";}}}s:5:\"link.\";a:5:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:14:\"allowedClasses\";s:96:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail\";}}s:5:\"page.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:13:\"internal-link\";}}}s:4:\"url.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:24:\"external-link-new-window\";}}}s:5:\"file.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:8:\"download\";}}}s:5:\"mail.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:4:\"mail\";}}}}s:14:\"toggleborders.\";a:1:{s:18:\"setOnTableCreation\";s:1:\"1\";}s:5:\"bold.\";a:1:{s:6:\"hotKey\";s:1:\"b\";}s:7:\"italic.\";a:1:{s:6:\"hotKey\";s:1:\"i\";}}s:41:\"disableAlignmentFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableSpacingFieldsetInTableOperations\";s:1:\"1\";s:37:\"disableColorFieldsetInTableOperations\";s:1:\"1\";s:38:\"disableLayoutFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableBordersFieldsetInTableOperations\";s:1:\"1\";s:7:\"schema.\";a:1:{s:8:\"sources.\";a:1:{s:9:\"schemaOrg\";s:63:\"EXT:rtehtmlarea/extensions/MicrodataSchema/res/schemaOrgAll.rdf\";}}s:11:\"hideButtons\";s:202:\"chMode, blockstyle, textstyle, underline, strikethrough, subscript, superscript, lefttoright, righttoleft, left, center, right, justifyfull, table, inserttag, findreplace, removeformat, copy, cut, paste\";}s:5:\"proc.\";a:15:{s:12:\"overruleMode\";s:6:\"ts_css\";s:21:\"dontConvBRtoParagraph\";s:1:\"1\";s:19:\"preserveDIVSections\";s:1:\"1\";s:16:\"allowTagsOutside\";s:69:\"address, article, aside, blockquote, footer, header, hr, nav, section\";s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:15:\"keepPDIVattribs\";s:67:\"id, title, dir, lang, xml:lang, itemscope, itemtype, itemprop,style\";s:26:\"transformBoldAndItalicTags\";s:1:\"1\";s:14:\"dontUndoHSC_db\";s:1:\"1\";s:11:\"dontHSC_rte\";s:1:\"1\";s:18:\"entryHTMLparser_db\";s:1:\"1\";s:19:\"entryHTMLparser_db.\";a:5:{s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:5:\"tags.\";a:28:{s:4:\"img.\";a:2:{s:14:\"allowedAttribs\";s:1:\"0\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:5:\"span.\";a:3:{s:10:\"fixAttrib.\";a:1:{s:6:\"style.\";a:0:{}}s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:2:\"p.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}s:3:\"hr.\";a:1:{s:14:\"allowedAttribs\";s:11:\"class,style\";}s:2:\"b.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"bdo.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"big.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:11:\"blockquote.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"cite.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"code.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"del.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"dfn.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"em.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"i.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"ins.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"kbd.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"label.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"q.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"samp.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"small.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strike.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strong.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sub.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sup.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"tt.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"u.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"var.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"div.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}}s:10:\"removeTags\";s:63:\"center, font, link, meta, o:p, sdfield, strike, style, title, u\";s:18:\"keepNonMatchedTags\";s:7:\"protect\";}s:14:\"HTMLparser_db.\";a:1:{s:8:\"noAttrib\";s:2:\"br\";}s:17:\"exitHTMLparser_db\";s:1:\"1\";s:18:\"exitHTMLparser_db.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";}s:14:\"allowedClasses\";s:301:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail,align-left, align-center, align-right, align-justify,csc-frame-frame1, csc-frame-frame2,component-items, action-items,component-items-ordered, action-items-ordered,important, name-of-person, detail,indent\";}s:15:\"enableWordClean\";s:1:\"1\";s:16:\"removeTrailingBR\";s:1:\"1\";s:14:\"removeComments\";s:1:\"1\";s:10:\"removeTags\";s:37:\"center, font, o:p, sdfield, strike, u\";s:21:\"removeTagsAndContents\";s:32:\"link, meta, script, style, title\";s:11:\"showButtons\";s:112:\"orderedlist,unorderedlist,bold,italic,subscript,superscript,link,unlink,blockstyle,textstyle,chMode,removeformat\";s:23:\"keepButtonGroupTogether\";s:1:\"1\";s:13:\"showStatusBar\";s:1:\"0\";s:8:\"buttons.\";a:7:{s:12:\"formatblock.\";a:1:{s:11:\"removeItems\";s:11:\"pre,address\";}s:11:\"blockstyle.\";a:2:{s:5:\"tags.\";a:4:{s:4:\"div.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}s:6:\"table.\";a:1:{s:14:\"allowedClasses\";s:0:\"\";}s:3:\"td.\";a:1:{s:14:\"allowedClasses\";s:0:\"\";}s:2:\"p.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}}s:18:\"showTagFreeClasses\";s:1:\"1\";}s:10:\"textstyle.\";a:2:{s:5:\"tags.\";a:1:{s:5:\"span.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}}s:18:\"showTagFreeClasses\";s:1:\"1\";}s:5:\"link.\";a:5:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:14:\"allowedClasses\";s:96:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail\";}}s:5:\"page.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:13:\"internal-link\";}}}s:4:\"url.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:24:\"external-link-new-window\";}}}s:5:\"file.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:8:\"download\";}}}s:5:\"mail.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:4:\"mail\";}}}}s:14:\"toggleborders.\";a:1:{s:18:\"setOnTableCreation\";s:1:\"1\";}s:5:\"bold.\";a:1:{s:6:\"hotKey\";s:1:\"b\";}s:7:\"italic.\";a:1:{s:6:\"hotKey\";s:1:\"i\";}}s:41:\"disableAlignmentFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableSpacingFieldsetInTableOperations\";s:1:\"1\";s:37:\"disableColorFieldsetInTableOperations\";s:1:\"1\";s:38:\"disableLayoutFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableBordersFieldsetInTableOperations\";s:1:\"1\";s:7:\"schema.\";a:1:{s:8:\"sources.\";a:1:{s:9:\"schemaOrg\";s:63:\"EXT:rtehtmlarea/extensions/MicrodataSchema/res/schemaOrgAll.rdf\";}}s:11:\"contentCSS.\";a:1:{s:7:\"mainCSS\";s:54:\"EXT:templatebootstrap/Resources/Public/Backend/RTE.css\";}s:12:\"contextMenu.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}s:5:\"proc.\";a:15:{s:12:\"overruleMode\";s:6:\"ts_css\";s:21:\"dontConvBRtoParagraph\";s:1:\"1\";s:19:\"preserveDIVSections\";s:1:\"1\";s:16:\"allowTagsOutside\";s:69:\"address, article, aside, blockquote, footer, header, hr, nav, section\";s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:15:\"keepPDIVattribs\";s:67:\"id, title, dir, lang, xml:lang, itemscope, itemtype, itemprop,style\";s:26:\"transformBoldAndItalicTags\";s:1:\"1\";s:14:\"dontUndoHSC_db\";s:1:\"1\";s:11:\"dontHSC_rte\";s:1:\"1\";s:18:\"entryHTMLparser_db\";s:1:\"1\";s:19:\"entryHTMLparser_db.\";a:5:{s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:5:\"tags.\";a:28:{s:4:\"img.\";a:2:{s:14:\"allowedAttribs\";s:1:\"0\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:5:\"span.\";a:3:{s:10:\"fixAttrib.\";a:1:{s:6:\"style.\";a:0:{}}s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:2:\"p.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}s:3:\"hr.\";a:1:{s:14:\"allowedAttribs\";s:11:\"class,style\";}s:2:\"b.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"bdo.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"big.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:11:\"blockquote.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"cite.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"code.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"del.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"dfn.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"em.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"i.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"ins.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"kbd.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"label.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"q.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"samp.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"small.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strike.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strong.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sub.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sup.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"tt.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"u.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"var.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"div.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}}s:10:\"removeTags\";s:63:\"center, font, link, meta, o:p, sdfield, strike, style, title, u\";s:18:\"keepNonMatchedTags\";s:7:\"protect\";}s:14:\"HTMLparser_db.\";a:1:{s:8:\"noAttrib\";s:2:\"br\";}s:17:\"exitHTMLparser_db\";s:1:\"1\";s:18:\"exitHTMLparser_db.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";}s:14:\"allowedClasses\";s:301:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail,align-left, align-center, align-right, align-justify,csc-frame-frame1, csc-frame-frame2,component-items, action-items,component-items-ordered, action-items-ordered,important, name-of-person, detail,indent\";}s:15:\"enableWordClean\";s:1:\"1\";s:16:\"removeTrailingBR\";s:1:\"1\";s:14:\"removeComments\";s:1:\"1\";s:10:\"removeTags\";s:37:\"center, font, o:p, sdfield, strike, u\";s:21:\"removeTagsAndContents\";s:32:\"link, meta, script, style, title\";s:11:\"showButtons\";s:112:\"orderedlist,unorderedlist,bold,italic,subscript,superscript,link,unlink,blockstyle,textstyle,chMode,removeformat\";s:23:\"keepButtonGroupTogether\";s:1:\"1\";s:13:\"showStatusBar\";s:1:\"1\";s:8:\"buttons.\";a:7:{s:12:\"formatblock.\";a:1:{s:11:\"removeItems\";s:11:\"pre,address\";}s:11:\"blockstyle.\";a:2:{s:5:\"tags.\";a:4:{s:4:\"div.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}s:6:\"table.\";a:1:{s:14:\"allowedClasses\";s:0:\"\";}s:3:\"td.\";a:1:{s:14:\"allowedClasses\";s:0:\"\";}s:2:\"p.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}}s:18:\"showTagFreeClasses\";s:1:\"1\";}s:10:\"textstyle.\";a:2:{s:5:\"tags.\";a:1:{s:5:\"span.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}}s:18:\"showTagFreeClasses\";s:1:\"1\";}s:5:\"link.\";a:5:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:14:\"allowedClasses\";s:96:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail\";}}s:5:\"page.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:13:\"internal-link\";}}}s:4:\"url.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:24:\"external-link-new-window\";}}}s:5:\"file.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:8:\"download\";}}}s:5:\"mail.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:4:\"mail\";}}}}s:14:\"toggleborders.\";a:1:{s:18:\"setOnTableCreation\";s:1:\"1\";}s:5:\"bold.\";a:1:{s:6:\"hotKey\";s:1:\"b\";}s:7:\"italic.\";a:1:{s:6:\"hotKey\";s:1:\"i\";}}s:41:\"disableAlignmentFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableSpacingFieldsetInTableOperations\";s:1:\"1\";s:37:\"disableColorFieldsetInTableOperations\";s:1:\"1\";s:38:\"disableLayoutFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableBordersFieldsetInTableOperations\";s:1:\"1\";s:7:\"schema.\";a:1:{s:8:\"sources.\";a:1:{s:9:\"schemaOrg\";s:63:\"EXT:rtehtmlarea/extensions/MicrodataSchema/res/schemaOrgAll.rdf\";}}s:11:\"contentCSS.\";a:1:{s:7:\"mainCSS\";s:54:\"EXT:templatebootstrap/Resources/Public/Backend/RTE.css\";}s:12:\"contextMenu.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}s:7:\"config.\";a:1:{s:11:\"tt_content.\";a:1:{s:9:\"bodytext.\";a:2:{s:5:\"proc.\";a:1:{s:12:\"overruleMode\";s:6:\"ts_css\";}s:6:\"types.\";a:2:{s:5:\"text.\";a:1:{s:5:\"proc.\";a:1:{s:12:\"overruleMode\";s:6:\"ts_css\";}}s:8:\"textpic.\";a:1:{s:5:\"proc.\";a:1:{s:12:\"overruleMode\";s:6:\"ts_css\";}}}}}}s:8:\"classes.\";a:13:{s:11:\"align-left.\";a:2:{s:4:\"name\";s:81:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_tooltips.xlf:justifyleft\";s:5:\"value\";s:17:\"text-align: left;\";}s:13:\"align-center.\";a:2:{s:4:\"name\";s:83:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_tooltips.xlf:justifycenter\";s:5:\"value\";s:19:\"text-align: center;\";}s:12:\"align-right.\";a:2:{s:4:\"name\";s:82:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_tooltips.xlf:justifyright\";s:5:\"value\";s:18:\"text-align: right;\";}s:17:\"csc-frame-frame1.\";a:2:{s:4:\"name\";s:84:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:frame-frame1\";s:5:\"value\";s:53:\"background-color: #EDEBF1; border: 1px solid #333333;\";}s:17:\"csc-frame-frame2.\";a:2:{s:4:\"name\";s:84:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:frame-frame2\";s:5:\"value\";s:53:\"background-color: #F5FFAA; border: 1px solid #333333;\";}s:10:\"important.\";a:2:{s:4:\"name\";s:81:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:important\";s:5:\"value\";s:15:\"color: #8A0020;\";}s:15:\"name-of-person.\";a:2:{s:4:\"name\";s:86:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:name-of-person\";s:5:\"value\";s:15:\"color: #10007B;\";}s:7:\"detail.\";a:2:{s:4:\"name\";s:78:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:detail\";s:5:\"value\";s:15:\"color: #186900;\";}s:16:\"component-items.\";a:2:{s:4:\"name\";s:87:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:component-items\";s:5:\"value\";s:15:\"color: #186900;\";}s:13:\"action-items.\";a:2:{s:4:\"name\";s:84:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:action-items\";s:5:\"value\";s:15:\"color: #8A0020;\";}s:24:\"component-items-ordered.\";a:2:{s:4:\"name\";s:87:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:component-items\";s:5:\"value\";s:15:\"color: #186900;\";}s:21:\"action-items-ordered.\";a:2:{s:4:\"name\";s:84:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:action-items\";s:5:\"value\";s:15:\"color: #8A0020;\";}s:13:\"examplestyle.\";a:2:{s:4:\"name\";s:89:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:rte.classes.examplestyle\";s:8:\"requires\";s:19:\"foo-class,bar-class\";}}s:14:\"classesAnchor.\";a:6:{s:13:\"externalLink.\";a:3:{s:5:\"class\";s:13:\"external-link\";s:4:\"type\";s:3:\"url\";s:9:\"titleText\";s:103:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:external_link_titleText\";}s:24:\"externalLinkInNewWindow.\";a:3:{s:5:\"class\";s:24:\"external-link-new-window\";s:4:\"type\";s:3:\"url\";s:9:\"titleText\";s:114:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:external_link_new_window_titleText\";}s:13:\"internalLink.\";a:3:{s:5:\"class\";s:13:\"internal-link\";s:4:\"type\";s:4:\"page\";s:9:\"titleText\";s:103:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:internal_link_titleText\";}s:24:\"internalLinkInNewWindow.\";a:3:{s:5:\"class\";s:24:\"internal-link-new-window\";s:4:\"type\";s:4:\"page\";s:9:\"titleText\";s:114:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:internal_link_new_window_titleText\";}s:9:\"download.\";a:3:{s:5:\"class\";s:8:\"download\";s:4:\"type\";s:4:\"file\";s:9:\"titleText\";s:98:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:download_titleText\";}s:5:\"mail.\";a:3:{s:5:\"class\";s:4:\"mail\";s:4:\"type\";s:4:\"mail\";s:9:\"titleText\";s:94:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:mail_titleText\";}}}s:8:\"TCEFORM.\";a:2:{s:11:\"tt_content.\";a:6:{s:12:\"imageorient.\";a:2:{s:6:\"types.\";a:1:{s:6:\"image.\";a:1:{s:11:\"removeItems\";s:18:\"8,9,10,17,18,25,26\";}}s:11:\"removeItems\";s:5:\"0,8,9\";}s:5:\"date.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:11:\"categories.\";a:1:{s:8:\"disabled\";s:1:\"0\";}s:14:\"header_layout.\";a:2:{s:9:\"keepItems\";s:5:\"2,3,4\";s:10:\"altLabels.\";a:4:{i:1;s:91:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.header_layout.1\";i:2;s:91:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.header_layout.2\";i:3;s:91:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.header_layout.3\";i:4;s:91:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.header_layout.4\";}}s:7:\"layout.\";a:3:{s:8:\"disabled\";s:1:\"0\";s:9:\"keepItems\";s:3:\"0,1\";s:10:\"altLabels.\";a:1:{i:1;s:96:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.layout.examplelayout\";}}s:12:\"imageborder.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}s:6:\"pages.\";a:12:{s:6:\"alias.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:9:\"abstract.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:9:\"keywords.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:11:\"categories.\";a:1:{s:8:\"disabled\";s:1:\"0\";}s:8:\"doktype.\";a:1:{s:11:\"removeItems\";s:1:\"6\";}s:7:\"layout.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:15:\"backend_layout.\";a:1:{s:11:\"removeItems\";s:2:\"-1\";}s:26:\"backend_layout_next_level.\";a:1:{s:11:\"removeItems\";s:2:\"-1\";}s:9:\"newUntil.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:7:\"author.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:13:\"author_email.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:12:\"lastUpdated.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}}s:12:\"TCAdefaults.\";a:2:{s:11:\"tt_content.\";a:3:{s:13:\"header_layout\";s:1:\"2\";s:9:\"imagecols\";s:1:\"1\";s:11:\"imageorient\";s:1:\"2\";}s:10:\"be_groups.\";a:9:{s:13:\"tables_select\";s:168:\"    pages,sys_category,sys_collection,sys_file,sys_file_collection,sys_file_metadata,\r\n    sys_file_reference,backend_layout,pages_language_overlay,tt_content,sys_note\r\";s:18:\"non_exclude_fields\";s:3089:\"    pages_language_overlay:abstract,pages_language_overlay:author,pages_language_overlay:description,\r\n    pages_language_overlay:author_email,pages_language_overlay:media,pages_language_overlay:hidden,pages_language_overlay:keywords,\r\n    pages_language_overlay:nav_title,pages_language_overlay:shortcut_mode,pages_language_overlay:starttime,\r\n    pages_language_overlay:endtime,pages_language_overlay:subtitle,pages_language_overlay:urltype,pages_language_overlay:doktype,\r\n    pages_language_overlay:url,pages_language_overlay:tx_realurl_pathsegment,\r\n\r\n    sys_category:hidden,sys_category:sys_language_uid,sys_category:starttime,sys_category:endtime,sys_category:l10n_parent,\r\n\r\n    sys_file_metadata:categories,sys_file_metadata:title,\r\n\r\n    sys_file_reference:alternative,sys_file_reference:description,sys_file_reference:crop,sys_file_reference:link,\r\n    sys_file_reference:title,sys_file_reference:autoplay,\r\n    sys_file_collection:hidden,sys_file_collection:sys_language_uid,sys_file_collection:starttime,sys_file_collection:endtime,\r\n    sys_file_collection:l10n_parent,\r\n\r\n    pages:newUntil,pages:abstract,pages:fe_group,pages:alias,pages:author,pages:backend_layout_next_level,pages:backend_layout,\r\n    pages:cache_timeout,pages:cache_tags,pages:categories,pages:module,pages:description,pages:tx_realurl_nocache,pages:author_email,\r\n    pages:url_scheme,pages:tx_realurl_exclude,pages:media,pages:nav_hide,pages:hidden,pages:extendToSubpages,pages:is_siteroot,\r\n    pages:keywords,pages:lastUpdated,pages:layout,pages:l18n_cfg,pages:fe_login_mode,pages:nav_title,pages:no_cache,pages:no_search,\r\n    pages:tx_realurl_pathoverride,pages:shortcut_mode,pages:content_from_pid,pages:tx_realurl_pathsegment,pages:starttime,\r\n    pages:php_tree_stop,pages:endtime,pages:subtitle,pages:target,pages:doktype,\r\n\r\n    tt_content:rowDescription,tt_content:fe_group,tt_content:uploads_description,tt_content:uploads_type,\r\n    tt_content:categories,tt_content:date,tt_content:colPos,tt_content:hidden,\r\n    tt_content:table_enclosure,tt_content:table_delimiter,tt_content:table_caption,tt_content:table_tfoot,tt_content:table_header_position,\r\n    tt_content:image_zoom,tt_content:select_key,tt_content:imagecols,\r\n    tt_content:section_frame,tt_content:imagewidth,tt_content:imageheight,\r\n    tt_content:sectionIndex,tt_content:sys_language_uid,tt_content:layout,tt_content:header_link,\r\n    tt_content:imageorient,tt_content:recursive,tt_content:starttime,tt_content:endtime,tt_content:subheader,\r\n    tt_content:linkToTop,tt_content:l18n_parent,tt_content:header_layout,\r\n    tt_content:pi_flexform;login;sDEF;pages,\r\n\r\n    fe_users:address,fe_users:city,fe_users:company,fe_users:country,fe_users:disable,fe_users:email,fe_users:fax,fe_users:first_name,\r\n    fe_users:felogin_forgotHash,fe_users:image,fe_users:lastlogin,fe_users:last_name,fe_users:lockToDomain,\r\n    fe_users:middle_name,fe_users:name,fe_users:telephone,fe_users:tx_extbase_type,fe_users:felogin_redirectPid,fe_users:starttime,\r\n    fe_users:endtime,fe_users:title,fe_users:TSconfig,fe_users:www,fe_users:zip\r\";s:18:\"explicit_allowdeny\";s:396:\"    tt_content:CType:--div--:ALLOW,tt_content:CType:header:ALLOW,tt_content:CType:textmedia:ALLOW,\r\n    tt_content:CType:table:ALLOW,tt_content:CType:uploads:ALLOW,tt_content:CType:menu:ALLOW,\r\n    tt_content:CType:shortcut:ALLOW,tt_content:CType:list:ALLOW,tt_content:CType:div:ALLOW,tt_content:CType:html:ALLOW,\r\n    tt_content:CType:mailform:ALLOW,tt_content:list_type:indexedsearch_pi2:ALLOW\r\";s:16:\"pagetypes_select\";s:13:\"1,4,3,254,199\";s:13:\"tables_modify\";s:188:\"    pages,sys_category,sys_collection,sys_file,sys_file_collection,sys_file_metadata,sys_file_reference,\r\n    sys_file_storage,backend_layout, pages_language_overlay,sys_domain,tt_content\r\";s:9:\"groupMods\";s:57:\"web_layout,web_list,web_info,file_FilelistList,user_setup\";s:16:\"file_permissions\";s:192:\"    readFolder,writeFolder,addFolder,renameFolder,moveFolder,deleteFolder,recursivedeleteFolder,copyFolder,\r\n    readFile,writeFile,addFile,copyFile,renameFile,replaceFile,moveFile,deleteFile\r\";s:14:\"db_mountpoints\";s:1:\"1\";s:16:\"file_mountpoints\";s:1:\"1\";}}s:8:\"TCEMAIN.\";a:1:{s:12:\"permissions.\";a:4:{s:6:\"userid\";s:1:\"1\";s:7:\"groupid\";s:1:\"1\";s:4:\"user\";s:36:\"show, editcontent, edit, new, delete\";s:5:\"group\";s:36:\"show, editcontent, edit, new, delete\";}}}'),(3,'14273b5a17db055e0b1d67ee40919770',2145909600,'a:13:{s:7:\"config.\";a:34:{s:16:\"sys_language_uid\";s:1:\"0\";s:20:\"sys_language_isocode\";s:2:\"de\";s:10:\"locale_all\";s:5:\"de_CH\";s:8:\"language\";s:2:\"de\";s:15:\"htmlTag_langKey\";s:2:\"de\";s:7:\"doctype\";s:5:\"html5\";s:11:\"xmlprologue\";s:4:\"none\";s:20:\"disablePrefixComment\";s:1:\"1\";s:12:\"index_enable\";s:1:\"1\";s:15:\"index_externals\";s:1:\"1\";s:14:\"index_metatags\";s:1:\"1\";s:15:\"removeDefaultJS\";s:8:\"external\";s:20:\"inlineStyle2TempFile\";s:1:\"1\";s:10:\"compressJs\";s:1:\"0\";s:11:\"compressCss\";s:1:\"0\";s:13:\"concatenateJs\";s:1:\"0\";s:14:\"concatenateCss\";s:1:\"0\";s:23:\"simulateStaticDocuments\";s:1:\"0\";s:12:\"absRefPrefix\";s:1:\"/\";s:17:\"tx_realurl_enable\";s:1:\"0\";s:25:\"spamProtectEmailAddresses\";s:2:\"-2\";s:33:\"spamProtectEmailAddresses_atSubst\";s:60:\"<script type=\"text/javascript\">document.write(\'@\');</script>\";s:38:\"spamProtectEmailAddresses_lastDotSubst\";s:60:\"<script type=\"text/javascript\">document.write(\'.\');</script>\";s:8:\"linkVars\";s:7:\"L(0-10)\";s:32:\"typolinkEnableLinksAcrossDomains\";s:1:\"1\";s:21:\"typolinkCheckRootline\";s:1:\"1\";s:35:\"content_from_pid_allowOutsideDomain\";s:1:\"1\";s:21:\"cache_clearAtMidnight\";s:1:\"1\";s:12:\"cache_period\";s:4:\"1200\";s:16:\"sendCacheHeaders\";s:1:\"1\";s:10:\"pageTitle.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:6:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:2:{s:5:\"value\";s:11:\"Muster AG -\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"|| |\";s:3:\"if.\";a:1:{s:6:\"isTrue\";s:11:\"Muster AG -\";}}}i:20;s:4:\"TEXT\";s:3:\"20.\";a:2:{s:5:\"field\";s:17:\"subtitle // title\";s:9:\"override.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:4:\"data\";s:17:\"TSFE:altPageTitle\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:2:{s:5:\"value\";s:0:\"\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:3:\"if.\";a:1:{s:6:\"isTrue\";s:0:\"\";}}}}}s:13:\"headerComment\";s:127:\"        Stämpfli AG // www.staempfli.com\r\n\r\nPackage version: 2.0.0-36-g6af9273 (master)\r\nBootstrap package version: 2.1.0-DEV\r\";s:16:\"htmlTag_stdWrap.\";a:3:{s:19:\"setContentToCurrent\";s:1:\"1\";s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:1:{s:15:\"htmlTag_langKey\";s:2:\"de\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:2:{s:5:\"value\";s:217:\"                <!--[if IE 8]><html class=\"no-js lt-ie9\" lang=\"{REGISTER:htmlTag_langKey}\"> <![endif]-->\r\n                <!--[if gt IE 8]><!--> <html class=\"no-js\" lang=\"{REGISTER:htmlTag_langKey}\"> <!--<![endif]-->\r\";s:6:\"value.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}i:30;s:16:\"RESTORE_REGISTER\";}}s:11:\"tx_extbase.\";a:3:{s:4:\"mvc.\";a:2:{s:16:\"requestHandlers.\";a:4:{s:48:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\FrontendRequestHandler\";s:48:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\FrontendRequestHandler\";s:47:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\BackendRequestHandler\";s:47:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\BackendRequestHandler\";s:40:\"TYPO3\\CMS\\Extbase\\Mvc\\Cli\\RequestHandler\";s:40:\"TYPO3\\CMS\\Extbase\\Mvc\\Cli\\RequestHandler\";s:48:\"TYPO3\\CMS\\Fluid\\Core\\Widget\\WidgetRequestHandler\";s:48:\"TYPO3\\CMS\\Fluid\\Core\\Widget\\WidgetRequestHandler\";}s:48:\"throwPageNotFoundExceptionIfActionCantBeResolved\";s:1:\"0\";}s:12:\"persistence.\";a:4:{s:28:\"enableAutomaticCacheClearing\";s:1:\"1\";s:20:\"updateReferenceIndex\";s:1:\"0\";s:13:\"useQueryCache\";s:1:\"1\";s:8:\"classes.\";a:14:{s:41:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\FileMount.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:14:\"sys_filemounts\";s:8:\"columns.\";a:3:{s:6:\"title.\";a:1:{s:13:\"mapOnProperty\";s:5:\"title\";}s:5:\"path.\";a:1:{s:13:\"mapOnProperty\";s:4:\"path\";}s:5:\"base.\";a:1:{s:13:\"mapOnProperty\";s:14:\"isAbsolutePath\";}}}}s:45:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\FileReference.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:18:\"sys_file_reference\";}}s:36:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\File.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:8:\"sys_file\";}}s:43:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\BackendUser.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:8:\"be_users\";s:8:\"columns.\";a:8:{s:9:\"username.\";a:1:{s:13:\"mapOnProperty\";s:8:\"userName\";}s:6:\"admin.\";a:1:{s:13:\"mapOnProperty\";s:15:\"isAdministrator\";}s:8:\"disable.\";a:1:{s:13:\"mapOnProperty\";s:10:\"isDisabled\";}s:9:\"realName.\";a:1:{s:13:\"mapOnProperty\";s:8:\"realName\";}s:10:\"starttime.\";a:1:{s:13:\"mapOnProperty\";s:16:\"startDateAndTime\";}s:8:\"endtime.\";a:1:{s:13:\"mapOnProperty\";s:14:\"endDateAndTime\";}s:14:\"disableIPlock.\";a:1:{s:13:\"mapOnProperty\";s:16:\"ipLockIsDisabled\";}s:10:\"lastlogin.\";a:1:{s:13:\"mapOnProperty\";s:20:\"lastLoginDateAndTime\";}}}}s:48:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\BackendUserGroup.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:9:\"be_groups\";s:8:\"columns.\";a:14:{s:9:\"subgroup.\";a:1:{s:13:\"mapOnProperty\";s:9:\"subGroups\";}s:10:\"groupMods.\";a:1:{s:13:\"mapOnProperty\";s:7:\"modules\";}s:14:\"tables_select.\";a:1:{s:13:\"mapOnProperty\";s:15:\"tablesListening\";}s:14:\"tables_modify.\";a:1:{s:13:\"mapOnProperty\";s:12:\"tablesModify\";}s:17:\"pagetypes_select.\";a:1:{s:13:\"mapOnProperty\";s:9:\"pageTypes\";}s:19:\"non_exclude_fields.\";a:1:{s:13:\"mapOnProperty\";s:20:\"allowedExcludeFields\";}s:19:\"explicit_allowdeny.\";a:1:{s:13:\"mapOnProperty\";s:22:\"explicitlyAllowAndDeny\";}s:18:\"allowed_languages.\";a:1:{s:13:\"mapOnProperty\";s:16:\"allowedLanguages\";}s:16:\"workspace_perms.\";a:1:{s:13:\"mapOnProperty\";s:19:\"workspacePermission\";}s:15:\"db_mountpoints.\";a:1:{s:13:\"mapOnProperty\";s:14:\"databaseMounts\";}s:17:\"file_permissions.\";a:1:{s:13:\"mapOnProperty\";s:24:\"fileOperationPermissions\";}s:13:\"lockToDomain.\";a:1:{s:13:\"mapOnProperty\";s:12:\"lockToDomain\";}s:14:\"hide_in_lists.\";a:1:{s:13:\"mapOnProperty\";s:10:\"hideInList\";}s:9:\"TSconfig.\";a:1:{s:13:\"mapOnProperty\";s:8:\"tsConfig\";}}}}s:44:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\FrontendUser.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:8:\"fe_users\";s:8:\"columns.\";a:1:{s:13:\"lockToDomain.\";a:1:{s:13:\"mapOnProperty\";s:12:\"lockToDomain\";}}}}s:49:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\FrontendUserGroup.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:9:\"fe_groups\";s:8:\"columns.\";a:1:{s:13:\"lockToDomain.\";a:1:{s:13:\"mapOnProperty\";s:12:\"lockToDomain\";}}}}s:40:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\Category.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:12:\"sys_category\";}}s:42:\"TYPO3\\CMS\\Beuser\\Domain\\Model\\BackendUser.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:8:\"be_users\";s:8:\"columns.\";a:4:{s:18:\"allowed_languages.\";a:1:{s:13:\"mapOnProperty\";s:16:\"allowedLanguages\";}s:17:\"file_mountpoints.\";a:1:{s:13:\"mapOnProperty\";s:15:\"fileMountPoints\";}s:15:\"db_mountpoints.\";a:1:{s:13:\"mapOnProperty\";s:13:\"dbMountPoints\";}s:10:\"usergroup.\";a:1:{s:13:\"mapOnProperty\";s:17:\"backendUserGroups\";}}}}s:47:\"TYPO3\\CMS\\Beuser\\Domain\\Model\\BackendUserGroup.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:9:\"be_groups\";s:8:\"columns.\";a:1:{s:9:\"subgroup.\";a:1:{s:13:\"mapOnProperty\";s:9:\"subGroups\";}}}}s:39:\"TYPO3\\CMS\\SysNote\\Domain\\Model\\SysNote.\";a:1:{s:8:\"mapping.\";a:3:{s:9:\"tableName\";s:8:\"sys_note\";s:10:\"recordType\";s:0:\"\";s:8:\"columns.\";a:3:{s:7:\"crdate.\";a:1:{s:13:\"mapOnProperty\";s:12:\"creationDate\";}s:7:\"tstamp.\";a:1:{s:13:\"mapOnProperty\";s:16:\"modificationDate\";}s:7:\"cruser.\";a:1:{s:13:\"mapOnProperty\";s:6:\"author\";}}}}s:41:\"DmitryDulepov\\Realurl\\Domain\\Model\\Alias.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:20:\"tx_realurl_uniqalias\";}}s:49:\"DmitryDulepov\\Realurl\\Domain\\Model\\UrlCacheEntry.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:19:\"tx_realurl_urlcache\";s:8:\"columns.\";a:1:{s:12:\"rootpage_id.\";a:1:{s:13:\"mapOnProperty\";s:10:\"rootPageId\";}}}}s:50:\"DmitryDulepov\\Realurl\\Domain\\Model\\PathCacheEntry.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:20:\"tx_realurl_pathcache\";s:8:\"columns.\";a:3:{s:6:\"mpvar.\";a:1:{s:13:\"mapOnProperty\";s:5:\"mpVar\";}s:9:\"pagepath.\";a:1:{s:13:\"mapOnProperty\";s:8:\"pagePath\";}s:12:\"rootpage_id.\";a:1:{s:13:\"mapOnProperty\";s:10:\"rootPageId\";}}}}}}s:9:\"features.\";a:2:{s:20:\"skipDefaultArguments\";s:1:\"0\";s:25:\"ignoreAllEnableFieldsInBe\";s:1:\"0\";}}}s:7:\"plugin.\";a:4:{s:17:\"tx_indexedsearch.\";a:4:{s:17:\"_DEFAULT_PI_VARS.\";a:1:{s:4:\"lang\";s:1:\"0\";}s:9:\"settings.\";a:24:{s:12:\"displayRules\";s:1:\"1\";s:25:\"displayAdvancedSearchLink\";s:1:\"1\";s:19:\"displayResultNumber\";s:1:\"0\";s:14:\"breadcrumbWrap\";s:6:\"/ || /\";s:17:\"displayParsetimes\";s:1:\"0\";s:21:\"displayLevel1Sections\";s:1:\"1\";s:21:\"displayLevel2Sections\";s:1:\"0\";s:21:\"displayLevelxAllTypes\";s:1:\"0\";s:14:\"clearSearchBox\";s:1:\"0\";s:15:\"clearSearchBox.\";a:1:{s:23:\"enableSubSearchCheckBox\";s:1:\"1\";}s:23:\"displayForbiddenRecords\";s:1:\"0\";s:19:\"alwaysShowPageLinks\";s:1:\"1\";s:9:\"mediaList\";s:0:\"\";s:11:\"rootPidList\";s:0:\"\";s:10:\"page_links\";s:2:\"10\";s:18:\"detectDomainRcords\";s:1:\"0\";s:23:\"defaultFreeIndexUidList\";s:0:\"\";s:34:\"searchSkipExtendToSubpagesChecking\";s:1:\"0\";s:10:\"exactCount\";s:1:\"0\";s:30:\"forwardSearchWordsInResultLink\";s:1:\"0\";s:31:\"forwardSearchWordsInResultLink.\";a:1:{s:8:\"no_cache\";s:1:\"1\";}s:8:\"results.\";a:11:{s:14:\"titleCropAfter\";s:2:\"50\";s:18:\"titleCropSignifier\";s:3:\"...\";s:16:\"summaryCropAfter\";s:3:\"180\";s:20:\"summaryCropSignifier\";s:0:\"\";s:22:\"hrefInSummaryCropAfter\";s:2:\"60\";s:26:\"hrefInSummaryCropSignifier\";s:3:\"...\";s:19:\"markupSW_summaryMax\";s:3:\"300\";s:19:\"markupSW_postPreLgd\";s:2:\"60\";s:26:\"markupSW_postPreLgd_offset\";s:1:\"5\";s:16:\"markupSW_divider\";s:3:\"...\";s:17:\"markupSW_divider.\";a:1:{s:10:\"noTrimWrap\";s:5:\"| | |\";}}s:6:\"blind.\";a:10:{s:10:\"searchType\";s:1:\"0\";s:14:\"defaultOperand\";s:1:\"0\";s:8:\"sections\";s:1:\"0\";s:12:\"freeIndexUid\";s:1:\"1\";s:9:\"mediaType\";s:1:\"0\";s:9:\"sortOrder\";s:1:\"0\";s:5:\"group\";s:1:\"0\";s:11:\"languageUid\";s:1:\"0\";s:4:\"desc\";s:1:\"0\";s:15:\"numberOfResults\";s:1:\"0\";}s:15:\"defaultOptions.\";a:8:{s:14:\"defaultOperand\";s:1:\"0\";s:8:\"sections\";s:1:\"0\";s:12:\"freeIndexUid\";s:2:\"-1\";s:9:\"mediaType\";s:2:\"-1\";s:9:\"sortOrder\";s:9:\"rank_flag\";s:11:\"languageUid\";s:2:\"-1\";s:8:\"sortDesc\";s:1:\"1\";s:10:\"searchType\";s:1:\"1\";}}s:5:\"view.\";a:3:{s:18:\"templateRootPaths.\";a:3:{i:0;s:47:\"EXT:indexed_search/Resources/Private/Templates/\";i:10;s:48:\"{$plugin.tx_indexedsearch.view.templateRootPath}\";i:20;s:76:\"EXT:templatebootstrap/Resources/Private/Extensions/indexed_search/Templates/\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:46:\"EXT:indexed_search/Resources/Private/Partials/\";i:10;s:47:\"{$plugin.tx_indexedsearch.view.partialRootPath}\";i:20;s:75:\"EXT:templatebootstrap/Resources/Private/Extensions/indexed_search/Partials/\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:45:\"EXT:indexed_search/Resources/Private/Layouts/\";i:10;s:46:\"{$plugin.tx_indexedsearch.view.layoutRootPath}\";i:20;s:74:\"EXT:templatebootstrap/Resources/Private/Extensions/indexed_search/Layouts/\";}}s:12:\"_LOCAL_LANG.\";a:5:{s:3:\"de.\";a:3:{s:14:\"form_searchFor\";s:6:\"Suchen\";s:26:\"pi_list_browseresults_prev\";s:9:\"«zurück\";s:26:\"pi_list_browseresults_next\";s:11:\"vorwärts»\";}s:3:\"fr.\";a:3:{s:14:\"form_searchFor\";s:8:\"Chercher\";s:26:\"pi_list_browseresults_prev\";s:13:\"«précédent\";s:26:\"pi_list_browseresults_next\";s:9:\"suivant»\";}s:3:\"it.\";a:3:{s:14:\"form_searchFor\";s:7:\"Cercare\";s:26:\"pi_list_browseresults_prev\";s:10:\"«indietro\";s:26:\"pi_list_browseresults_next\";s:8:\"avanti»\";}s:3:\"en.\";a:3:{s:14:\"form_searchFor\";s:6:\"Search\";s:26:\"pi_list_browseresults_prev\";s:10:\"«previous\";s:26:\"pi_list_browseresults_next\";s:6:\"next»\";}s:8:\"default.\";a:3:{s:14:\"form_searchFor\";s:6:\"Suchen\";s:26:\"pi_list_browseresults_prev\";s:9:\"«zurück\";s:26:\"pi_list_browseresults_next\";s:11:\"vorwärts»\";}}}s:8:\"tx_form.\";a:4:{s:5:\"view.\";a:4:{s:16:\"elementPartials.\";a:23:{s:7:\"BUTTON.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:19:\"FlatElements/Button\";}}s:10:\"BUTTONTAG.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:22:\"FlatElements/ButtonTag\";}}s:9:\"CHECKBOX.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:21:\"FlatElements/Checkbox\";}}s:14:\"CHECKBOXGROUP.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:31:\"ContainerElements/Checkboxgroup\";}}s:15:\"CONTENTELEMENT.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:27:\"FlatElements/ContentElement\";}}s:9:\"FIELDSET.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:26:\"ContainerElements/Fieldset\";}}s:11:\"FILEUPLOAD.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:19:\"FlatElements/Upload\";}}s:5:\"FORM.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:22:\"ContainerElements/Form\";}}s:7:\"HEADER.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:19:\"FlatElements/Header\";}}s:7:\"HIDDEN.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:19:\"FlatElements/Hidden\";}}s:12:\"IMAGEBUTTON.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:24:\"FlatElements/Imagebutton\";}}s:6:\"INPUT.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:18:\"FlatElements/Input\";}}s:16:\"INPUTTYPEBUTTON.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:28:\"FlatElements/InputTypeButton\";}}s:6:\"LABEL.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:24:\"AdditionalElements/Label\";}}s:9:\"PASSWORD.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:21:\"FlatElements/Password\";}}s:6:\"RADIO.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:18:\"FlatElements/Radio\";}}s:11:\"RADIOGROUP.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:28:\"ContainerElements/Radiogroup\";}}s:6:\"RESET.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:18:\"FlatElements/Reset\";}}s:7:\"SELECT.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:19:\"FlatElements/Select\";}}s:7:\"SUBMIT.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:19:\"FlatElements/Submit\";}}s:9:\"TEXTAREA.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:21:\"FlatElements/Textarea\";}}s:10:\"TEXTBLOCK.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:22:\"FlatElements/Textblock\";}}s:9:\"TEXTLINE.\";a:1:{s:3:\"10.\";a:2:{s:11:\"displayName\";s:7:\"Default\";s:11:\"partialPath\";s:22:\"FlatElements/Textfield\";}}}s:18:\"templateRootPaths.\";a:2:{i:10;s:37:\"EXT:form/Resources/Private/Templates/\";i:20;s:66:\"EXT:templatebootstrap/Resources/Private/Extensions/form/Templates/\";}s:17:\"partialRootPaths.\";a:2:{i:10;s:36:\"EXT:form/Resources/Private/Partials/\";i:20;s:65:\"EXT:templatebootstrap/Resources/Private/Extensions/form/Partials/\";}s:16:\"layoutRootPaths.\";a:2:{i:10;s:35:\"EXT:form/Resources/Private/Layouts/\";i:20;s:64:\"EXT:templatebootstrap/Resources/Private/Extensions/form/Layouts/\";}}s:9:\"settings.\";a:3:{s:19:\"registeredElements.\";a:27:{s:6:\"BUTTON\";s:18:\"< .INPUTTYPEBUTTON\";s:7:\"BUTTON.\";a:1:{s:11:\"partialPath\";s:68:\"< plugin.tx_form.view.elementPartials.INPUTTYPEBUTTON.10.partialPath\";}s:10:\"BUTTONTAG.\";a:7:{s:15:\"htmlAttributes.\";a:21:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:9:\"autofocus\";i:210;s:8:\"disabled\";i:220;s:4:\"name\";i:230;s:4:\"type\";i:240;s:5:\"value\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:13:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:9:\"autofocus\";i:130;s:4:\"type\";}s:27:\"defaultHtmlAttributeValues.\";a:1:{s:4:\"type\";s:6:\"button\";}s:11:\"partialPath\";s:62:\"< plugin.tx_form.view.elementPartials.BUTTONTAG.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:9:\"CHECKBOX.\";a:7:{s:15:\"htmlAttributes.\";a:24:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:9:\"autofocus\";i:220;s:7:\"checked\";i:230;s:8:\"disabled\";i:240;s:4:\"name\";i:250;s:8:\"readonly\";i:260;s:8:\"required\";i:270;s:5:\"value\";}s:25:\"fixedHtmlAttributeValues.\";a:1:{s:4:\"type\";s:8:\"checkbox\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:12:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:7:\"checked\";}s:11:\"partialPath\";s:61:\"< plugin.tx_form.view.elementPartials.CHECKBOX.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"1\";s:13:\"visibleInMail\";s:1:\"1\";}s:13:\"CHECKBOXGROUP\";s:11:\"< .FIELDSET\";s:14:\"CHECKBOXGROUP.\";a:4:{s:11:\"partialPath\";s:66:\"< plugin.tx_form.view.elementPartials.CHECKBOXGROUP.10.partialPath\";s:19:\"childrenInheritName\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"1\";s:13:\"visibleInMail\";s:1:\"1\";}s:15:\"CONTENTELEMENT.\";a:4:{s:11:\"partialPath\";s:67:\"< plugin.tx_form.view.elementPartials.CONTENTELEMENT.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:9:\"FIELDSET.\";a:5:{s:15:\"htmlAttributes.\";a:17:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:200;s:8:\"disabled\";i:210;s:4:\"name\";}s:11:\"partialPath\";s:61:\"< plugin.tx_form.view.elementPartials.FIELDSET.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:11:\"FILEUPLOAD.\";a:7:{s:15:\"htmlAttributes.\";a:25:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:6:\"accept\";i:220;s:9:\"autofocus\";i:230;s:8:\"disabled\";i:240;s:8:\"multiple\";i:250;s:4:\"name\";i:260;s:8:\"readonly\";i:270;s:8:\"required\";i:280;s:5:\"value\";}s:25:\"fixedHtmlAttributeValues.\";a:1:{s:4:\"type\";s:4:\"file\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:12:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:8:\"multiple\";}s:11:\"partialPath\";s:63:\"< plugin.tx_form.view.elementPartials.FILEUPLOAD.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"1\";s:13:\"visibleInMail\";s:1:\"1\";}s:5:\"FORM.\";a:8:{s:17:\"compatibilityMode\";s:1:\"0\";s:9:\"themeName\";s:7:\"Default\";s:15:\"htmlAttributes.\";a:25:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:6:\"action\";i:210;s:6:\"accept\";i:220;s:14:\"accept-charset\";i:230;s:12:\"autocomplete\";i:240;s:7:\"enctype\";i:250;s:6:\"method\";i:260;s:4:\"name\";i:270;s:10:\"novalidate\";i:280;s:6:\"target\";}s:27:\"defaultHtmlAttributeValues.\";a:2:{s:7:\"enctype\";s:19:\"multipart/form-data\";s:6:\"method\";s:4:\"post\";}s:24:\"fixedHtmlAttributeValues\";s:0:\"\";s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:11:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"enctype\";i:100;s:6:\"method\";i:110;s:4:\"name\";}s:11:\"partialPath\";s:57:\"< plugin.tx_form.view.elementPartials.FORM.10.partialPath\";s:27:\"viewHelperDefaultArguments.\";a:3:{s:10:\"arguments.\";a:0:{}s:17:\"additionalParams.\";a:0:{}s:37:\"argumentsToBeExcludedFromQueryString.\";a:0:{}}}s:7:\"HEADER.\";a:4:{s:11:\"partialPath\";s:59:\"< plugin.tx_form.view.elementPartials.HEADER.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:7:\"HIDDEN.\";a:7:{s:15:\"htmlAttributes.\";a:19:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:4:\"name\";i:220;s:5:\"value\";}s:25:\"fixedHtmlAttributeValues.\";a:1:{s:4:\"type\";s:6:\"hidden\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:11:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";}s:11:\"partialPath\";s:59:\"< plugin.tx_form.view.elementPartials.HIDDEN.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:12:\"IMAGEBUTTON.\";a:7:{s:15:\"htmlAttributes.\";a:40:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:200;s:4:\"type\";i:210;s:6:\"accept\";i:220;s:12:\"autocomplete\";i:230;s:3:\"alt\";i:240;s:9:\"autofocus\";i:250;s:7:\"checked\";i:260;s:8:\"disabled\";i:270;s:6:\"height\";i:280;s:9:\"inputmode\";i:290;s:4:\"list\";i:300;s:3:\"max\";i:310;s:9:\"maxlength\";i:320;s:3:\"min\";i:330;s:9:\"minlength\";i:340;s:8:\"multiple\";i:350;s:4:\"name\";i:360;s:7:\"pattern\";i:370;s:11:\"placeholder\";i:380;s:8:\"readonly\";i:390;s:8:\"required\";i:400;s:4:\"size\";i:410;s:3:\"src\";i:420;s:4:\"step\";i:430;s:5:\"value\";i:440;s:5:\"width\";}s:25:\"fixedHtmlAttributeValues.\";a:1:{s:4:\"type\";s:5:\"image\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:18:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:9:\"autofocus\";i:130;s:9:\"maxlength\";i:140;s:4:\"size\";i:150;s:11:\"placeholder\";i:160;s:7:\"pattern\";i:170;s:8:\"required\";i:180;s:4:\"type\";}s:11:\"partialPath\";s:64:\"< plugin.tx_form.view.elementPartials.IMAGEBUTTON.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:6:\"INPUT.\";a:7:{s:15:\"htmlAttributes.\";a:39:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:6:\"accept\";i:220;s:12:\"autocomplete\";i:230;s:9:\"autofocus\";i:240;s:7:\"checked\";i:250;s:8:\"disabled\";i:260;s:4:\"list\";i:270;s:9:\"inputmode\";i:280;s:3:\"max\";i:290;s:9:\"maxlength\";i:300;s:3:\"min\";i:310;s:9:\"minlength\";i:320;s:8:\"multiple\";i:330;s:4:\"name\";i:340;s:7:\"pattern\";i:350;s:11:\"placeholder\";i:360;s:8:\"readonly\";i:370;s:8:\"required\";i:380;s:4:\"size\";i:390;s:3:\"src\";i:400;s:4:\"step\";i:410;s:5:\"value\";i:420;s:5:\"width\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:18:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:9:\"autofocus\";i:130;s:9:\"maxlength\";i:140;s:4:\"size\";i:150;s:11:\"placeholder\";i:160;s:7:\"pattern\";i:170;s:8:\"required\";i:180;s:4:\"type\";}s:27:\"defaultHtmlAttributeValues.\";a:1:{s:4:\"type\";s:4:\"text\";}s:11:\"partialPath\";s:58:\"< plugin.tx_form.view.elementPartials.INPUT.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"1\";s:13:\"visibleInMail\";s:1:\"1\";}s:16:\"INPUTTYPEBUTTON.\";a:7:{s:15:\"htmlAttributes.\";a:21:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:9:\"autofocus\";i:220;s:8:\"disabled\";i:230;s:4:\"name\";i:240;s:5:\"value\";}s:25:\"fixedHtmlAttributeValues.\";a:1:{s:4:\"type\";s:6:\"button\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:12:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:4:\"type\";}s:11:\"partialPath\";s:68:\"< plugin.tx_form.view.elementPartials.INPUTTYPEBUTTON.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:9:\"OPTGROUP.\";a:0:{}s:7:\"OPTION.\";a:0:{}s:9:\"PASSWORD.\";a:7:{s:15:\"htmlAttributes.\";a:29:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:12:\"autocomplete\";i:220;s:9:\"autofocus\";i:230;s:8:\"disabled\";i:240;s:9:\"maxlength\";i:250;s:9:\"minlength\";i:260;s:4:\"name\";i:270;s:7:\"pattern\";i:280;s:11:\"placeholder\";i:290;s:8:\"readonly\";i:300;s:8:\"required\";i:310;s:4:\"size\";i:320;s:5:\"value\";}s:25:\"fixedHtmlAttributeValues.\";a:1:{s:4:\"type\";s:8:\"password\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:13:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:9:\"maxlength\";i:130;s:4:\"size\";}s:11:\"partialPath\";s:61:\"< plugin.tx_form.view.elementPartials.PASSWORD.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:6:\"RADIO.\";a:7:{s:15:\"htmlAttributes.\";a:24:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:9:\"autofocus\";i:220;s:7:\"checked\";i:230;s:8:\"disabled\";i:240;s:4:\"name\";i:250;s:8:\"readonly\";i:260;s:8:\"required\";i:270;s:5:\"value\";}s:25:\"fixedHtmlAttributeValues.\";a:1:{s:4:\"type\";s:5:\"radio\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:13:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:8:\"multiple\";i:130;s:7:\"checked\";}s:11:\"partialPath\";s:58:\"< plugin.tx_form.view.elementPartials.RADIO.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"1\";s:13:\"visibleInMail\";s:1:\"1\";}s:10:\"RADIOGROUP\";s:11:\"< .FIELDSET\";s:11:\"RADIOGROUP.\";a:4:{s:11:\"partialPath\";s:63:\"< plugin.tx_form.view.elementPartials.RADIOGROUP.10.partialPath\";s:19:\"childrenInheritName\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"1\";s:13:\"visibleInMail\";s:1:\"1\";}s:6:\"RESET.\";a:7:{s:15:\"htmlAttributes.\";a:21:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:9:\"autofocus\";i:220;s:8:\"disabled\";i:230;s:4:\"name\";i:240;s:5:\"value\";}s:25:\"fixedHtmlAttributeValues.\";a:1:{s:4:\"type\";s:5:\"reset\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:13:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:9:\"autofocus\";i:130;s:4:\"type\";}s:11:\"partialPath\";s:58:\"< plugin.tx_form.view.elementPartials.RESET.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:7:\"SELECT.\";a:6:{s:15:\"htmlAttributes.\";a:22:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:9:\"autofocus\";i:210;s:8:\"disabled\";i:220;s:8:\"multiple\";i:230;s:4:\"name\";i:240;s:8:\"required\";i:250;s:4:\"size\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:13:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:8:\"multiple\";i:130;s:4:\"size\";}s:11:\"partialPath\";s:59:\"< plugin.tx_form.view.elementPartials.SELECT.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"1\";s:13:\"visibleInMail\";s:1:\"1\";}s:7:\"SUBMIT.\";a:7:{s:15:\"htmlAttributes.\";a:21:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:9:\"autofocus\";i:220;s:8:\"disabled\";i:230;s:4:\"name\";i:240;s:5:\"value\";}s:25:\"fixedHtmlAttributeValues.\";a:1:{s:4:\"type\";s:6:\"submit\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:10:{i:10;s:3:\"dir\";i:20;s:2:\"id\";i:30;s:4:\"lang\";i:40;s:5:\"style\";i:50;s:5:\"title\";i:60;s:9:\"accesskey\";i:70;s:8:\"tabindex\";i:80;s:7:\"onclick\";i:90;s:4:\"name\";i:100;s:5:\"value\";}s:11:\"partialPath\";s:59:\"< plugin.tx_form.view.elementPartials.SUBMIT.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:9:\"TEXTAREA.\";a:6:{s:15:\"htmlAttributes.\";a:31:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:9:\"autofocus\";i:210;s:4:\"cols\";i:220;s:8:\"disabled\";i:230;s:9:\"inputmode\";i:240;s:9:\"maxlength\";i:250;s:9:\"minlength\";i:260;s:4:\"name\";i:270;s:11:\"placeholder\";i:280;s:8:\"readonly\";i:290;s:8:\"required\";i:300;s:4:\"rows\";i:310;s:18:\"selectionDirection\";i:320;s:12:\"selectionEnd\";i:330;s:14:\"selectionStart\";i:340;s:4:\"wrap\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:15:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:9:\"autofocus\";i:130;s:4:\"rows\";i:140;s:4:\"cols\";i:150;s:11:\"placeholder\";}s:11:\"partialPath\";s:61:\"< plugin.tx_form.view.elementPartials.TEXTAREA.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"1\";s:13:\"visibleInMail\";s:1:\"1\";}s:10:\"TEXTBLOCK.\";a:4:{s:11:\"partialPath\";s:62:\"< plugin.tx_form.view.elementPartials.TEXTBLOCK.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"0\";s:13:\"visibleInMail\";s:1:\"0\";}s:9:\"TEXTLINE.\";a:7:{s:15:\"htmlAttributes.\";a:31:{i:10;s:2:\"id\";i:20;s:5:\"class\";i:30;s:9:\"accesskey\";i:40;s:15:\"contenteditable\";i:50;s:11:\"contextmenu\";i:60;s:3:\"dir\";i:70;s:9:\"draggable\";i:80;s:8:\"dropzone\";i:90;s:6:\"hidden\";i:100;s:4:\"lang\";i:110;s:10:\"spellcheck\";i:120;s:5:\"style\";i:130;s:8:\"tabindex\";i:140;s:5:\"title\";i:150;s:6:\"data-*\";i:160;s:9:\"translate\";i:200;s:4:\"type\";i:210;s:12:\"autocomplete\";i:220;s:9:\"autofocus\";i:230;s:8:\"disabled\";i:240;s:9:\"inputmode\";i:250;s:4:\"list\";i:260;s:9:\"maxlength\";i:270;s:9:\"minlength\";i:280;s:4:\"name\";i:290;s:7:\"pattern\";i:300;s:11:\"placeholder\";i:310;s:8:\"readonly\";i:320;s:8:\"required\";i:330;s:4:\"size\";i:340;s:5:\"value\";}s:42:\"htmlAttributesUsedByTheViewHelperDirectly.\";a:18:{i:10;s:5:\"class\";i:20;s:3:\"dir\";i:30;s:2:\"id\";i:40;s:4:\"lang\";i:50;s:5:\"style\";i:60;s:5:\"title\";i:70;s:9:\"accesskey\";i:80;s:8:\"tabindex\";i:90;s:7:\"onclick\";i:100;s:4:\"name\";i:110;s:5:\"value\";i:120;s:9:\"autofocus\";i:130;s:9:\"maxlength\";i:140;s:4:\"size\";i:150;s:11:\"placeholder\";i:160;s:7:\"pattern\";i:170;s:8:\"required\";i:180;s:4:\"type\";}s:27:\"defaultHtmlAttributeValues.\";a:1:{s:4:\"type\";s:4:\"text\";}s:11:\"partialPath\";s:61:\"< plugin.tx_form.view.elementPartials.TEXTLINE.10.partialPath\";s:19:\"visibleInShowAction\";s:1:\"1\";s:27:\"visibleInConfirmationAction\";s:1:\"1\";s:13:\"visibleInMail\";s:1:\"1\";}}s:18:\"registeredFilters.\";a:12:{s:11:\"alphabetic.\";a:2:{s:11:\"displayName\";s:10:\"Alphabetic\";s:9:\"className\";s:45:\"TYPO3\\CMS\\Form\\Domain\\Filter\\AlphabeticFilter\";}s:13:\"alphanumeric.\";a:2:{s:11:\"displayName\";s:12:\"Alphanumeric\";s:9:\"className\";s:47:\"TYPO3\\CMS\\Form\\Domain\\Filter\\AlphanumericFilter\";}s:9:\"currency.\";a:2:{s:11:\"displayName\";s:8:\"Currency\";s:9:\"className\";s:43:\"TYPO3\\CMS\\Form\\Domain\\Filter\\CurrencyFilter\";}s:6:\"digit.\";a:2:{s:11:\"displayName\";s:5:\"Digit\";s:9:\"className\";s:40:\"TYPO3\\CMS\\Form\\Domain\\Filter\\DigitFilter\";}s:8:\"integer.\";a:2:{s:11:\"displayName\";s:7:\"Integer\";s:9:\"className\";s:42:\"TYPO3\\CMS\\Form\\Domain\\Filter\\IntegerFilter\";}s:10:\"lowercase.\";a:2:{s:11:\"displayName\";s:9:\"Lowercase\";s:9:\"className\";s:44:\"TYPO3\\CMS\\Form\\Domain\\Filter\\LowerCaseFilter\";}s:7:\"regexp.\";a:2:{s:11:\"displayName\";s:18:\"Regular Expression\";s:9:\"className\";s:41:\"TYPO3\\CMS\\Form\\Domain\\Filter\\RegExpFilter\";}s:10:\"removexss.\";a:2:{s:11:\"displayName\";s:10:\"Remove XSS\";s:9:\"className\";s:44:\"TYPO3\\CMS\\Form\\Domain\\Filter\\RemoveXssFilter\";}s:14:\"stripnewlines.\";a:2:{s:11:\"displayName\";s:15:\"Strip New Lines\";s:9:\"className\";s:48:\"TYPO3\\CMS\\Form\\Domain\\Filter\\StripNewLinesFilter\";}s:10:\"titlecase.\";a:2:{s:11:\"displayName\";s:9:\"Titlecase\";s:9:\"className\";s:44:\"TYPO3\\CMS\\Form\\Domain\\Filter\\TitleCaseFilter\";}s:5:\"trim.\";a:2:{s:11:\"displayName\";s:4:\"Trim\";s:9:\"className\";s:39:\"TYPO3\\CMS\\Form\\Domain\\Filter\\TrimFilter\";}s:10:\"uppercase.\";a:2:{s:11:\"displayName\";s:9:\"Uppercase\";s:9:\"className\";s:44:\"TYPO3\\CMS\\Form\\Domain\\Filter\\UpperCaseFilter\";}}s:21:\"registeredValidators.\";a:20:{s:11:\"alphabetic.\";a:2:{s:11:\"displayName\";s:10:\"Alphabetic\";s:9:\"className\";s:51:\"TYPO3\\CMS\\Form\\Domain\\Validator\\AlphabeticValidator\";}s:13:\"alphanumeric.\";a:2:{s:11:\"displayName\";s:12:\"Alphanumeric\";s:9:\"className\";s:53:\"TYPO3\\CMS\\Form\\Domain\\Validator\\AlphanumericValidator\";}s:8:\"between.\";a:2:{s:11:\"displayName\";s:7:\"Between\";s:9:\"className\";s:48:\"TYPO3\\CMS\\Form\\Domain\\Validator\\BetweenValidator\";}s:5:\"date.\";a:2:{s:11:\"displayName\";s:4:\"Date\";s:9:\"className\";s:45:\"TYPO3\\CMS\\Form\\Domain\\Validator\\DateValidator\";}s:6:\"digit.\";a:2:{s:11:\"displayName\";s:5:\"Digit\";s:9:\"className\";s:46:\"TYPO3\\CMS\\Form\\Domain\\Validator\\DigitValidator\";}s:6:\"email.\";a:2:{s:11:\"displayName\";s:13:\"Email address\";s:9:\"className\";s:46:\"TYPO3\\CMS\\Form\\Domain\\Validator\\EmailValidator\";}s:7:\"equals.\";a:2:{s:11:\"displayName\";s:6:\"Equals\";s:9:\"className\";s:47:\"TYPO3\\CMS\\Form\\Domain\\Validator\\EqualsValidator\";}s:17:\"fileallowedtypes.\";a:2:{s:11:\"displayName\";s:26:\"Allowed mimetypes for file\";s:9:\"className\";s:57:\"TYPO3\\CMS\\Form\\Domain\\Validator\\FileAllowedTypesValidator\";}s:16:\"filemaximumsize.\";a:2:{s:11:\"displayName\";s:29:\"Maximum size for file (bytes)\";s:9:\"className\";s:56:\"TYPO3\\CMS\\Form\\Domain\\Validator\\FileMaximumSizeValidator\";}s:16:\"fileminimumsize.\";a:2:{s:11:\"displayName\";s:29:\"Minimum size for file (bytes)\";s:9:\"className\";s:56:\"TYPO3\\CMS\\Form\\Domain\\Validator\\FileMinimumSizeValidator\";}s:6:\"float.\";a:2:{s:11:\"displayName\";s:5:\"Float\";s:9:\"className\";s:46:\"TYPO3\\CMS\\Form\\Domain\\Validator\\FloatValidator\";}s:12:\"greaterthan.\";a:2:{s:11:\"displayName\";s:12:\"Greater than\";s:9:\"className\";s:52:\"TYPO3\\CMS\\Form\\Domain\\Validator\\GreaterThanValidator\";}s:8:\"inarray.\";a:2:{s:11:\"displayName\";s:8:\"In array\";s:9:\"className\";s:48:\"TYPO3\\CMS\\Form\\Domain\\Validator\\InArrayValidator\";}s:8:\"integer.\";a:2:{s:11:\"displayName\";s:7:\"Integer\";s:9:\"className\";s:48:\"TYPO3\\CMS\\Form\\Domain\\Validator\\IntegerValidator\";}s:3:\"ip.\";a:2:{s:11:\"displayName\";s:10:\"Ip address\";s:9:\"className\";s:43:\"TYPO3\\CMS\\Form\\Domain\\Validator\\IpValidator\";}s:7:\"length.\";a:2:{s:11:\"displayName\";s:6:\"Length\";s:9:\"className\";s:47:\"TYPO3\\CMS\\Form\\Domain\\Validator\\LengthValidator\";}s:9:\"lessthan.\";a:2:{s:11:\"displayName\";s:9:\"Less than\";s:9:\"className\";s:49:\"TYPO3\\CMS\\Form\\Domain\\Validator\\LessThanValidator\";}s:7:\"regexp.\";a:2:{s:11:\"displayName\";s:18:\"Regular Expression\";s:9:\"className\";s:47:\"TYPO3\\CMS\\Form\\Domain\\Validator\\RegExpValidator\";}s:9:\"required.\";a:2:{s:11:\"displayName\";s:8:\"Required\";s:9:\"className\";s:49:\"TYPO3\\CMS\\Form\\Domain\\Validator\\RequiredValidator\";}s:4:\"uri.\";a:2:{s:11:\"displayName\";s:27:\"Uniform Resource Identifier\";s:9:\"className\";s:44:\"TYPO3\\CMS\\Form\\Domain\\Validator\\UriValidator\";}}}s:9:\"features.\";a:1:{s:20:\"skipDefaultArguments\";s:1:\"1\";}s:19:\"registeredElements.\";a:1:{s:24:\"defaultModelDescription.\";a:1:{s:17:\"compatibilityMode\";s:1:\"0\";}}}s:14:\"tx_felogin_pi1\";s:8:\"USER_INT\";s:15:\"tx_felogin_pi1.\";a:46:{s:8:\"userFunc\";s:58:\"TYPO3\\CMS\\Felogin\\Controller\\FrontendLoginController->main\";s:10:\"storagePid\";s:0:\"\";s:9:\"recursive\";s:0:\"\";s:12:\"templateFile\";s:58:\"EXT:felogin/Resources/Private/Templates/FrontendLogin.html\";s:14:\"feloginBaseURL\";s:0:\"\";s:22:\"wrapContentInBaseClass\";s:1:\"1\";s:11:\"linkConfig.\";a:2:{s:6:\"target\";s:0:\"\";s:10:\"ATagParams\";s:14:\"rel=\"nofollow\"\";}s:15:\"preserveGETvars\";s:3:\"all\";s:22:\"showForgotPasswordLink\";s:0:\"\";s:14:\"showPermaLogin\";s:0:\"\";s:23:\"forgotLinkHashValidTime\";s:2:\"12\";s:20:\"newPasswordMinLength\";s:1:\"6\";s:22:\"welcomeHeader_stdWrap.\";a:2:{s:4:\"wrap\";s:10:\"<h3>|</h3>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:23:\"welcomeMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:22:\"successHeader_stdWrap.\";a:2:{s:4:\"wrap\";s:10:\"<h3>|</h3>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:23:\"successMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:21:\"logoutHeader_stdWrap.\";a:2:{s:4:\"wrap\";s:10:\"<h3>|</h3>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:22:\"logoutMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:20:\"errorHeader_stdWrap.\";a:2:{s:4:\"wrap\";s:10:\"<h3>|</h3>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:21:\"errorMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:21:\"forgotHeader_stdWrap.\";a:2:{s:4:\"wrap\";s:10:\"<h3>|</h3>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:22:\"forgotMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:27:\"forgotErrorMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:43:\"forgotResetMessageEmailSentMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:38:\"changePasswordNotValidMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:38:\"changePasswordTooShortMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:38:\"changePasswordNotEqualMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:29:\"changePasswordHeader_stdWrap.\";a:2:{s:4:\"wrap\";s:10:\"<h3>|</h3>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:30:\"changePasswordMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:34:\"changePasswordDoneMessage_stdWrap.\";a:2:{s:4:\"wrap\";s:12:\"<div>|</div>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:22:\"cookieWarning_stdWrap.\";a:2:{s:4:\"wrap\";s:45:\"<p style=\"color:red; font-weight:bold;\">|</p>\";s:16:\"htmlSpecialChars\";s:1:\"1\";}s:11:\"userfields.\";a:1:{s:9:\"username.\";a:2:{s:16:\"htmlSpecialChars\";s:1:\"1\";s:4:\"wrap\";s:18:\"<strong>|</strong>\";}}s:12:\"redirectMode\";s:0:\"\";s:19:\"redirectFirstMethod\";s:0:\"\";s:17:\"redirectPageLogin\";s:0:\"\";s:22:\"redirectPageLoginError\";s:0:\"\";s:18:\"redirectPageLogout\";s:0:\"\";s:15:\"redirectDisable\";s:0:\"\";s:10:\"email_from\";s:0:\"\";s:14:\"email_fromName\";s:0:\"\";s:7:\"replyTo\";s:0:\"\";s:7:\"domains\";s:0:\"\";s:24:\"showLogoutFormAfterLogin\";s:0:\"\";s:10:\"dateFormat\";s:9:\"Y-m-d H:i\";s:43:\"exposeNonexistentUserInForgotPasswordDialog\";s:1:\"0\";s:18:\"_CSS_DEFAULT_STYLE\";s:48:\"		.tx-felogin-pi1 label {\n			display: block;\n		}\";}}s:4:\"page\";s:4:\"PAGE\";s:5:\"page.\";a:11:{s:5:\"meta.\";a:25:{s:8:\"viewport\";s:37:\"width=device-width, initial-scale=1.0\";s:9:\"keywords.\";a:2:{s:5:\"field\";s:8:\"keywords\";s:9:\"override.\";a:1:{s:4:\"data\";s:21:\"register:newsKeywords\";}}s:12:\"description.\";a:1:{s:5:\"field\";s:11:\"description\";}s:9:\"abstract.\";a:1:{s:5:\"field\";s:8:\"abstract\";}s:6:\"robots\";s:16:\"NOINDEX.NOFOLLOW\";s:7:\"robots.\";a:2:{s:8:\"override\";s:12:\"INDEX,FOLLOW\";s:9:\"override.\";a:1:{s:3:\"if.\";a:3:{s:8:\"isFalse.\";a:1:{s:5:\"field\";s:9:\"no_search\";}s:8:\"isInList\";s:5:\"local\";s:5:\"value\";s:16:\"local,production\";}}}s:8:\"og:title\";s:4:\"TEXT\";s:9:\"og:title.\";a:2:{s:5:\"field\";s:5:\"title\";s:9:\"attribute\";s:8:\"property\";}s:12:\"og:site_name\";s:9:\"Muster AG\";s:13:\"og:site_name.\";a:1:{s:9:\"attribute\";s:8:\"property\";}s:7:\"og:type\";s:7:\"website\";s:8:\"og:type.\";a:1:{s:9:\"attribute\";s:8:\"property\";}s:9:\"og:locale\";s:5:\"de_CH\";s:10:\"og:locale.\";a:1:{s:9:\"attribute\";s:8:\"property\";}s:15:\"og:description.\";a:2:{s:5:\"field\";s:11:\"description\";s:9:\"attribute\";s:8:\"property\";}s:7:\"og:url.\";a:2:{s:9:\"typolink.\";a:5:{s:10:\"parameter.\";a:1:{s:5:\"field\";s:3:\"uid\";}s:14:\"addQueryString\";s:1:\"1\";s:12:\"useCacheHash\";s:1:\"0\";s:10:\"returnLast\";s:3:\"url\";s:16:\"forceAbsoluteUrl\";s:1:\"1\";}s:9:\"attribute\";s:8:\"property\";}s:8:\"og:image\";s:4:\"TEXT\";s:9:\"og:image.\";a:3:{s:7:\"cObject\";s:5:\"FILES\";s:8:\"cObject.\";a:4:{s:8:\"stdWrap.\";a:1:{s:7:\"listNum\";s:1:\"0\";}s:11:\"references.\";a:1:{s:4:\"data\";s:19:\"levelmedia:-1,slide\";}s:9:\"renderObj\";s:3:\"COA\";s:10:\"renderObj.\";a:2:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:3:{s:3:\"if.\";a:2:{s:6:\"equals\";s:1:\"2\";s:6:\"value.\";a:1:{s:4:\"data\";s:17:\"file:current:type\";}}s:9:\"typolink.\";a:3:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:22:\"file:current:publicUrl\";}s:10:\"returnLast\";s:3:\"url\";s:16:\"forceAbsoluteUrl\";s:1:\"1\";}s:4:\"wrap\";s:2:\"|,\";}}}s:9:\"attribute\";s:8:\"property\";}s:12:\"twitter:card\";s:7:\"summary\";s:13:\"twitter:title\";s:4:\"TEXT\";s:14:\"twitter:title.\";a:1:{s:5:\"field\";s:5:\"title\";}s:20:\"twitter:description.\";a:1:{s:5:\"field\";s:11:\"description\";}s:13:\"twitter:image\";s:4:\"TEXT\";s:14:\"twitter:image.\";a:2:{s:7:\"cObject\";s:5:\"FILES\";s:8:\"cObject.\";a:4:{s:8:\"stdWrap.\";a:1:{s:7:\"listNum\";s:1:\"0\";}s:11:\"references.\";a:1:{s:4:\"data\";s:19:\"levelmedia:-1,slide\";}s:9:\"renderObj\";s:3:\"COA\";s:10:\"renderObj.\";a:2:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:3:{s:3:\"if.\";a:2:{s:6:\"equals\";s:1:\"2\";s:6:\"value.\";a:1:{s:4:\"data\";s:17:\"file:current:type\";}}s:9:\"typolink.\";a:3:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:22:\"file:current:publicUrl\";}s:10:\"returnLast\";s:3:\"url\";s:16:\"forceAbsoluteUrl\";s:1:\"1\";}s:4:\"wrap\";s:2:\"|,\";}}}}s:12:\"twitter:url.\";a:1:{s:9:\"typolink.\";a:5:{s:10:\"parameter.\";a:1:{s:5:\"field\";s:3:\"uid\";}s:14:\"addQueryString\";s:1:\"1\";s:12:\"useCacheHash\";s:1:\"0\";s:10:\"returnLast\";s:3:\"url\";s:16:\"forceAbsoluteUrl\";s:1:\"1\";}}}s:14:\"bodyTagCObject\";s:3:\"COA\";s:15:\"bodyTagCObject.\";a:7:{s:8:\"stdWrap.\";a:1:{s:4:\"wrap\";s:16:\"<body class=\"|\">\";}i:10;s:4:\"TEXT\";s:3:\"10.\";a:3:{s:5:\"value\";s:16:\"page-{field:uid}\";s:10:\"insertData\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"|| |\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:3:{s:4:\"data\";s:47:\"levelfield:-1, backend_layout_next_level, slide\";s:9:\"override.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"split.\";a:2:{s:5:\"token\";s:2:\"__\";s:9:\"returnKey\";s:1:\"1\";}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:13:\"fieldRequired\";s:6:\"layout\";s:5:\"value\";s:21:\"layout-{field:layout}\";s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:10;s:13:\"FLUIDTEMPLATE\";s:3:\"10.\";a:7:{s:18:\"templateRootPaths.\";a:1:{i:10;s:54:\"EXT:templatebootstrap/Resources/Private/Templates/Page\";}s:17:\"partialRootPaths.\";a:1:{i:10;s:48:\"EXT:templatebootstrap/Resources/Private/Partials\";}s:16:\"layoutRootPaths.\";a:1:{i:10;s:47:\"EXT:templatebootstrap/Resources/Private/Layouts\";}s:12:\"templateName\";s:4:\"TEXT\";s:13:\"templateName.\";a:1:{s:8:\"stdWrap.\";a:3:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:4:\"data\";s:10:\"pagelayout\";s:6:\"split.\";a:2:{s:5:\"token\";s:2:\"__\";s:9:\"returnKey\";s:1:\"1\";}}s:7:\"ifEmpty\";s:12:\"SingleColumn\";}}s:9:\"settings.\";a:2:{s:8:\"company.\";a:6:{s:4:\"name\";s:9:\"Muster AG\";s:6:\"street\";s:16:\"Musterstrasse 44\";s:5:\"pobox\";s:0:\"\";s:4:\"city\";s:17:\"9999 Musterhausen\";s:5:\"phone\";s:13:\"099 999 99 99\";s:5:\"email\";s:19:\"muster@musterag.com\";}s:5:\"site.\";a:1:{s:3:\"url\";s:31:\"http://templatebootstrap.local/\";}}s:10:\"variables.\";a:2:{s:9:\"pagetitle\";s:4:\"TEXT\";s:10:\"pagetitle.\";a:1:{s:4:\"data\";s:10:\"page:title\";}}}s:11:\"includeCSS.\";a:6:{s:10:\"normalizer\";s:69:\"EXT:templatebootstrap/Resources/Public/Template/css/lib/normalize.css\";s:11:\"normalizer.\";a:1:{s:5:\"media\";s:12:\"screen,print\";}s:7:\"mainCSS\";s:59:\"EXT:templatebootstrap/Resources/Public/Template/css/app.css\";s:8:\"mainCSS.\";a:1:{s:5:\"media\";s:12:\"screen,print\";}s:8:\"fancybox\";s:86:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/fancybox/jquery.fancybox.css\";s:23:\"fancyboxThumbnailHelper\";s:93:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/fancybox/jquery.fancybox-thumbs.css\";}s:14:\"includeJSLibs.\";a:1:{s:9:\"modernizr\";s:73:\"EXT:templatebootstrap/Resources/Public/Template/js/modernizr/modernizr.js\";}s:20:\"includeJSFooterlibs.\";a:15:{s:6:\"jQuery\";s:78:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/jquery-1.11.3.min.js\";s:12:\"jQueryCookie\";s:74:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/jquery.cookie.js\";s:19:\"jQueryDoubleTapMenu\";s:81:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/jquery.doubleTapMenu.js\";s:13:\"jQueryToggler\";s:75:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/jquery.toggler.js\";s:10:\"jQueryMove\";s:78:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/jquery.event.move.js\";s:11:\"jQuerySwipe\";s:79:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/jquery.event.swipe.js\";s:17:\"jQueryPlaceholder\";s:79:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/jquery.placeholder.js\";s:14:\"jQueryFancybox\";s:93:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/fancybox/jquery.fancybox_patched.js\";s:25:\"jQueryFancyboxMediahelper\";s:91:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/fancybox/jquery.fancybox-media.js\";s:29:\"jQueryFancyboxThumbnailHelper\";s:92:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/fancybox/jquery.fancybox-thumbs.js\";s:10:\"foundation\";s:75:\"EXT:templatebootstrap/Resources/Public/Template/js/foundation/foundation.js\";s:18:\"foundationClearing\";s:84:\"EXT:templatebootstrap/Resources/Public/Template/js/foundation/foundation.clearing.js\";s:19:\"foundationOffcanvas\";s:85:\"EXT:templatebootstrap/Resources/Public/Template/js/foundation/foundation.offcanvas.js\";s:11:\"slickslider\";s:72:\"EXT:templatebootstrap/Resources/Public/Template/js/jquery/slick/slick.js\";s:9:\"fastclick\";s:73:\"EXT:templatebootstrap/Resources/Public/Template/js/fastclick/fastclick.js\";}s:16:\"includeJSFooter.\";a:1:{s:6:\"mainJS\";s:57:\"EXT:templatebootstrap/Resources/Public/Template/js/app.js\";}s:11:\"footerData.\";a:2:{i:10;s:3:\"COA\";s:3:\"10.\";a:5:{s:4:\"wrap\";s:74:\"            <!--[if lt IE 9]>\r\n               |\r\n            <![endif]-->\r\";i:10;s:4:\"TEXT\";s:3:\"10.\";a:3:{s:5:\"value\";s:80:\"/typo3conf/ext/templatebootstrap/Resources/Public/Template/js/respond/respond.js\";s:4:\"wrap\";s:25:\"<script src=\"|\"></script>\";s:10:\"insertData\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:3:{s:5:\"value\";s:76:\"/typo3conf/ext/templatebootstrap/Resources/Public/Template/js/rem/rem.min.js\";s:4:\"wrap\";s:25:\"<script src=\"|\"></script>\";s:10:\"insertData\";s:1:\"1\";}}}s:11:\"headerData.\";a:2:{i:20;s:3:\"COA\";s:3:\"20.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:1:{s:11:\"iconRelPath\";s:29:\"Resources/Private/LogoSources\";}i:20;s:3:\"COA\";s:3:\"20.\";a:32:{i:10;s:3:\"COA\";s:3:\"10.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:2:\"72\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:34:\"{$site.icons.1.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:28:\"{$site.icons.1.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:31:\"{$site.icons.1.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:16:\"apple-touch-icon\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:36:\"{$site.icons.1.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:20;s:3:\"COA\";s:3:\"20.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:3:\"114\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:34:\"{$site.icons.2.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:28:\"{$site.icons.2.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:31:\"{$site.icons.2.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:16:\"apple-touch-icon\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:36:\"{$site.icons.2.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:30;s:3:\"COA\";s:3:\"30.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:3:\"144\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:34:\"{$site.icons.3.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:28:\"{$site.icons.3.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:31:\"{$site.icons.3.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:16:\"apple-touch-icon\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:36:\"{$site.icons.3.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:40;s:3:\"COA\";s:3:\"40.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:3:\"180\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:34:\"{$site.icons.4.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:28:\"{$site.icons.4.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:31:\"{$site.icons.4.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:16:\"apple-touch-icon\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:36:\"{$site.icons.4.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:50;s:3:\"COA\";s:3:\"50.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:2:\"32\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:34:\"{$site.icons.5.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:28:\"{$site.icons.5.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:31:\"{$site.icons.5.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:28:\"{$site.icons.5.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:16:\"type=\"image/png\"\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:60;s:3:\"COA\";s:3:\"60.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:2:\"96\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:34:\"{$site.icons.6.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:28:\"{$site.icons.6.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:31:\"{$site.icons.6.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:28:\"{$site.icons.6.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:16:\"type=\"image/png\"\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:70;s:3:\"COA\";s:3:\"70.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:3:\"192\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:34:\"{$site.icons.7.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:28:\"{$site.icons.7.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:31:\"{$site.icons.7.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:28:\"{$site.icons.7.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:16:\"type=\"image/png\"\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:80;s:3:\"COA\";s:3:\"80.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:3:\"256\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:11:\"icon-larger\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:28:\"{$site.icons.8.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:31:\"{$site.icons.8.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:28:\"{$site.icons.8.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:16:\"type=\"image/png\"\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:90;s:3:\"COA\";s:3:\"90.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:20:\"{$site.icons.9.size}\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:34:\"{$site.icons.9.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:28:\"{$site.icons.9.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:31:\"{$site.icons.9.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:28:\"{$site.icons.9.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:36:\"{$site.icons.9.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:100;s:3:\"COA\";s:4:\"100.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:21:\"{$site.icons.10.size}\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:35:\"{$site.icons.10.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:29:\"{$site.icons.10.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:32:\"{$site.icons.10.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:29:\"{$site.icons.10.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:37:\"{$site.icons.10.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:110;s:3:\"COA\";s:4:\"110.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:21:\"{$site.icons.11.size}\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:35:\"{$site.icons.11.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:29:\"{$site.icons.11.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:32:\"{$site.icons.11.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:29:\"{$site.icons.11.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:37:\"{$site.icons.11.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:120;s:3:\"COA\";s:4:\"120.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:21:\"{$site.icons.12.size}\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:35:\"{$site.icons.12.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:29:\"{$site.icons.12.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:32:\"{$site.icons.12.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:29:\"{$site.icons.12.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:37:\"{$site.icons.12.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:130;s:3:\"COA\";s:4:\"130.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:21:\"{$site.icons.13.size}\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:35:\"{$site.icons.13.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:29:\"{$site.icons.13.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:32:\"{$site.icons.13.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:29:\"{$site.icons.13.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:37:\"{$site.icons.13.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:140;s:3:\"COA\";s:4:\"140.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:21:\"{$site.icons.14.size}\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:35:\"{$site.icons.14.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:29:\"{$site.icons.14.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:32:\"{$site.icons.14.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:29:\"{$site.icons.14.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:37:\"{$site.icons.14.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:150;s:3:\"COA\";s:4:\"150.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:12:{s:8:\"iconSize\";s:21:\"{$site.icons.15.size}\";s:9:\"iconSize.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:18:\"iconSourceFileName\";s:12:\"icon-default\";s:19:\"iconSourceFileName.\";a:2:{s:8:\"override\";s:35:\"{$site.icons.15.iconSourceFileName}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:12:\"colorOverlay\";s:29:\"{$site.icons.15.colorOverlay}\";s:13:\"colorOverlay.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:15:\"backgroundColor\";s:32:\"{$site.icons.15.backgroundColor}\";s:16:\"backgroundColor.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"relAttribute\";s:4:\"icon\";s:13:\"relAttribute.\";a:2:{s:8:\"override\";s:29:\"{$site.icons.15.relAttribute}\";s:9:\"override.\";a:2:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";}}s:20:\"additionalAttributes\";s:37:\"{$site.icons.15.additionalAttributes}\";s:21:\"additionalAttributes.\";a:3:{s:10:\"insertData\";s:1:\"1\";s:8:\"required\";s:1:\"1\";s:10:\"noTrimWrap\";s:4:\"| ||\";}}i:20;s:12:\"IMG_RESOURCE\";s:3:\"20.\";a:2:{s:5:\"file.\";a:8:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:3:\"0,0\";s:7:\"params.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:10:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"value\";s:15:\"-colorspace RGB\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:5:{s:5:\"value\";s:23:\"-background transparent\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:4:\"| ||\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:5:{s:5:\"value\";s:23:\"{register:colorOverlay}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:27:\"| -fill \"|\" -colorize 100%|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"register:colorOverlay\";}}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:5:{s:5:\"value\";s:26:\"{register:backgroundColor}\";s:10:\"insertData\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"noTrimWrap\";s:32:\"| -background \"|\" -alpha remove|\";s:8:\"required\";s:1:\"1\";}s:8:\"required\";s:1:\"1\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:24:\"register:backgroundColor\";}}}i:100;s:4:\"TEXT\";s:4:\"100.\";a:1:{s:10:\"noTrimWrap\";s:4:\"| ||\";}}}s:7:\"import.\";a:2:{s:4:\"data\";s:82:\"PATH:EXT:templatebootstrap/{register:iconRelPath}/{register:iconSourceFileName}.ai\";s:5:\"data.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:5:\"width\";s:20:\"{register:iconSize}c\";s:6:\"width.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:6:\"height\";s:20:\"{register:iconSize}c\";s:7:\"height.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:4:\"wrap\";s:124:\"<link href=\"|\" rel=\"{register:relAttribute}\" sizes=\"{register:iconSize}x{register:iconSize}\"{register:additionalAttributes}>\";s:10:\"insertData\";s:1:\"1\";s:3:\"if.\";a:1:{s:11:\"isPositive.\";a:1:{s:4:\"data\";s:17:\"register:iconSize\";}}}}i:50;s:16:\"RESTORE_REGISTER\";}i:500;s:4:\"TEXT\";s:4:\"500.\";a:5:{s:7:\"prepend\";s:4:\"TEXT\";s:8:\"prepend.\";a:1:{s:4:\"char\";s:2:\"10\";}s:8:\"stdWrap.\";a:2:{s:4:\"data\";s:71:\"PATH:EXT:templatebootstrap/Resources/Public/Template/images/favicon.ico\";s:8:\"required\";s:1:\"1\";}s:4:\"wrap\";s:55:\"<link href=\"|\" rel=\"shortcut icon\" type=\"image/x-icon\">\";s:8:\"required\";s:1:\"1\";}}i:50;s:16:\"RESTORE_REGISTER\";}}}s:4:\"lib.\";a:25:{s:10:\"parseFunc.\";a:8:{s:9:\"makelinks\";s:1:\"1\";s:10:\"makelinks.\";a:2:{s:5:\"http.\";a:2:{s:4:\"keep\";s:4:\"path\";s:9:\"extTarget\";s:6:\"_blank\";}s:7:\"mailto.\";a:1:{s:4:\"keep\";s:4:\"path\";}}s:5:\"tags.\";a:2:{s:4:\"link\";s:4:\"TEXT\";s:5:\"link.\";a:3:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:3:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:22:\"parameters : allParams\";}s:9:\"extTarget\";s:6:\"_blank\";s:6:\"target\";s:0:\"\";}s:10:\"parseFunc.\";a:1:{s:9:\"constants\";s:1:\"1\";}}}s:9:\"allowTags\";s:389:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer, header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small, span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:8:\"denyTags\";s:1:\"*\";s:5:\"sword\";s:31:\"<span class=\"ce-sword\">|</span>\";s:9:\"constants\";s:1:\"1\";s:18:\"nonTypoTagStdWrap.\";a:2:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"2\";}}}s:14:\"parseFunc_RTE.\";a:10:{s:9:\"makelinks\";s:1:\"1\";s:10:\"makelinks.\";a:2:{s:5:\"http.\";a:2:{s:4:\"keep\";s:4:\"path\";s:9:\"extTarget\";s:6:\"_blank\";}s:7:\"mailto.\";a:1:{s:4:\"keep\";s:4:\"path\";}}s:5:\"tags.\";a:2:{s:4:\"link\";s:4:\"TEXT\";s:5:\"link.\";a:3:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:3:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:22:\"parameters : allParams\";}s:9:\"extTarget\";s:6:\"_blank\";s:6:\"target\";s:0:\"\";}s:10:\"parseFunc.\";a:1:{s:9:\"constants\";s:1:\"1\";}}}s:9:\"allowTags\";s:389:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer, header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small, span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:8:\"denyTags\";s:1:\"*\";s:5:\"sword\";s:31:\"<span class=\"ce-sword\">|</span>\";s:9:\"constants\";s:1:\"1\";s:18:\"nonTypoTagStdWrap.\";a:3:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"2\";}s:12:\"encapsLines.\";a:4:{s:13:\"encapsTagList\";s:29:\"p,pre,h1,h2,h3,h4,h5,h6,hr,dt\";s:9:\"remapTag.\";a:1:{s:3:\"DIV\";s:1:\"P\";}s:13:\"nonWrappedTag\";s:1:\"P\";s:17:\"innerStdWrap_all.\";a:1:{s:7:\"ifBlank\";s:6:\"&nbsp;\";}}}s:14:\"externalBlocks\";s:84:\"article, aside, blockquote, div, dd, dl, footer, header, nav, ol, section, table, ul\";s:15:\"externalBlocks.\";a:13:{s:11:\"blockquote.\";a:3:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";s:14:\"callRecursive.\";a:1:{s:11:\"tagStdWrap.\";a:2:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:1:{s:5:\"tags.\";a:1:{s:11:\"blockquote.\";a:1:{s:15:\"overrideAttribs\";s:37:\"style=\"margin-bottom:0;margin-top:0;\"\";}}}}}}s:3:\"ol.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:8:\"stdWrap.\";a:1:{s:9:\"parseFunc\";s:15:\"< lib.parseFunc\";}}s:3:\"ul.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:8:\"stdWrap.\";a:1:{s:9:\"parseFunc\";s:15:\"< lib.parseFunc\";}}s:6:\"table.\";a:4:{s:7:\"stripNL\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:5:\"tags.\";a:1:{s:6:\"table.\";a:1:{s:10:\"fixAttrib.\";a:1:{s:6:\"class.\";a:3:{s:7:\"default\";s:12:\"contenttable\";s:6:\"always\";s:1:\"1\";s:4:\"list\";s:12:\"contenttable\";}}}}s:18:\"keepNonMatchedTags\";s:1:\"1\";}}s:14:\"HTMLtableCells\";s:1:\"1\";s:15:\"HTMLtableCells.\";a:2:{s:8:\"default.\";a:1:{s:8:\"stdWrap.\";a:2:{s:9:\"parseFunc\";s:19:\"< lib.parseFunc_RTE\";s:10:\"parseFunc.\";a:1:{s:18:\"nonTypoTagStdWrap.\";a:1:{s:12:\"encapsLines.\";a:1:{s:13:\"nonWrappedTag\";s:0:\"\";}}}}}s:25:\"addChr10BetweenParagraphs\";s:1:\"1\";}}s:4:\"div.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:8:\"article.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:6:\"aside.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:7:\"footer.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:7:\"header.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:4:\"nav.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:8:\"section.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:3:\"dl.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:3:\"dd.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}}}s:12:\"fluidContent\";s:13:\"FLUIDTEMPLATE\";s:13:\"fluidContent.\";a:5:{s:12:\"templateName\";s:7:\"Default\";s:18:\"templateRootPaths.\";a:2:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:90:\"EXT:templatebootstrap/Resources/Private/Extensions/fluid_styled_content/Templates/Content/\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:81:\"EXT:templatebootstrap/Resources/Private/Extensions/fluid_styled_content/Partials/\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:80:\"EXT:templatebootstrap/Resources/Private/Extensions/fluid_styled_content/Layouts/\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:1:{s:6:\"popup.\";a:8:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"1200\";s:6:\"height\";s:0:\"\";s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"1\";}}s:15:\"directImageLink\";s:1:\"1\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}}}}s:9:\"stdheader\";s:18:\"< lib.fluidContent\";s:10:\"stdheader.\";a:1:{s:12:\"templateName\";s:10:\"HeaderOnly\";}s:8:\"tx_form.\";a:1:{s:19:\"registeredElements.\";a:1:{s:24:\"defaultModelDescription.\";a:1:{s:17:\"compatibilityMode\";s:1:\"0\";}}}s:9:\"searchbox\";s:4:\"USER\";s:10:\"searchbox.\";a:7:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:10:\"vendorName\";s:9:\"TYPO3\\CMS\";s:13:\"extensionName\";s:13:\"IndexedSearch\";s:10:\"pluginName\";s:3:\"Pi2\";s:28:\"switchableControllerActions.\";a:1:{s:7:\"Search.\";a:1:{i:1;s:4:\"form\";}}s:4:\"view\";s:30:\"< plugin.tx_indexedsearch.view\";s:8:\"settings\";s:34:\"< plugin.tx_indexedsearch.settings\";}s:13:\"contentloader\";s:3:\"COA\";s:14:\"contentloader.\";a:4:{i:5;s:13:\"LOAD_REGISTER\";s:2:\"5.\";a:1:{s:7:\"colPos.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:1:\"0\";}}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:2:{s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:3:{s:7:\"orderBy\";s:7:\"sorting\";s:5:\"where\";s:24:\"colPos={register:colPos}\";s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}}}s:4:\"logo\";s:3:\"COA\";s:5:\"logo.\";a:4:{i:10;s:5:\"IMAGE\";s:3:\"10.\";a:5:{s:4:\"file\";s:67:\"EXT:templatebootstrap/Resources/Private/LogoSources/logo-default.ai\";s:5:\"file.\";a:3:{s:4:\"maxW\";s:3:\"600\";s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:7:\"0.1,0.1\";}s:7:\"altText\";s:83:\"{LLL:EXT:templatebootstrap/Resources/Private/Language/locallang.xlf:logo} Muster AG\";s:8:\"altText.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:9:\"typolink.\";a:3:{s:6:\"title.\";a:1:{s:4:\"data\";s:71:\"LLL:EXT:templatebootstrap/Resources/Private/Language/locallang.xlf:home\";}s:9:\"parameter\";s:1:\"1\";s:10:\"ATagParams\";s:36:\"id=\"logo\" class=\"show-for-medium-up\"\";}}}i:20;s:5:\"IMAGE\";s:3:\"20.\";a:5:{s:4:\"file\";s:67:\"EXT:templatebootstrap/Resources/Private/LogoSources/icon-default.ai\";s:5:\"file.\";a:3:{s:3:\"ext\";s:3:\"png\";s:4:\"crop\";s:7:\"0.1,0.1\";s:4:\"maxH\";s:3:\"100\";}s:7:\"altText\";s:83:\"{LLL:EXT:templatebootstrap/Resources/Private/Language/locallang.xlf:logo} Muster AG\";s:8:\"altText.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:9:\"typolink.\";a:3:{s:6:\"title.\";a:1:{s:4:\"data\";s:71:\"LLL:EXT:templatebootstrap/Resources/Private/Language/locallang.xlf:home\";}s:9:\"parameter\";s:1:\"1\";s:10:\"ATagParams\";s:19:\"id=\"logo-iconesque\"\";}}}}s:4:\"menu\";s:5:\"HMENU\";s:5:\"menu.\";a:7:{s:4:\"wrap\";s:59:\"<nav role=\"navigation\" id=\"mainNavigation\"><ul>|</ul></nav>\";i:1;s:5:\"TMENU\";s:2:\"1.\";a:16:{s:6:\"noBlur\";s:1:\"1\";s:6:\"expAll\";s:1:\"1\";s:2:\"NO\";s:1:\"1\";s:3:\"NO.\";a:5:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}}s:3:\"ACT\";s:1:\"1\";s:4:\"ACT.\";a:6:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:10:\"ATagParams\";s:36:\"class=\"active layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:3:\"CUR\";s:1:\"1\";s:4:\"CUR.\";a:6:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:10:\"ATagParams\";s:36:\"class=\"active layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:8:\"CURIFSUB\";s:1:\"1\";s:9:\"CURIFSUB.\";a:6:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:10:\"ATagParams\";s:36:\"class=\"active layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:5:\"IFSUB\";s:1:\"1\";s:6:\"IFSUB.\";a:6:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:8:\"ACTIFSUB\";s:1:\"1\";s:9:\"ACTIFSUB.\";a:6:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:3:\"SPC\";s:1:\"1\";s:4:\"SPC.\";a:5:{s:14:\"wrapItemAndSub\";s:41:\"<li class=\"divider\"><label>|</label></li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}}}i:2;s:5:\"TMENU\";s:2:\"2.\";a:17:{s:6:\"noBlur\";s:1:\"1\";s:6:\"expAll\";s:1:\"1\";s:2:\"NO\";s:1:\"1\";s:3:\"NO.\";a:5:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}}s:3:\"ACT\";s:1:\"1\";s:4:\"ACT.\";a:6:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:10:\"ATagParams\";s:36:\"class=\"active layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:3:\"CUR\";s:1:\"1\";s:4:\"CUR.\";a:6:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:10:\"ATagParams\";s:36:\"class=\"active layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:8:\"CURIFSUB\";s:1:\"1\";s:9:\"CURIFSUB.\";a:6:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:10:\"ATagParams\";s:36:\"class=\"active layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:5:\"IFSUB\";s:1:\"1\";s:6:\"IFSUB.\";a:6:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:8:\"ACTIFSUB\";s:1:\"1\";s:9:\"ACTIFSUB.\";a:6:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:3:\"SPC\";s:1:\"1\";s:4:\"SPC.\";a:5:{s:14:\"wrapItemAndSub\";s:41:\"<li class=\"divider\"><label>|</label></li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}}s:4:\"wrap\";s:10:\"<ul>|</ul>\";}i:3;s:5:\"TMENU\";s:2:\"3.\";a:17:{s:6:\"noBlur\";s:1:\"1\";s:6:\"expAll\";s:1:\"1\";s:2:\"NO\";s:1:\"1\";s:3:\"NO.\";a:5:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}}s:3:\"ACT\";s:1:\"1\";s:4:\"ACT.\";a:6:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:10:\"ATagParams\";s:36:\"class=\"active layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:3:\"CUR\";s:1:\"1\";s:4:\"CUR.\";a:6:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:10:\"ATagParams\";s:36:\"class=\"active layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:8:\"CURIFSUB\";s:1:\"1\";s:9:\"CURIFSUB.\";a:6:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:10:\"ATagParams\";s:36:\"class=\"active layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:5:\"IFSUB\";s:1:\"1\";s:6:\"IFSUB.\";a:6:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:8:\"ACTIFSUB\";s:1:\"1\";s:9:\"ACTIFSUB.\";a:6:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}s:14:\"ATagBeforeWrap\";s:1:\"1\";}s:3:\"SPC\";s:1:\"1\";s:4:\"SPC.\";a:5:{s:14:\"wrapItemAndSub\";s:41:\"<li class=\"divider\"><label>|</label></li>\";s:10:\"ATagParams\";s:29:\"class=\"layout-{field:layout}\"\";s:11:\"allStdWrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:8:\"stdWrap.\";a:1:{s:16:\"htmlSpecialChars\";s:1:\"1\";}s:10:\"ATagTitle.\";a:1:{s:5:\"field\";s:17:\"subtitle // title\";}}s:4:\"wrap\";s:10:\"<ul>|</ul>\";}}s:13:\"menu-language\";s:5:\"HMENU\";s:14:\"menu-language.\";a:7:{s:7:\"special\";s:8:\"language\";s:8:\"special.\";a:2:{s:5:\"value\";s:5:\"0,1,2\";s:20:\"normalWhenNoLanguage\";s:1:\"0\";}s:14:\"addQueryString\";s:1:\"1\";s:15:\"addQueryString.\";a:1:{s:7:\"exclude\";s:7:\"L,cHash\";}s:4:\"wrap\";s:38:\"<nav class=\"language\"><ul>|</ul></nav>\";i:1;s:5:\"TMENU\";s:2:\"1.\";a:7:{s:6:\"noBlur\";s:1:\"1\";s:2:\"NO\";s:1:\"1\";s:3:\"NO.\";a:2:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:8:\"stdWrap.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"value\";s:34:\"Deutsch |*| Français |*| Italiano\";}}}s:3:\"ACT\";s:1:\"1\";s:4:\"ACT.\";a:3:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:8:\"stdWrap.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"value\";s:34:\"Deutsch |*| Français |*| Italiano\";}}s:10:\"ATagParams\";s:14:\"class=\"active\"\";}s:8:\"USERDEF1\";s:1:\"1\";s:9:\"USERDEF1.\";a:3:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:8:\"stdWrap.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"value\";s:34:\"Deutsch |*| Français |*| Italiano\";}}s:11:\"doNotLinkIt\";s:1:\"1\";}}}s:11:\"currentyear\";s:4:\"TEXT\";s:12:\"currentyear.\";a:2:{s:4:\"data\";s:6:\"date:U\";s:8:\"strftime\";s:2:\"%Y\";}s:9:\"skiplinks\";s:3:\"COA\";s:10:\"skiplinks.\";a:9:{s:4:\"wrap\";s:56:\"<ul id=\"skiplinks\" title=\"Skiplinks\" tabindex=\"1\">|</ul>\";i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:4:\"wrap\";s:10:\"<li>|</li>\";s:5:\"value\";s:96:\"<span>{LLL:EXT:templatebootstrap/Resources/Private/Language/locallang.xlf:skiplinks.home}</span>\";s:6:\"value.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:9:\"typolink.\";a:3:{s:5:\"title\";s:9:\"[ALT + 0]\";s:9:\"parameter\";s:1:\"1\";s:10:\"ATagParams\";s:13:\"accesskey=\"0\"\";}}i:20;s:4:\"TEXT\";s:3:\"20.\";a:4:{s:4:\"wrap\";s:10:\"<li>|</li>\";s:5:\"value\";s:102:\"<span>{LLL:EXT:templatebootstrap/Resources/Private/Language/locallang.xlf:skiplinks.navigation}</span>\";s:6:\"value.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:9:\"typolink.\";a:3:{s:5:\"title\";s:9:\"[ALT + 1]\";s:9:\"parameter\";s:11:\"#navigation\";s:10:\"ATagParams\";s:13:\"accesskey=\"1\"\";}}i:30;s:4:\"TEXT\";s:3:\"30.\";a:4:{s:4:\"wrap\";s:10:\"<li>|</li>\";s:5:\"value\";s:99:\"<span>{LLL:EXT:templatebootstrap/Resources/Private/Language/locallang.xlf:skiplinks.content}</span>\";s:6:\"value.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:9:\"typolink.\";a:3:{s:5:\"title\";s:9:\"[ALT + 2]\";s:9:\"parameter\";s:8:\"#content\";s:10:\"ATagParams\";s:13:\"accesskey=\"2\"\";}}i:40;s:4:\"TEXT\";s:3:\"40.\";a:4:{s:4:\"wrap\";s:10:\"<li>|</li>\";s:5:\"value\";s:99:\"<span>{LLL:EXT:templatebootstrap/Resources/Private/Language/locallang.xlf:skiplinks.contact}</span>\";s:6:\"value.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:9:\"typolink.\";a:3:{s:5:\"title\";s:9:\"[ALT + 3]\";s:9:\"parameter\";s:0:\"\";s:10:\"ATagParams\";s:13:\"accesskey=\"3\"\";}}}s:9:\"moodImage\";s:5:\"FILES\";s:10:\"moodImage.\";a:4:{s:11:\"references.\";a:2:{s:7:\"cObject\";s:4:\"USER\";s:8:\"cObject.\";a:6:{s:8:\"userFunc\";s:74:\"Julian\\TemplateBootstrap\\Utility\\TSRootLineMedia->getFilteredRootLineFiles\";s:9:\"fieldName\";s:30:\"tx_templatebootstrap_moodimage\";s:9:\"fileTypes\";s:16:\"jpg,jpeg,png,gif\";s:21:\"maxStepsUpTheRootLine\";s:4:\"1000\";s:3:\"pid\";s:1:\"0\";s:10:\"fieldName.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}}s:8:\"maxItems\";s:1:\"1\";s:9:\"renderObj\";s:4:\"CASE\";s:10:\"renderObj.\";a:5:{s:4:\"key.\";a:1:{s:4:\"data\";s:17:\"file:current:type\";}i:2;s:12:\"IMG_RESOURCE\";s:2:\"2.\";a:2:{s:5:\"file.\";a:3:{s:7:\"import.\";a:1:{s:4:\"data\";s:16:\"file:current:uid\";}s:18:\"treatIdAsReference\";s:1:\"1\";s:5:\"width\";s:4:\"1000\";}s:8:\"stdWrap.\";a:1:{s:4:\"wrap\";s:2:\"/|\";}}i:4;s:3:\"COA\";s:2:\"4.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:3:{s:10:\"attributes\";s:0:\"\";s:2:\"id\";s:0:\"\";s:7:\"classes\";s:0:\"\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:3:{s:5:\"value\";s:210:\"                    <video id=\"{register:id}\" class=\"{register:classes}\" {register:attributes}>\r\n                        <source src=\"{file:current:publicUrl}\" type=\"video/mp4\" />\r\n                    </video>\r\";s:10:\"insertData\";s:1:\"1\";s:9:\"typolink.\";a:2:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:17:\"file:current:link\";}s:10:\"ATagParams\";s:22:\"class=\"lightbox-media\"\";}}i:30;s:16:\"RESTORE_REGISTER\";}}}s:15:\"backgroundImage\";s:5:\"FILES\";s:16:\"backgroundImage.\";a:4:{s:11:\"references.\";a:2:{s:7:\"cObject\";s:4:\"USER\";s:8:\"cObject.\";a:6:{s:8:\"userFunc\";s:74:\"Julian\\TemplateBootstrap\\Utility\\TSRootLineMedia->getFilteredRootLineFiles\";s:9:\"fieldName\";s:36:\"tx_templatebootstrap_backgroundimage\";s:9:\"fileTypes\";s:16:\"jpg,jpeg,png,gif\";s:21:\"maxStepsUpTheRootLine\";s:4:\"1000\";s:3:\"pid\";s:1:\"0\";s:10:\"fieldName.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}}s:8:\"maxItems\";s:1:\"1\";s:9:\"renderObj\";s:4:\"CASE\";s:10:\"renderObj.\";a:5:{s:4:\"key.\";a:1:{s:4:\"data\";s:17:\"file:current:type\";}i:2;s:12:\"IMG_RESOURCE\";s:2:\"2.\";a:2:{s:5:\"file.\";a:3:{s:7:\"import.\";a:1:{s:4:\"data\";s:16:\"file:current:uid\";}s:18:\"treatIdAsReference\";s:1:\"1\";s:5:\"width\";s:4:\"1000\";}s:8:\"stdWrap.\";a:1:{s:4:\"wrap\";s:2:\"/|\";}}i:4;s:3:\"COA\";s:2:\"4.\";a:5:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:3:{s:10:\"attributes\";s:0:\"\";s:2:\"id\";s:0:\"\";s:7:\"classes\";s:0:\"\";}i:20;s:4:\"TEXT\";s:3:\"20.\";a:3:{s:5:\"value\";s:210:\"                    <video id=\"{register:id}\" class=\"{register:classes}\" {register:attributes}>\r\n                        <source src=\"{file:current:publicUrl}\" type=\"video/mp4\" />\r\n                    </video>\r\";s:10:\"insertData\";s:1:\"1\";s:9:\"typolink.\";a:2:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:17:\"file:current:link\";}s:10:\"ATagParams\";s:22:\"class=\"lightbox-media\"\";}}i:30;s:16:\"RESTORE_REGISTER\";}}}}s:7:\"styles.\";a:1:{s:8:\"content.\";a:2:{s:3:\"get\";s:7:\"CONTENT\";s:4:\"get.\";a:2:{s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:2:{s:7:\"orderBy\";s:7:\"sorting\";s:5:\"where\";s:8:\"colPos=0\";}}}}s:10:\"tt_content\";s:4:\"CASE\";s:11:\"tt_content.\";a:28:{s:4:\"key.\";a:1:{s:5:\"field\";s:5:\"CType\";}s:8:\"stdWrap.\";a:2:{s:9:\"editPanel\";s:1:\"1\";s:10:\"editPanel.\";a:5:{s:5:\"allow\";s:29:\"move, new, edit, hide, delete\";s:5:\"label\";s:2:\"%s\";s:14:\"onlyCurrentPid\";s:1:\"1\";s:13:\"previewBorder\";s:1:\"1\";s:5:\"edit.\";a:1:{s:13:\"displayRecord\";s:1:\"1\";}}}s:7:\"bullets\";s:18:\"< lib.fluidContent\";s:8:\"bullets.\";a:3:{s:12:\"templateName\";s:7:\"Bullets\";s:15:\"dataProcessing.\";a:4:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\SplitProcessor\";s:3:\"10.\";a:4:{s:3:\"if.\";a:2:{s:5:\"value\";s:1:\"2\";s:11:\"isLessThan.\";a:1:{s:5:\"field\";s:12:\"bullets_type\";}}s:9:\"fieldName\";s:8:\"bodytext\";s:18:\"removeEmptyEntries\";s:1:\"1\";s:2:\"as\";s:7:\"bullets\";}i:20;s:62:\"TYPO3\\CMS\\Frontend\\DataProcessing\\CommaSeparatedValueProcessor\";s:3:\"20.\";a:4:{s:9:\"fieldName\";s:8:\"bodytext\";s:3:\"if.\";a:2:{s:5:\"value\";s:1:\"2\";s:7:\"equals.\";a:1:{s:5:\"field\";s:12:\"bullets_type\";}}s:14:\"fieldDelimiter\";s:1:\"|\";s:2:\"as\";s:7:\"bullets\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:59:\"tt_content: header [header_layout], bodytext [bullets_type]\";s:10:\"editIcons.\";a:2:{s:13:\"beforeLastTag\";s:1:\"1\";s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:92:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.bullets\";}}}}s:3:\"div\";s:18:\"< lib.fluidContent\";s:4:\"div.\";a:1:{s:12:\"templateName\";s:3:\"Div\";}s:6:\"header\";s:18:\"< lib.fluidContent\";s:7:\"header.\";a:2:{s:12:\"templateName\";s:6:\"Header\";s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:63:\"tt_content: header [header_layout|header_link], subheader, date\";s:10:\"editIcons.\";a:2:{s:13:\"beforeLastTag\";s:1:\"1\";s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:91:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.header\";}}}}s:4:\"html\";s:18:\"< lib.fluidContent\";s:5:\"html.\";a:2:{s:12:\"templateName\";s:4:\"Html\";s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:20:\"tt_content: bodytext\";s:10:\"editIcons.\";a:2:{s:13:\"beforeLastTag\";s:1:\"1\";s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.html\";}}}}s:4:\"list\";s:18:\"< lib.fluidContent\";s:5:\"list.\";a:3:{s:12:\"templateName\";s:4:\"List\";s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:84:\"tt_content: header [header_layout], list_type, layout, select_key, pages [recursive]\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.list\";}}}s:3:\"20.\";a:2:{s:9:\"form_form\";s:4:\"USER\";s:10:\"form_form.\";a:4:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:13:\"extensionName\";s:4:\"Form\";s:10:\"pluginName\";s:4:\"Form\";s:10:\"vendorName\";s:9:\"TYPO3\\CMS\";}}}s:4:\"menu\";s:18:\"< lib.fluidContent\";s:5:\"menu.\";a:3:{s:12:\"templateName\";s:4:\"Menu\";s:15:\"dataProcessing.\";a:4:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\SplitProcessor\";s:3:\"10.\";a:7:{s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:9:\"fieldName\";s:5:\"pages\";s:9:\"delimiter\";s:1:\",\";s:18:\"removeEmptyEntries\";s:1:\"1\";s:14:\"filterIntegers\";s:1:\"1\";s:12:\"filterUnique\";s:1:\"1\";s:2:\"as\";s:8:\"pageUids\";}i:20;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\SplitProcessor\";s:3:\"20.\";a:7:{s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:5:\"field\";s:19:\"selected_categories\";}}s:9:\"fieldName\";s:19:\"selected_categories\";s:9:\"delimiter\";s:1:\",\";s:18:\"removeEmptyEntries\";s:1:\"1\";s:14:\"filterIntegers\";s:1:\"1\";s:12:\"filterUnique\";s:1:\"1\";s:2:\"as\";s:12:\"categoryUids\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:52:\"tt_content: header [header_layout], menu_type, pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:8:\"shortcut\";s:18:\"< lib.fluidContent\";s:9:\"shortcut.\";a:3:{s:12:\"templateName\";s:8:\"Shortcut\";s:10:\"variables.\";a:2:{s:9:\"shortcuts\";s:7:\"RECORDS\";s:10:\"shortcuts.\";a:2:{s:7:\"source.\";a:1:{s:5:\"field\";s:7:\"records\";}s:6:\"tables\";s:55:\"tt_content,tt_address,tt_news,tx_news_domain_model_news\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:43:\"tt_content: header [header_layout], records\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:93:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.shortcut\";}}}}s:5:\"table\";s:18:\"< lib.fluidContent\";s:6:\"table.\";a:3:{s:12:\"templateName\";s:5:\"Table\";s:15:\"dataProcessing.\";a:2:{i:10;s:62:\"TYPO3\\CMS\\Frontend\\DataProcessing\\CommaSeparatedValueProcessor\";s:3:\"10.\";a:5:{s:9:\"fieldName\";s:8:\"bodytext\";s:15:\"fieldDelimiter.\";a:1:{s:5:\"char.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"field\";s:15:\"table_delimiter\";}}}s:15:\"fieldEnclosure.\";a:1:{s:5:\"char.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"field\";s:15:\"table_enclosure\";}}}s:15:\"maximumColumns.\";a:1:{s:5:\"field\";s:4:\"cols\";}s:2:\"as\";s:5:\"table\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:100:\"tt_content: header [header_layout], bodytext, [table_caption|cols|table_header_position|table_tfoot]\";s:10:\"editIcons.\";a:2:{s:13:\"beforeLastTag\";s:1:\"1\";s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:90:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.table\";}}}}s:9:\"textmedia\";s:18:\"< lib.fluidContent\";s:10:\"textmedia.\";a:3:{s:12:\"templateName\";s:9:\"Textmedia\";s:15:\"dataProcessing.\";a:4:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:6:\"assets\";}}i:20;s:50:\"TYPO3\\CMS\\Frontend\\DataProcessing\\GalleryProcessor\";s:3:\"20.\";a:5:{s:15:\"maxGalleryWidth\";s:4:\"1200\";s:21:\"maxGalleryWidthInText\";s:3:\"840\";s:13:\"columnSpacing\";s:1:\"0\";s:11:\"borderWidth\";s:1:\"0\";s:13:\"borderPadding\";s:1:\"0\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:126:\"tt_content: header [header_layout], bodytext, assets [imageorient|imagewidth|imageheight], [imagecols|imageborder], image_zoom\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:94:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.textmedia\";}}}}s:7:\"uploads\";s:18:\"< lib.fluidContent\";s:8:\"uploads.\";a:3:{s:12:\"templateName\";s:7:\"Uploads\";s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:3:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}s:12:\"collections.\";a:1:{s:5:\"field\";s:16:\"file_collections\";}s:8:\"sorting.\";a:1:{s:5:\"field\";s:16:\"filelink_sorting\";}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:127:\"tt_content: header [header_layout], media, file_collections, filelink_sorting, [filelink_size|uploads_description|uploads_type]\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:92:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.uploads\";}}}}s:7:\"default\";s:18:\"< lib.fluidContent\";s:5:\"login\";s:3:\"COA\";s:6:\"login.\";a:2:{i:10;s:15:\"< lib.stdheader\";i:20;s:23:\"< plugin.tx_felogin_pi1\";}s:8:\"mailform\";s:3:\"COA\";s:9:\"mailform.\";a:3:{i:10;s:15:\"< lib.stdheader\";i:20;s:4:\"FORM\";s:3:\"20.\";a:1:{s:8:\"stdWrap.\";a:4:{s:4:\"wrap\";s:33:\"<div class=\"csc-mailform\">|</div>\";s:9:\"editIcons\";s:20:\"tt_content: bodytext\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:55:\"LLL:EXT:css_styled_content/pi1/locallang.xlf:eIcon.form\";}}s:13:\"prefixComment\";s:23:\"2 | Mail form inserted:\";}}}s:17:\"gridelements_pi1.\";a:1:{s:3:\"20.\";a:1:{s:3:\"10.\";a:1:{s:6:\"setup.\";a:2:{s:7:\"slider.\";a:3:{s:7:\"cObject\";s:13:\"FLUIDTEMPLATE\";s:8:\"cObject.\";a:1:{s:4:\"file\";s:84:\"typo3conf/ext/templatebootstrap/Resources/Private/Templates/GridElements/Slider.html\";}s:8:\"columns.\";a:1:{s:2:\"0.\";a:2:{s:9:\"renderObj\";s:3:\"COA\";s:10:\"renderObj.\";a:2:{i:20;s:3:\"COA\";s:3:\"20.\";a:4:{s:4:\"wrap\";s:26:\"<div class=\"slide\">|</div>\";i:10;s:4:\"TEXT\";s:3:\"10.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:6:\"header\";}s:4:\"wrap\";s:14:\"<span>|</span>\";}s:3:\"20.\";a:3:{s:2:\"1.\";a:1:{s:5:\"file.\";a:1:{s:7:\"import.\";a:1:{s:7:\"listNum\";s:1:\"0\";}}}s:6:\"layout\";s:4:\"TEXT\";s:7:\"layout.\";a:1:{s:5:\"value\";s:12:\"###IMAGES###\";}}}}}}}s:13:\"threecolumns.\";a:2:{s:7:\"cObject\";s:13:\"FLUIDTEMPLATE\";s:8:\"cObject.\";a:1:{s:4:\"file\";s:90:\"typo3conf/ext/templatebootstrap/Resources/Private/Templates/GridElements/ThreeColumns.html\";}}}}}}}s:23:\"fluidAjaxWidgetResponse\";s:4:\"PAGE\";s:24:\"fluidAjaxWidgetResponse.\";a:4:{s:7:\"typeNum\";s:4:\"7076\";s:7:\"config.\";a:4:{s:8:\"no_cache\";s:1:\"1\";s:20:\"disableAllHeaderCode\";s:1:\"1\";s:17:\"additionalHeaders\";s:23:\"Content-type:text/plain\";s:5:\"debug\";s:1:\"0\";}i:10;s:8:\"USER_INT\";s:3:\"10.\";a:1:{s:8:\"userFunc\";s:42:\"TYPO3\\CMS\\Fluid\\Core\\Widget\\Bootstrap->run\";}}s:7:\"module.\";a:4:{s:20:\"tx_extensionmanager.\";a:2:{s:9:\"settings.\";a:1:{s:13:\"repositoryUid\";s:1:\"1\";}s:9:\"features.\";a:1:{s:20:\"skipDefaultArguments\";s:1:\"0\";}}s:9:\"tx_belog.\";a:2:{s:12:\"persistence.\";a:1:{s:8:\"classes.\";a:3:{s:38:\"TYPO3\\CMS\\Belog\\Domain\\Model\\LogEntry.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:7:\"sys_log\";s:8:\"columns.\";a:8:{s:7:\"userid.\";a:1:{s:13:\"mapOnProperty\";s:14:\"backendUserUid\";}s:7:\"recuid.\";a:1:{s:13:\"mapOnProperty\";s:9:\"recordUid\";}s:10:\"tablename.\";a:1:{s:13:\"mapOnProperty\";s:9:\"tableName\";}s:7:\"recpid.\";a:1:{s:13:\"mapOnProperty\";s:9:\"recordPid\";}s:11:\"details_nr.\";a:1:{s:13:\"mapOnProperty\";s:13:\"detailsNumber\";}s:3:\"IP.\";a:1:{s:13:\"mapOnProperty\";s:2:\"ip\";}s:10:\"workspace.\";a:1:{s:13:\"mapOnProperty\";s:12:\"workspaceUid\";}s:6:\"NEWid.\";a:1:{s:13:\"mapOnProperty\";s:5:\"newId\";}}}}s:39:\"TYPO3\\CMS\\Belog\\Domain\\Model\\Workspace.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:13:\"sys_workspace\";}}s:42:\"TYPO3\\CMS\\Belog\\Domain\\Model\\HistoryEntry.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:11:\"sys_history\";}}}}s:9:\"settings.\";a:3:{s:29:\"selectableNumberOfLogEntries.\";a:7:{i:20;s:2:\"20\";i:50;s:2:\"50\";i:100;s:3:\"100\";i:200;s:3:\"200\";i:500;s:3:\"500\";i:1000;s:4:\"1000\";i:1000000;s:3:\"any\";}s:21:\"selectableTimeFrames.\";a:8:{i:0;s:8:\"thisWeek\";i:1;s:8:\"lastWeek\";i:2;s:9:\"last7Days\";i:10;s:9:\"thisMonth\";i:11;s:9:\"lastMonth\";i:12;s:10:\"last31Days\";i:20;s:7:\"noLimit\";i:30;s:11:\"userDefined\";}s:18:\"selectableActions.\";a:7:{i:0;s:3:\"any\";i:1;s:14:\"actionDatabase\";i:2;s:10:\"actionFile\";i:3;s:11:\"actionCache\";i:254;s:14:\"actionSettings\";i:255;s:11:\"actionLogin\";i:-1;s:12:\"actionErrors\";}}}s:10:\"tx_beuser.\";a:2:{s:12:\"persistence.\";a:1:{s:10:\"storagePid\";s:1:\"0\";}s:9:\"settings.\";a:1:{s:5:\"dummy\";s:3:\"foo\";}}s:11:\"tx_sysnote.\";a:1:{s:5:\"view.\";a:3:{s:14:\"layoutRootPath\";s:39:\"EXT:sys_note/Resources/Private/Layouts/\";s:16:\"templateRootPath\";s:41:\"EXT:sys_note/Resources/Private/Templates/\";s:15:\"partialRootPath\";s:40:\"EXT:sys_note/Resources/Private/Partials/\";}}}s:9:\"sitetitle\";s:0:\"\";s:6:\"types.\";a:2:{i:0;s:4:\"page\";i:7076;s:23:\"fluidAjaxWidgetResponse\";}}'),(4,'aa2db4f568fd4799b95ecb1d9136fa88',2145909600,'a:9:{s:32:\"3b1c933e57bbab37176e5c5c33724c56\";s:73:\"[Julian\\TemplateBootstrap\\Utility\\Condition\\EnvironmentCondition = local]\";s:32:\"eb2d65b0fbe5588b70317e642bfd58bb\";s:79:\"[Julian\\TemplateBootstrap\\Utility\\Condition\\EnvironmentCondition = development]\";s:32:\"bda496bca03246a2cfe01bea2c18e731\";s:20:\"[globalVar = GP:L=1]\";s:32:\"c165d28a38e2e639f89b6fb992b1fc12\";s:20:\"[globalVar = GP:L=2]\";s:32:\"5be57a4856867340c7e09c7c9eff37ea\";s:20:\"[globalVar = GP:L=3]\";s:32:\"010966dc7d2c314f2db05fdd3c3b5194\";s:23:\"[globalVar = LIT:1 = 1]\";s:32:\"3652062149e223f25ffa91556d2e2994\";s:23:\"[globalVar = LIT:1 = 0]\";s:32:\"65eec5f9b2cd8d6bf326f8d786001cca\";s:64:\"[Julian\\TemplateBootstrap\\Utility\\Condition\\DoNotTrackCondition]\";s:32:\"b64f7205ede255aeb493808fc5a4cf58\";s:22:\"[globalVar = LIT:0 < ]\";}'),(5,'2c18365758f04f4d2e0170bafa596523',2145909600,'a:1:{s:4:\"data\";a:1:{s:4:\"sDEF\";a:1:{s:4:\"lDEF\";a:3:{s:8:\"basePath\";a:1:{s:4:\"vDEF\";s:10:\"fileadmin/\";}s:8:\"pathType\";a:1:{s:4:\"vDEF\";s:8:\"relative\";}s:13:\"caseSensitive\";a:1:{s:4:\"vDEF\";s:0:\"\";}}}}}'),(6,'ee2f1329ecdebc0aa1bdaa21ca466ac8',1459579993,'a:3:{i:0;a:3:{s:14:\"wrapItemAndSub\";s:25:\"<li class=\"active\">|</li>\";s:8:\"stdWrap.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"value\";s:7:\"Deutsch\";}}s:10:\"ATagParams\";s:14:\"class=\"active\"\";}i:1;a:3:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:8:\"stdWrap.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"value\";s:9:\"Français\";}}s:11:\"doNotLinkIt\";s:1:\"1\";}i:2;a:3:{s:14:\"wrapItemAndSub\";s:10:\"<li>|</li>\";s:8:\"stdWrap.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"value\";s:8:\"Italiano\";}}s:11:\"doNotLinkIt\";s:1:\"1\";}}'),(7,'83e76310cb57635bd93661d4f08e21a6',1459579993,'a:0:{}'),(8,'169aca1993aa78525ae9f1ef7a71dfe2',2145909600,'a:2:{i:0;a:3:{s:8:\"TSconfig\";a:4:{s:8:\"options.\";a:13:{s:15:\"enableBookmarks\";s:1:\"1\";s:10:\"file_list.\";a:3:{s:28:\"enableDisplayBigControlPanel\";s:9:\"activated\";s:23:\"enableDisplayThumbnails\";s:10:\"selectable\";s:15:\"enableClipBoard\";s:10:\"selectable\";}s:9:\"pageTree.\";a:5:{s:31:\"doktypesToShowInNewPageDragArea\";s:21:\"1,6,4,7,3,254,255,199\";s:19:\"showPathAboveMounts\";s:1:\"1\";s:19:\"showPageIdWithTitle\";s:1:\"1\";s:12:\"showNavTitle\";s:1:\"1\";s:23:\"showDomainNameWithTitle\";s:1:\"1\";}s:12:\"contextMenu.\";a:2:{s:8:\"options.\";a:1:{s:9:\"leftIcons\";s:1:\"1\";}s:6:\"table.\";a:3:{s:13:\"virtual_root.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:6:\"items.\";a:7:{i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:7:\"history\";s:5:\"label\";s:42:\"LLL:EXT:lang/locallang_misc.xlf:CM_history\";s:8:\"iconName\";s:29:\"actions-document-history-open\";s:16:\"displayCondition\";s:19:\"canShowHistory != 0\";s:14:\"callbackAction\";s:16:\"openHistoryPopUp\";}i:9000;s:7:\"DIVIDER\";i:9100;s:4:\"ITEM\";s:5:\"9100.\";a:4:{s:4:\"name\";s:9:\"exportT3d\";s:5:\"label\";s:62:\"LLL:EXT:impexp/Resources/Private/Language/locallang.xlf:export\";s:8:\"iconName\";s:27:\"actions-document-export-t3d\";s:14:\"callbackAction\";s:9:\"exportT3d\";}i:9200;s:4:\"ITEM\";s:5:\"9200.\";a:4:{s:4:\"name\";s:9:\"importT3d\";s:5:\"label\";s:62:\"LLL:EXT:impexp/Resources/Private/Language/locallang.xlf:import\";s:8:\"iconName\";s:27:\"actions-document-import-t3d\";s:14:\"callbackAction\";s:9:\"importT3d\";}}}s:11:\"pages_root.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:6:\"items.\";a:12:{i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:4:\"view\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.view\";s:8:\"iconName\";s:21:\"actions-document-view\";s:16:\"displayCondition\";s:16:\"canBeViewed != 0\";s:14:\"callbackAction\";s:8:\"viewPage\";}i:200;s:4:\"ITEM\";s:4:\"200.\";a:5:{s:4:\"name\";s:3:\"new\";s:5:\"label\";s:38:\"LLL:EXT:lang/locallang_core.xlf:cm.new\";s:8:\"iconName\";s:16:\"actions-page-new\";s:16:\"displayCondition\";s:22:\"canCreateNewPages != 0\";s:14:\"callbackAction\";s:13:\"newPageWizard\";}i:300;s:7:\"DIVIDER\";i:400;s:4:\"ITEM\";s:4:\"400.\";a:5:{s:4:\"name\";s:7:\"history\";s:5:\"label\";s:42:\"LLL:EXT:lang/locallang_misc.xlf:CM_history\";s:8:\"iconName\";s:29:\"actions-document-history-open\";s:16:\"displayCondition\";s:19:\"canShowHistory != 0\";s:14:\"callbackAction\";s:16:\"openHistoryPopUp\";}i:9000;s:7:\"DIVIDER\";i:9100;s:4:\"ITEM\";s:5:\"9100.\";a:4:{s:4:\"name\";s:9:\"exportT3d\";s:5:\"label\";s:62:\"LLL:EXT:impexp/Resources/Private/Language/locallang.xlf:export\";s:8:\"iconName\";s:27:\"actions-document-export-t3d\";s:14:\"callbackAction\";s:9:\"exportT3d\";}i:9200;s:4:\"ITEM\";s:5:\"9200.\";a:4:{s:4:\"name\";s:9:\"importT3d\";s:5:\"label\";s:62:\"LLL:EXT:impexp/Resources/Private/Language/locallang.xlf:import\";s:8:\"iconName\";s:27:\"actions-document-import-t3d\";s:14:\"callbackAction\";s:9:\"importT3d\";}}}s:6:\"pages.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:6:\"items.\";a:18:{i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:4:\"view\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.view\";s:8:\"iconName\";s:21:\"actions-document-view\";s:16:\"displayCondition\";s:16:\"canBeViewed != 0\";s:14:\"callbackAction\";s:8:\"viewPage\";}i:200;s:7:\"DIVIDER\";i:300;s:4:\"ITEM\";s:4:\"300.\";a:5:{s:4:\"name\";s:7:\"disable\";s:5:\"label\";s:41:\"LLL:EXT:lang/locallang_common.xlf:disable\";s:8:\"iconName\";s:17:\"actions-edit-hide\";s:16:\"displayCondition\";s:52:\"getRecord|hidden = 0 && canBeDisabledAndEnabled != 0\";s:14:\"callbackAction\";s:11:\"disablePage\";}i:400;s:4:\"ITEM\";s:4:\"400.\";a:5:{s:4:\"name\";s:6:\"enable\";s:5:\"label\";s:40:\"LLL:EXT:lang/locallang_common.xlf:enable\";s:8:\"iconName\";s:19:\"actions-edit-unhide\";s:16:\"displayCondition\";s:52:\"getRecord|hidden = 1 && canBeDisabledAndEnabled != 0\";s:14:\"callbackAction\";s:10:\"enablePage\";}i:500;s:4:\"ITEM\";s:4:\"500.\";a:5:{s:4:\"name\";s:4:\"edit\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.edit\";s:8:\"iconName\";s:17:\"actions-page-open\";s:16:\"displayCondition\";s:16:\"canBeEdited != 0\";s:14:\"callbackAction\";s:18:\"editPageProperties\";}i:600;s:4:\"ITEM\";s:4:\"600.\";a:5:{s:4:\"name\";s:4:\"info\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.info\";s:8:\"iconName\";s:21:\"actions-document-info\";s:16:\"displayCondition\";s:16:\"canShowInfo != 0\";s:14:\"callbackAction\";s:13:\"openInfoPopUp\";}i:700;s:4:\"ITEM\";s:4:\"700.\";a:5:{s:4:\"name\";s:7:\"history\";s:5:\"label\";s:42:\"LLL:EXT:lang/locallang_misc.xlf:CM_history\";s:8:\"iconName\";s:29:\"actions-document-history-open\";s:16:\"displayCondition\";s:19:\"canShowHistory != 0\";s:14:\"callbackAction\";s:16:\"openHistoryPopUp\";}i:800;s:7:\"DIVIDER\";i:900;s:7:\"SUBMENU\";s:4:\"900.\";a:19:{s:5:\"label\";s:51:\"LLL:EXT:lang/locallang_core.xlf:cm.copyPasteActions\";i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:3:\"new\";s:5:\"label\";s:38:\"LLL:EXT:lang/locallang_core.xlf:cm.new\";s:8:\"iconName\";s:16:\"actions-page-new\";s:16:\"displayCondition\";s:22:\"canCreateNewPages != 0\";s:14:\"callbackAction\";s:13:\"newPageWizard\";}i:200;s:7:\"DIVIDER\";i:300;s:4:\"ITEM\";s:4:\"300.\";a:5:{s:4:\"name\";s:3:\"cut\";s:5:\"label\";s:38:\"LLL:EXT:lang/locallang_core.xlf:cm.cut\";s:8:\"iconName\";s:16:\"actions-edit-cut\";s:16:\"displayCondition\";s:53:\"isInCutMode = 0 && canBeCut != 0 && isMountPoint != 1\";s:14:\"callbackAction\";s:13:\"enableCutMode\";}i:400;s:4:\"ITEM\";s:4:\"400.\";a:5:{s:4:\"name\";s:3:\"cut\";s:5:\"label\";s:38:\"LLL:EXT:lang/locallang_core.xlf:cm.cut\";s:8:\"iconName\";s:24:\"actions-edit-cut-release\";s:16:\"displayCondition\";s:32:\"isInCutMode = 1 && canBeCut != 0\";s:14:\"callbackAction\";s:14:\"disableCutMode\";}i:500;s:4:\"ITEM\";s:4:\"500.\";a:5:{s:4:\"name\";s:4:\"copy\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.copy\";s:8:\"iconName\";s:17:\"actions-edit-copy\";s:16:\"displayCondition\";s:36:\"isInCopyMode = 0 && canBeCopied != 0\";s:14:\"callbackAction\";s:14:\"enableCopyMode\";}i:600;s:4:\"ITEM\";s:4:\"600.\";a:5:{s:4:\"name\";s:4:\"copy\";s:5:\"label\";s:39:\"LLL:EXT:lang/locallang_core.xlf:cm.copy\";s:8:\"iconName\";s:25:\"actions-edit-copy-release\";s:16:\"displayCondition\";s:36:\"isInCopyMode = 1 && canBeCopied != 0\";s:14:\"callbackAction\";s:15:\"disableCopyMode\";}i:700;s:4:\"ITEM\";s:4:\"700.\";a:5:{s:4:\"name\";s:9:\"pasteInto\";s:5:\"label\";s:44:\"LLL:EXT:lang/locallang_core.xlf:cm.pasteinto\";s:8:\"iconName\";s:27:\"actions-document-paste-into\";s:16:\"displayCondition\";s:85:\"getContextInfo|inCopyMode = 1 || getContextInfo|inCutMode = 1 && canBePastedInto != 0\";s:14:\"callbackAction\";s:13:\"pasteIntoNode\";}i:800;s:4:\"ITEM\";s:4:\"800.\";a:5:{s:4:\"name\";s:10:\"pasteAfter\";s:5:\"label\";s:45:\"LLL:EXT:lang/locallang_core.xlf:cm.pasteafter\";s:8:\"iconName\";s:28:\"actions-document-paste-after\";s:16:\"displayCondition\";s:86:\"getContextInfo|inCopyMode = 1 || getContextInfo|inCutMode = 1 && canBePastedAfter != 0\";s:14:\"callbackAction\";s:14:\"pasteAfterNode\";}i:900;s:7:\"DIVIDER\";i:1000;s:4:\"ITEM\";s:5:\"1000.\";a:5:{s:4:\"name\";s:6:\"delete\";s:5:\"label\";s:41:\"LLL:EXT:lang/locallang_core.xlf:cm.delete\";s:8:\"iconName\";s:19:\"actions-edit-delete\";s:16:\"displayCondition\";s:38:\"canBeRemoved != 0 && isMountPoint != 1\";s:14:\"callbackAction\";s:10:\"removeNode\";}}i:1000;s:7:\"SUBMENU\";s:5:\"1000.\";a:13:{s:5:\"label\";s:48:\"LLL:EXT:lang/locallang_core.xlf:cm.branchActions\";i:100;s:4:\"ITEM\";s:4:\"100.\";a:5:{s:4:\"name\";s:15:\"mountAsTreeroot\";s:5:\"label\";s:49:\"LLL:EXT:lang/locallang_core.xlf:cm.tempMountPoint\";s:8:\"iconName\";s:26:\"actions-pagetree-mountroot\";s:16:\"displayCondition\";s:49:\"canBeTemporaryMountPoint != 0 && isMountPoint = 0\";s:14:\"callbackAction\";s:15:\"mountAsTreeRoot\";}i:200;s:7:\"DIVIDER\";i:300;s:4:\"ITEM\";s:4:\"300.\";a:5:{s:4:\"name\";s:12:\"expandBranch\";s:5:\"label\";s:47:\"LLL:EXT:lang/locallang_core.xlf:cm.expandBranch\";s:8:\"iconName\";s:23:\"actions-pagetree-expand\";s:16:\"displayCondition\";s:0:\"\";s:14:\"callbackAction\";s:12:\"expandBranch\";}i:400;s:4:\"ITEM\";s:4:\"400.\";a:5:{s:4:\"name\";s:14:\"collapseBranch\";s:5:\"label\";s:49:\"LLL:EXT:lang/locallang_core.xlf:cm.collapseBranch\";s:8:\"iconName\";s:25:\"actions-pagetree-collapse\";s:16:\"displayCondition\";s:0:\"\";s:14:\"callbackAction\";s:14:\"collapseBranch\";}i:9000;s:7:\"DIVIDER\";i:9100;s:4:\"ITEM\";s:5:\"9100.\";a:4:{s:4:\"name\";s:9:\"exportT3d\";s:5:\"label\";s:62:\"LLL:EXT:impexp/Resources/Private/Language/locallang.xlf:export\";s:8:\"iconName\";s:27:\"actions-document-export-t3d\";s:14:\"callbackAction\";s:9:\"exportT3d\";}i:9200;s:4:\"ITEM\";s:5:\"9200.\";a:4:{s:4:\"name\";s:9:\"importT3d\";s:5:\"label\";s:62:\"LLL:EXT:impexp/Resources/Private/Language/locallang.xlf:import\";s:8:\"iconName\";s:27:\"actions-document-import-t3d\";s:14:\"callbackAction\";s:9:\"importT3d\";}}}}}}s:14:\"disableDelete.\";a:2:{s:17:\"sys_file_metadata\";s:1:\"1\";s:8:\"sys_file\";s:1:\"1\";}s:11:\"saveDocView\";s:1:\"1\";s:10:\"saveDocNew\";s:1:\"1\";s:11:\"saveDocNew.\";a:3:{s:5:\"pages\";s:1:\"0\";s:8:\"sys_file\";s:1:\"0\";s:17:\"sys_file_metadata\";s:1:\"0\";}s:17:\"createFoldersInEB\";s:1:\"1\";s:11:\"alertPopups\";s:3:\"254\";s:11:\"clearCache.\";a:2:{s:5:\"pages\";s:1:\"1\";s:3:\"all\";s:1:\"1\";}s:15:\"popupWindowSize\";s:7:\"900x900\";s:4:\"RTE.\";a:1:{s:15:\"popupWindowSize\";s:7:\"900x900\";}}s:6:\"setup.\";a:1:{s:8:\"default.\";a:1:{s:8:\"edit_RTE\";s:1:\"1\";}}s:9:\"admPanel.\";a:1:{s:7:\"enable.\";a:4:{s:7:\"preview\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:4:\"info\";s:1:\"1\";s:3:\"all\";s:1:\"1\";}}s:12:\"TCAdefaults.\";a:1:{s:9:\"sys_note.\";a:2:{s:6:\"author\";s:0:\"\";s:5:\"email\";s:0:\"\";}}}s:8:\"sections\";a:0:{}s:5:\"match\";a:0:{}}i:1;s:32:\"5916b6e2c122db1ec8d4bd89cb38e1ba\";}'),(9,'a2216dd799ebe77d9cf38093419b5752',2145909600,'a:2:{i:0;a:3:{s:8:\"TSconfig\";a:5:{s:4:\"mod.\";a:6:{s:9:\"web_list.\";a:4:{s:28:\"enableDisplayBigControlPanel\";s:9:\"activated\";s:15:\"enableClipBoard\";s:10:\"selectable\";s:22:\"enableLocalizationView\";s:10:\"selectable\";s:18:\"tableDisplayOrder.\";a:11:{s:9:\"be_users.\";a:1:{s:5:\"after\";s:9:\"be_groups\";}s:15:\"sys_filemounts.\";a:1:{s:5:\"after\";s:8:\"be_users\";}s:17:\"sys_file_storage.\";a:1:{s:5:\"after\";s:14:\"sys_filemounts\";}s:13:\"sys_language.\";a:1:{s:5:\"after\";s:16:\"sys_file_storage\";}s:23:\"pages_language_overlay.\";a:1:{s:6:\"before\";s:5:\"pages\";}s:9:\"fe_users.\";a:2:{s:5:\"after\";s:9:\"fe_groups\";s:6:\"before\";s:5:\"pages\";}s:13:\"sys_template.\";a:1:{s:5:\"after\";s:5:\"pages\";}s:15:\"backend_layout.\";a:1:{s:5:\"after\";s:5:\"pages\";}s:11:\"sys_domain.\";a:1:{s:5:\"after\";s:12:\"sys_template\";}s:11:\"tt_content.\";a:1:{s:5:\"after\";s:33:\"pages,backend_layout,sys_template\";}s:13:\"sys_category.\";a:1:{s:5:\"after\";s:10:\"tt_content\";}}}s:8:\"wizards.\";a:3:{s:10:\"newRecord.\";a:1:{s:6:\"pages.\";a:1:{s:5:\"show.\";a:3:{s:10:\"pageInside\";s:1:\"1\";s:9:\"pageAfter\";s:1:\"1\";s:18:\"pageSelectPosition\";s:1:\"1\";}}}s:18:\"newContentElement.\";a:1:{s:12:\"wizardItems.\";a:4:{s:7:\"common.\";a:3:{s:6:\"header\";s:81:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common\";s:9:\"elements.\";a:8:{s:7:\"header.\";a:4:{s:14:\"iconIdentifier\";s:14:\"content-header\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_headerOnly_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_headerOnly_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:6:\"header\";}}s:5:\"text.\";a:4:{s:14:\"iconIdentifier\";s:12:\"content-text\";s:5:\"title\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_regularText_title\";s:11:\"description\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_regularText_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"text\";}}s:8:\"textpic.\";a:4:{s:14:\"iconIdentifier\";s:15:\"content-textpic\";s:5:\"title\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_title\";s:11:\"description\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_description\";s:21:\"tt_content_defValues.\";a:2:{s:5:\"CType\";s:7:\"textpic\";s:11:\"imageorient\";s:2:\"17\";}}s:6:\"image.\";a:4:{s:14:\"iconIdentifier\";s:13:\"content-image\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:5:\"image\";}}s:8:\"bullets.\";a:4:{s:14:\"iconIdentifier\";s:15:\"content-bullets\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_bulletList_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_bulletList_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:7:\"bullets\";}}s:6:\"table.\";a:4:{s:14:\"iconIdentifier\";s:13:\"content-table\";s:5:\"title\";s:93:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_table_title\";s:11:\"description\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_table_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:5:\"table\";}}s:10:\"textmedia.\";a:4:{s:14:\"iconIdentifier\";s:15:\"content-textpic\";s:5:\"title\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textMedia_title\";s:11:\"description\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textMedia_description\";s:21:\"tt_content_defValues.\";a:2:{s:5:\"CType\";s:9:\"textmedia\";s:11:\"imageorient\";s:2:\"17\";}}s:8:\"uploads.\";a:4:{s:14:\"iconIdentifier\";s:23:\"content-special-uploads\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:7:\"uploads\";}}}s:4:\"show\";s:65:\"header,image,bullets,table,header,textmedia,bullets,table,uploads\";}s:8:\"special.\";a:3:{s:6:\"header\";s:82:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special\";s:9:\"elements.\";a:5:{s:8:\"uploads.\";a:4:{s:14:\"iconIdentifier\";s:23:\"content-special-uploads\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:7:\"uploads\";}}s:5:\"menu.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-special-menu\";s:5:\"title\";s:94:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_menus_title\";s:11:\"description\";s:100:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_menus_description\";s:21:\"tt_content_defValues.\";a:2:{s:5:\"CType\";s:4:\"menu\";s:9:\"menu_type\";s:1:\"0\";}}s:5:\"html.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-special-html\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_plainHTML_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_plainHTML_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"html\";}}s:4:\"div.\";a:4:{s:14:\"iconIdentifier\";s:19:\"content-special-div\";s:5:\"title\";s:96:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_divider_title\";s:11:\"description\";s:102:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_divider_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:3:\"div\";}}s:9:\"shortcut.\";a:4:{s:14:\"iconIdentifier\";s:24:\"content-special-shortcut\";s:5:\"title\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_shortcut_title\";s:11:\"description\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_shortcut_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:8:\"shortcut\";}}}s:4:\"show\";s:53:\"uploads,menu,html,div,shortcut,menu,html,div,shortcut\";}s:6:\"forms.\";a:3:{s:6:\"header\";s:80:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:forms\";s:9:\"elements.\";a:2:{s:6:\"login.\";a:4:{s:14:\"iconIdentifier\";s:22:\"content-elements-login\";s:5:\"title\";s:92:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:forms_login_title\";s:11:\"description\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:forms_login_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:5:\"login\";}}s:9:\"mailform.\";a:4:{s:14:\"iconIdentifier\";s:25:\"content-elements-mailform\";s:5:\"title\";s:91:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:forms_mail_title\";s:11:\"description\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:forms_mail_description\";s:21:\"tt_content_defValues.\";a:2:{s:5:\"CType\";s:8:\"mailform\";s:8:\"bodytext\";s:176:\"enctype = application/x-www-form-urlencoded\r\nmethod = post\r\nprefix = tx_form\r\npostProcessor {\r\n    1 = mail\r\n    1 {\r\n        recipientEmail =\r\n        senderEmail =\r\n    }\r\n}\r\";}}}s:4:\"show\";s:14:\"login,mailform\";}s:8:\"plugins.\";a:3:{s:6:\"header\";s:82:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins\";s:9:\"elements.\";a:1:{s:8:\"general.\";a:4:{s:14:\"iconIdentifier\";s:14:\"content-plugin\";s:5:\"title\";s:96:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins_general_title\";s:11:\"description\";s:102:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins_general_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"list\";}}}s:4:\"show\";s:1:\"*\";}}}s:5:\"form.\";a:2:{s:9:\"defaults.\";a:2:{s:8:\"showTabs\";s:23:\"elements, options, form\";s:5:\"tabs.\";a:3:{s:9:\"elements.\";a:2:{s:14:\"showAccordions\";s:26:\"basic, predefined, content\";s:11:\"accordions.\";a:3:{s:6:\"basic.\";a:1:{s:11:\"showButtons\";s:106:\"textline, textarea, checkbox, radio, select, fileupload, hidden, password, fieldset, submit, reset, button\";}s:11:\"predefined.\";a:1:{s:11:\"showButtons\";s:38:\"name, email, checkboxgroup, radiogroup\";}s:8:\"content.\";a:1:{s:11:\"showButtons\";s:17:\"header, textblock\";}}}s:8:\"options.\";a:2:{s:14:\"showAccordions\";s:64:\"legend, label, attributes, options, validation, filters, various\";s:11:\"accordions.\";a:4:{s:6:\"label.\";a:1:{s:14:\"showProperties\";s:5:\"label\";}s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:490:\"accept, accept-charset, accesskey, action, alt, autocomplete, autofocus, checked, class, cols, contenteditable, contextmenu, dir, draggable, dropzone, disabled, enctype, hidden, height, id, inputmode, label, lang, list, max, maxlength, method, min, minlength, multiple, name, novalidate, pattern, placeholder, readonly, required, rows, selected, selectionDirection, selectionEnd, selectionStart, size, spellcheck, src, step, style, tabindex, text, title, translate, type, value, width, wrap\";}s:11:\"validation.\";a:2:{s:9:\"showRules\";s:196:\"alphabetic, alphanumeric, between, date, digit, email, equals, fileallowedtypes, filemaximumsize, fileminimumsize, float, greaterthan, inarray, integer, ip, length, lessthan, regexp, required, uri\";s:6:\"rules.\";a:20:{s:11:\"alphabetic.\";a:1:{s:14:\"showProperties\";s:44:\"message, error, showMessage, allowWhiteSpace\";}s:13:\"alphanumeric.\";a:1:{s:14:\"showProperties\";s:44:\"message, error, showMessage, allowWhiteSpace\";}s:8:\"between.\";a:1:{s:14:\"showProperties\";s:56:\"message, error, showMessage, minimum, maximum, inclusive\";}s:5:\"date.\";a:1:{s:14:\"showProperties\";s:35:\"message, error, showMessage, format\";}s:6:\"digit.\";a:1:{s:14:\"showProperties\";s:27:\"message, error, showMessage\";}s:6:\"email.\";a:1:{s:14:\"showProperties\";s:27:\"message, error, showMessage\";}s:7:\"equals.\";a:1:{s:14:\"showProperties\";s:34:\"message, error, showMessage, field\";}s:17:\"fileallowedtypes.\";a:1:{s:14:\"showProperties\";s:34:\"message, error, showMessage, types\";}s:16:\"filemaximumsize.\";a:1:{s:14:\"showProperties\";s:36:\"message, error, showMessage, maximum\";}s:16:\"fileminimumsize.\";a:1:{s:14:\"showProperties\";s:36:\"message, error, showMessage, minimum\";}s:6:\"float.\";a:1:{s:14:\"showProperties\";s:27:\"message, error, showMessage\";}s:12:\"greaterthan.\";a:1:{s:14:\"showProperties\";s:36:\"message, error, showMessage, minimum\";}s:8:\"inarray.\";a:1:{s:14:\"showProperties\";s:42:\"message, error, showMessage, array, strict\";}s:8:\"integer.\";a:1:{s:14:\"showProperties\";s:27:\"message, error, showMessage\";}s:3:\"ip.\";a:1:{s:14:\"showProperties\";s:27:\"message, error, showMessage\";}s:7:\"length.\";a:1:{s:14:\"showProperties\";s:45:\"message, error, showMessage, minimum, maximum\";}s:9:\"lessthan.\";a:1:{s:14:\"showProperties\";s:36:\"message, error, showMessage, maximum\";}s:7:\"regexp.\";a:1:{s:14:\"showProperties\";s:39:\"message, error, showMessage, expression\";}s:9:\"required.\";a:1:{s:14:\"showProperties\";s:27:\"message, error, showMessage\";}s:4:\"uri.\";a:1:{s:14:\"showProperties\";s:27:\"message, error, showMessage\";}}}s:10:\"filtering.\";a:2:{s:11:\"showFilters\";s:123:\"alphabetic, alphanumeric, currency, digit, integer, lowercase, regexp, removexss, stripnewlines, titlecase, trim, uppercase\";s:8:\"filters.\";a:12:{s:11:\"alphabetic.\";a:1:{s:14:\"showProperties\";s:15:\"allowWhiteSpace\";}s:13:\"alphanumeric.\";a:1:{s:14:\"showProperties\";s:15:\"allowWhiteSpace\";}s:9:\"currency.\";a:1:{s:14:\"showProperties\";s:31:\"decimalPoint, thousandSeparator\";}s:6:\"digit.\";a:1:{s:14:\"showProperties\";s:0:\"\";}s:8:\"integer.\";a:1:{s:14:\"showProperties\";s:0:\"\";}s:10:\"lowercase.\";a:1:{s:14:\"showProperties\";s:0:\"\";}s:7:\"regexp.\";a:1:{s:14:\"showProperties\";s:10:\"expression\";}s:10:\"removexss.\";a:1:{s:14:\"showProperties\";s:0:\"\";}s:14:\"stripnewlines.\";a:1:{s:14:\"showProperties\";s:0:\"\";}s:10:\"titlecase.\";a:1:{s:14:\"showProperties\";s:0:\"\";}s:5:\"trim.\";a:1:{s:14:\"showProperties\";s:13:\"characterList\";}s:10:\"uppercase.\";a:1:{s:14:\"showProperties\";s:0:\"\";}}}}}s:5:\"form.\";a:2:{s:14:\"showAccordions\";s:44:\"behaviour, prefix, attributes, postProcessor\";s:11:\"accordions.\";a:1:{s:14:\"postProcessor.\";a:2:{s:18:\"showPostProcessors\";s:14:\"mail, redirect\";s:15:\"postProcessors.\";a:2:{s:5:\"mail.\";a:1:{s:14:\"showProperties\";s:36:\"recipientEmail, senderEmail, subject\";}s:9:\"redirect.\";a:1:{s:14:\"showProperties\";s:11:\"destination\";}}}}}}}s:9:\"elements.\";a:19:{s:5:\"form.\";a:1:{s:11:\"accordions.\";a:1:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:79:\"accept, action, dir, enctype, lang, method, novalidate, class, id, style, title\";}}}s:7:\"button.\";a:2:{s:14:\"showAccordions\";s:17:\"label, attributes\";s:11:\"accordions.\";a:1:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:22:\"name, value, class, id\";}}}s:9:\"checkbox.\";a:2:{s:14:\"showAccordions\";s:29:\"label, attributes, validation\";s:11:\"accordions.\";a:2:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:41:\"name, value, class, id, checked, required\";}s:11:\"validation.\";a:1:{s:9:\"showRules\";s:8:\"required\";}}}s:9:\"fieldset.\";a:2:{s:14:\"showAccordions\";s:18:\"legend, attributes\";s:11:\"accordions.\";a:1:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:9:\"class, id\";}}}s:11:\"fileupload.\";a:2:{s:14:\"showAccordions\";s:29:\"label, attributes, validation\";s:11:\"accordions.\";a:2:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:25:\"name, class, id, required\";}s:11:\"validation.\";a:1:{s:9:\"showRules\";s:60:\"required, fileallowedtypes, filemaximumsize, fileminimumsize\";}}}s:7:\"hidden.\";a:2:{s:14:\"showAccordions\";s:10:\"attributes\";s:11:\"accordions.\";a:1:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:11:\"name, value\";}}}s:9:\"password.\";a:2:{s:14:\"showAccordions\";s:29:\"label, attributes, validation\";s:11:\"accordions.\";a:2:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:52:\"name, placeholder, class, id, autocomplete, required\";}s:11:\"validation.\";a:1:{s:9:\"showRules\";s:16:\"required, equals\";}}}s:6:\"radio.\";a:2:{s:14:\"showAccordions\";s:29:\"label, attributes, validation\";s:11:\"accordions.\";a:2:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:41:\"name, value, class, id, checked, required\";}s:11:\"validation.\";a:1:{s:9:\"showRules\";s:8:\"required\";}}}s:6:\"reset.\";a:2:{s:14:\"showAccordions\";s:17:\"label, attributes\";s:11:\"accordions.\";a:1:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:14:\"value,class,id\";}}}s:7:\"select.\";a:2:{s:14:\"showAccordions\";s:38:\"label, attributes, options, validation\";s:11:\"accordions.\";a:2:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:41:\"name, size, class, id, multiple, required\";}s:11:\"validation.\";a:1:{s:9:\"showRules\";s:8:\"required\";}}}s:7:\"submit.\";a:2:{s:14:\"showAccordions\";s:17:\"label, attributes\";s:11:\"accordions.\";a:1:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:14:\"value,class,id\";}}}s:9:\"textarea.\";a:2:{s:14:\"showAccordions\";s:38:\"label, attributes, validation, filters\";s:11:\"accordions.\";a:3:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:56:\"name, placeholder, cols, rows, class, id, required, text\";}s:10:\"filtering.\";a:1:{s:11:\"showFilters\";s:86:\"alphabetic, alphanumeric, lowercase, regexp, stripnewlines, titlecase, trim, uppercase\";}s:11:\"validation.\";a:1:{s:9:\"showRules\";s:50:\"alphabetic, alphanumeric, length, regexp, required\";}}}s:9:\"textline.\";a:2:{s:14:\"showAccordions\";s:38:\"label, attributes, validation, filters\";s:11:\"accordions.\";a:3:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:58:\"name, placeholder, type, class, id, autocomplete, required\";}s:11:\"validation.\";a:1:{s:9:\"showRules\";s:144:\"alphabetic, alphanumeric, between, date, digit, email, equals, float, greaterthan, inarray, integer, ip, length, lessthan, regexp, required, uri\";}s:10:\"filtering.\";a:1:{s:11:\"showFilters\";s:97:\"alphabetic, alphanumeric, currency, digit, integer, lowercase, regexp, titlecase, trim, uppercase\";}}}s:5:\"name.\";a:1:{s:14:\"showAccordions\";s:15:\"legend, various\";}s:6:\"email.\";a:2:{s:14:\"showAccordions\";s:38:\"label, attributes, validation, filters\";s:11:\"accordions.\";a:3:{s:11:\"attributes.\";a:1:{s:14:\"showProperties\";s:58:\"name, placeholder, type, class, id, autocomplete, required\";}s:11:\"validation.\";a:1:{s:9:\"showRules\";s:144:\"alphabetic, alphanumeric, between, date, digit, email, equals, float, greaterthan, inarray, integer, ip, length, lessthan, regexp, required, uri\";}s:10:\"filtering.\";a:1:{s:11:\"showFilters\";s:97:\"alphabetic, alphanumeric, currency, digit, integer, lowercase, regexp, titlecase, trim, uppercase\";}}}s:14:\"checkboxgroup.\";a:2:{s:14:\"showAccordions\";s:36:\"legend, options, various, validation\";s:11:\"accordions.\";a:1:{s:11:\"validation.\";a:1:{s:9:\"showRules\";s:8:\"required\";}}}s:11:\"radiogroup.\";a:2:{s:14:\"showAccordions\";s:36:\"legend, options, various, validation\";s:11:\"accordions.\";a:1:{s:11:\"validation.\";a:1:{s:9:\"showRules\";s:8:\"required\";}}}s:7:\"header.\";a:1:{s:14:\"showAccordions\";s:7:\"various\";}s:10:\"textblock.\";a:1:{s:14:\"showAccordions\";s:7:\"various\";}}}}s:9:\"web_view.\";a:1:{s:19:\"previewFrameWidths.\";a:11:{s:5:\"1280.\";a:1:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";}s:5:\"1024.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:tablet\";}s:4:\"960.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"800.\";a:1:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";}s:4:\"768.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:tablet\";}s:4:\"600.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:tablet\";}s:4:\"640.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"480.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"400.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"360.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}s:4:\"300.\";a:1:{s:5:\"label\";s:64:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:mobile\";}}}s:7:\"SHARED.\";a:2:{s:20:\"defaultLanguageLabel\";s:7:\"Deutsch\";s:19:\"defaultLanguageFlag\";s:2:\"ch\";}s:10:\"file_list.\";a:1:{s:28:\"enableDisplayBigControlPanel\";s:9:\"activated\";}s:11:\"web_layout.\";a:1:{s:5:\"menu.\";a:1:{s:9:\"function.\";a:2:{i:0;s:1:\"0\";i:3;s:1:\"0\";}}}}s:4:\"RTE.\";a:4:{s:8:\"default.\";a:20:{s:4:\"skin\";s:35:\"EXT:t3skin/rtehtmlarea/htmlarea.css\";s:3:\"FE.\";a:20:{s:4:\"skin\";s:35:\"EXT:t3skin/rtehtmlarea/htmlarea.css\";s:3:\"FE.\";a:18:{s:4:\"skin\";s:35:\"EXT:t3skin/rtehtmlarea/htmlarea.css\";s:5:\"proc.\";a:15:{s:12:\"overruleMode\";s:6:\"ts_css\";s:21:\"dontConvBRtoParagraph\";s:1:\"1\";s:19:\"preserveDIVSections\";s:1:\"1\";s:16:\"allowTagsOutside\";s:69:\"address, article, aside, blockquote, footer, header, hr, nav, section\";s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:15:\"keepPDIVattribs\";s:67:\"id, title, dir, lang, xml:lang, itemscope, itemtype, itemprop,style\";s:26:\"transformBoldAndItalicTags\";s:1:\"1\";s:14:\"dontUndoHSC_db\";s:1:\"1\";s:11:\"dontHSC_rte\";s:1:\"1\";s:18:\"entryHTMLparser_db\";s:1:\"1\";s:19:\"entryHTMLparser_db.\";a:5:{s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:5:\"tags.\";a:28:{s:4:\"img.\";a:2:{s:14:\"allowedAttribs\";s:1:\"0\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:5:\"span.\";a:3:{s:10:\"fixAttrib.\";a:1:{s:6:\"style.\";a:0:{}}s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:2:\"p.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}s:3:\"hr.\";a:1:{s:14:\"allowedAttribs\";s:11:\"class,style\";}s:2:\"b.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"bdo.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"big.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:11:\"blockquote.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"cite.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"code.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"del.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"dfn.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"em.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"i.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"ins.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"kbd.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"label.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"q.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"samp.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"small.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strike.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strong.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sub.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sup.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"tt.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"u.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"var.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"div.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}}s:10:\"removeTags\";s:63:\"center, font, link, meta, o:p, sdfield, strike, style, title, u\";s:18:\"keepNonMatchedTags\";s:7:\"protect\";}s:14:\"HTMLparser_db.\";a:1:{s:8:\"noAttrib\";s:2:\"br\";}s:17:\"exitHTMLparser_db\";s:1:\"1\";s:18:\"exitHTMLparser_db.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";}s:14:\"allowedClasses\";s:301:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail,align-left, align-center, align-right, align-justify,csc-frame-frame1, csc-frame-frame2,component-items, action-items,component-items-ordered, action-items-ordered,important, name-of-person, detail,indent\";}s:15:\"enableWordClean\";s:1:\"1\";s:16:\"removeTrailingBR\";s:1:\"1\";s:14:\"removeComments\";s:1:\"1\";s:10:\"removeTags\";s:37:\"center, font, o:p, sdfield, strike, u\";s:21:\"removeTagsAndContents\";s:32:\"link, meta, script, style, title\";s:11:\"showButtons\";s:493:\"blockstylelabel, blockstyle, textstylelabel, textstyle,formatblock, bold, italic, subscript, superscript,orderedlist, unorderedlist, outdent, indent, textindicator,insertcharacter, link, table, findreplace, chMode, removeformat, undo, redo, about,toggleborders, tableproperties,rowproperties, rowinsertabove, rowinsertunder, rowdelete, rowsplit,columninsertbefore, columninsertafter, columndelete, columnsplit,cellproperties, cellinsertbefore, cellinsertafter, celldelete, cellsplit, cellmerge\";s:23:\"keepButtonGroupTogether\";s:1:\"1\";s:13:\"showStatusBar\";s:1:\"0\";s:8:\"buttons.\";a:7:{s:12:\"formatblock.\";a:1:{s:11:\"removeItems\";s:11:\"pre,address\";}s:11:\"blockstyle.\";a:1:{s:5:\"tags.\";a:3:{s:4:\"div.\";a:1:{s:14:\"allowedClasses\";s:72:\"align-left, align-center, align-right,csc-frame-frame1, csc-frame-frame2\";}s:6:\"table.\";a:1:{s:14:\"allowedClasses\";s:34:\"csc-frame-frame1, csc-frame-frame2\";}s:3:\"td.\";a:1:{s:14:\"allowedClasses\";s:37:\"align-left, align-center, align-right\";}}}s:10:\"textstyle.\";a:1:{s:5:\"tags.\";a:1:{s:5:\"span.\";a:1:{s:14:\"allowedClasses\";s:33:\"important, name-of-person, detail\";}}}s:5:\"link.\";a:5:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:14:\"allowedClasses\";s:96:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail\";}}s:5:\"page.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:13:\"internal-link\";}}}s:4:\"url.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:24:\"external-link-new-window\";}}}s:5:\"file.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:8:\"download\";}}}s:5:\"mail.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:4:\"mail\";}}}}s:14:\"toggleborders.\";a:1:{s:18:\"setOnTableCreation\";s:1:\"1\";}s:5:\"bold.\";a:1:{s:6:\"hotKey\";s:1:\"b\";}s:7:\"italic.\";a:1:{s:6:\"hotKey\";s:1:\"i\";}}s:41:\"disableAlignmentFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableSpacingFieldsetInTableOperations\";s:1:\"1\";s:37:\"disableColorFieldsetInTableOperations\";s:1:\"1\";s:38:\"disableLayoutFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableBordersFieldsetInTableOperations\";s:1:\"1\";s:7:\"schema.\";a:1:{s:8:\"sources.\";a:1:{s:9:\"schemaOrg\";s:63:\"EXT:rtehtmlarea/extensions/MicrodataSchema/res/schemaOrgAll.rdf\";}}s:11:\"hideButtons\";s:202:\"chMode, blockstyle, textstyle, underline, strikethrough, subscript, superscript, lefttoright, righttoleft, left, center, right, justifyfull, table, inserttag, findreplace, removeformat, copy, cut, paste\";}s:5:\"proc.\";a:15:{s:12:\"overruleMode\";s:6:\"ts_css\";s:21:\"dontConvBRtoParagraph\";s:1:\"1\";s:19:\"preserveDIVSections\";s:1:\"1\";s:16:\"allowTagsOutside\";s:69:\"address, article, aside, blockquote, footer, header, hr, nav, section\";s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:15:\"keepPDIVattribs\";s:67:\"id, title, dir, lang, xml:lang, itemscope, itemtype, itemprop,style\";s:26:\"transformBoldAndItalicTags\";s:1:\"1\";s:14:\"dontUndoHSC_db\";s:1:\"1\";s:11:\"dontHSC_rte\";s:1:\"1\";s:18:\"entryHTMLparser_db\";s:1:\"1\";s:19:\"entryHTMLparser_db.\";a:5:{s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:5:\"tags.\";a:28:{s:4:\"img.\";a:2:{s:14:\"allowedAttribs\";s:1:\"0\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:5:\"span.\";a:3:{s:10:\"fixAttrib.\";a:1:{s:6:\"style.\";a:0:{}}s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:2:\"p.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}s:3:\"hr.\";a:1:{s:14:\"allowedAttribs\";s:11:\"class,style\";}s:2:\"b.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"bdo.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"big.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:11:\"blockquote.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"cite.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"code.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"del.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"dfn.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"em.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"i.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"ins.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"kbd.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"label.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"q.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"samp.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"small.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strike.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strong.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sub.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sup.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"tt.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"u.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"var.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"div.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}}s:10:\"removeTags\";s:63:\"center, font, link, meta, o:p, sdfield, strike, style, title, u\";s:18:\"keepNonMatchedTags\";s:7:\"protect\";}s:14:\"HTMLparser_db.\";a:1:{s:8:\"noAttrib\";s:2:\"br\";}s:17:\"exitHTMLparser_db\";s:1:\"1\";s:18:\"exitHTMLparser_db.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";}s:14:\"allowedClasses\";s:301:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail,align-left, align-center, align-right, align-justify,csc-frame-frame1, csc-frame-frame2,component-items, action-items,component-items-ordered, action-items-ordered,important, name-of-person, detail,indent\";}s:15:\"enableWordClean\";s:1:\"1\";s:16:\"removeTrailingBR\";s:1:\"1\";s:14:\"removeComments\";s:1:\"1\";s:10:\"removeTags\";s:37:\"center, font, o:p, sdfield, strike, u\";s:21:\"removeTagsAndContents\";s:32:\"link, meta, script, style, title\";s:11:\"showButtons\";s:112:\"orderedlist,unorderedlist,bold,italic,subscript,superscript,link,unlink,blockstyle,textstyle,chMode,removeformat\";s:23:\"keepButtonGroupTogether\";s:1:\"1\";s:13:\"showStatusBar\";s:1:\"0\";s:8:\"buttons.\";a:7:{s:12:\"formatblock.\";a:1:{s:11:\"removeItems\";s:11:\"pre,address\";}s:11:\"blockstyle.\";a:2:{s:5:\"tags.\";a:4:{s:4:\"div.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}s:6:\"table.\";a:1:{s:14:\"allowedClasses\";s:0:\"\";}s:3:\"td.\";a:1:{s:14:\"allowedClasses\";s:0:\"\";}s:2:\"p.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}}s:18:\"showTagFreeClasses\";s:1:\"1\";}s:10:\"textstyle.\";a:2:{s:5:\"tags.\";a:1:{s:5:\"span.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}}s:18:\"showTagFreeClasses\";s:1:\"1\";}s:5:\"link.\";a:5:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:14:\"allowedClasses\";s:96:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail\";}}s:5:\"page.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:13:\"internal-link\";}}}s:4:\"url.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:24:\"external-link-new-window\";}}}s:5:\"file.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:8:\"download\";}}}s:5:\"mail.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:4:\"mail\";}}}}s:14:\"toggleborders.\";a:1:{s:18:\"setOnTableCreation\";s:1:\"1\";}s:5:\"bold.\";a:1:{s:6:\"hotKey\";s:1:\"b\";}s:7:\"italic.\";a:1:{s:6:\"hotKey\";s:1:\"i\";}}s:41:\"disableAlignmentFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableSpacingFieldsetInTableOperations\";s:1:\"1\";s:37:\"disableColorFieldsetInTableOperations\";s:1:\"1\";s:38:\"disableLayoutFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableBordersFieldsetInTableOperations\";s:1:\"1\";s:7:\"schema.\";a:1:{s:8:\"sources.\";a:1:{s:9:\"schemaOrg\";s:63:\"EXT:rtehtmlarea/extensions/MicrodataSchema/res/schemaOrgAll.rdf\";}}s:11:\"contentCSS.\";a:1:{s:7:\"mainCSS\";s:54:\"EXT:templatebootstrap/Resources/Public/Backend/RTE.css\";}s:12:\"contextMenu.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}s:5:\"proc.\";a:15:{s:12:\"overruleMode\";s:6:\"ts_css\";s:21:\"dontConvBRtoParagraph\";s:1:\"1\";s:19:\"preserveDIVSections\";s:1:\"1\";s:16:\"allowTagsOutside\";s:69:\"address, article, aside, blockquote, footer, header, hr, nav, section\";s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:15:\"keepPDIVattribs\";s:67:\"id, title, dir, lang, xml:lang, itemscope, itemtype, itemprop,style\";s:26:\"transformBoldAndItalicTags\";s:1:\"1\";s:14:\"dontUndoHSC_db\";s:1:\"1\";s:11:\"dontHSC_rte\";s:1:\"1\";s:18:\"entryHTMLparser_db\";s:1:\"1\";s:19:\"entryHTMLparser_db.\";a:5:{s:9:\"allowTags\";s:387:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer,header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, samp, sdfield, section, small,span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:5:\"tags.\";a:28:{s:4:\"img.\";a:2:{s:14:\"allowedAttribs\";s:1:\"0\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:5:\"span.\";a:3:{s:10:\"fixAttrib.\";a:1:{s:6:\"style.\";a:0:{}}s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:15:\"rmTagIfNoAttrib\";s:1:\"1\";}s:2:\"p.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}s:3:\"hr.\";a:1:{s:14:\"allowedAttribs\";s:11:\"class,style\";}s:2:\"b.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"bdo.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"big.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:11:\"blockquote.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"cite.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"code.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"del.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"dfn.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"em.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"i.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"ins.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"kbd.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"label.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"q.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:5:\"samp.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:6:\"small.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strike.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:7:\"strong.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sub.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"sup.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:3:\"tt.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:2:\"u.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"var.\";a:1:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";}s:4:\"div.\";a:2:{s:14:\"allowedAttribs\";s:74:\"id, title, dir, lang, xml:lang, class, itemscope, itemtype, itemprop,style\";s:10:\"fixAttrib.\";a:1:{s:6:\"align.\";a:1:{s:5:\"unset\";s:1:\"1\";}}}}s:10:\"removeTags\";s:63:\"center, font, link, meta, o:p, sdfield, strike, style, title, u\";s:18:\"keepNonMatchedTags\";s:7:\"protect\";}s:14:\"HTMLparser_db.\";a:1:{s:8:\"noAttrib\";s:2:\"br\";}s:17:\"exitHTMLparser_db\";s:1:\"1\";s:18:\"exitHTMLparser_db.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";}s:14:\"allowedClasses\";s:301:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail,align-left, align-center, align-right, align-justify,csc-frame-frame1, csc-frame-frame2,component-items, action-items,component-items-ordered, action-items-ordered,important, name-of-person, detail,indent\";}s:15:\"enableWordClean\";s:1:\"1\";s:16:\"removeTrailingBR\";s:1:\"1\";s:14:\"removeComments\";s:1:\"1\";s:10:\"removeTags\";s:37:\"center, font, o:p, sdfield, strike, u\";s:21:\"removeTagsAndContents\";s:32:\"link, meta, script, style, title\";s:11:\"showButtons\";s:112:\"orderedlist,unorderedlist,bold,italic,subscript,superscript,link,unlink,blockstyle,textstyle,chMode,removeformat\";s:23:\"keepButtonGroupTogether\";s:1:\"1\";s:13:\"showStatusBar\";s:1:\"1\";s:8:\"buttons.\";a:7:{s:12:\"formatblock.\";a:1:{s:11:\"removeItems\";s:11:\"pre,address\";}s:11:\"blockstyle.\";a:2:{s:5:\"tags.\";a:4:{s:4:\"div.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}s:6:\"table.\";a:1:{s:14:\"allowedClasses\";s:0:\"\";}s:3:\"td.\";a:1:{s:14:\"allowedClasses\";s:0:\"\";}s:2:\"p.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}}s:18:\"showTagFreeClasses\";s:1:\"1\";}s:10:\"textstyle.\";a:2:{s:5:\"tags.\";a:1:{s:5:\"span.\";a:1:{s:14:\"allowedClasses\";s:12:\"examplestyle\";}}s:18:\"showTagFreeClasses\";s:1:\"1\";}s:5:\"link.\";a:5:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:14:\"allowedClasses\";s:96:\"external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail\";}}s:5:\"page.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:13:\"internal-link\";}}}s:4:\"url.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:24:\"external-link-new-window\";}}}s:5:\"file.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:8:\"download\";}}}s:5:\"mail.\";a:1:{s:11:\"properties.\";a:1:{s:6:\"class.\";a:1:{s:7:\"default\";s:4:\"mail\";}}}}s:14:\"toggleborders.\";a:1:{s:18:\"setOnTableCreation\";s:1:\"1\";}s:5:\"bold.\";a:1:{s:6:\"hotKey\";s:1:\"b\";}s:7:\"italic.\";a:1:{s:6:\"hotKey\";s:1:\"i\";}}s:41:\"disableAlignmentFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableSpacingFieldsetInTableOperations\";s:1:\"1\";s:37:\"disableColorFieldsetInTableOperations\";s:1:\"1\";s:38:\"disableLayoutFieldsetInTableOperations\";s:1:\"1\";s:39:\"disableBordersFieldsetInTableOperations\";s:1:\"1\";s:7:\"schema.\";a:1:{s:8:\"sources.\";a:1:{s:9:\"schemaOrg\";s:63:\"EXT:rtehtmlarea/extensions/MicrodataSchema/res/schemaOrgAll.rdf\";}}s:11:\"contentCSS.\";a:1:{s:7:\"mainCSS\";s:54:\"EXT:templatebootstrap/Resources/Public/Backend/RTE.css\";}s:12:\"contextMenu.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}s:7:\"config.\";a:1:{s:11:\"tt_content.\";a:1:{s:9:\"bodytext.\";a:2:{s:5:\"proc.\";a:1:{s:12:\"overruleMode\";s:6:\"ts_css\";}s:6:\"types.\";a:2:{s:5:\"text.\";a:1:{s:5:\"proc.\";a:1:{s:12:\"overruleMode\";s:6:\"ts_css\";}}s:8:\"textpic.\";a:1:{s:5:\"proc.\";a:1:{s:12:\"overruleMode\";s:6:\"ts_css\";}}}}}}s:8:\"classes.\";a:13:{s:11:\"align-left.\";a:2:{s:4:\"name\";s:81:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_tooltips.xlf:justifyleft\";s:5:\"value\";s:17:\"text-align: left;\";}s:13:\"align-center.\";a:2:{s:4:\"name\";s:83:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_tooltips.xlf:justifycenter\";s:5:\"value\";s:19:\"text-align: center;\";}s:12:\"align-right.\";a:2:{s:4:\"name\";s:82:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_tooltips.xlf:justifyright\";s:5:\"value\";s:18:\"text-align: right;\";}s:17:\"csc-frame-frame1.\";a:2:{s:4:\"name\";s:84:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:frame-frame1\";s:5:\"value\";s:53:\"background-color: #EDEBF1; border: 1px solid #333333;\";}s:17:\"csc-frame-frame2.\";a:2:{s:4:\"name\";s:84:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:frame-frame2\";s:5:\"value\";s:53:\"background-color: #F5FFAA; border: 1px solid #333333;\";}s:10:\"important.\";a:2:{s:4:\"name\";s:81:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:important\";s:5:\"value\";s:15:\"color: #8A0020;\";}s:15:\"name-of-person.\";a:2:{s:4:\"name\";s:86:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:name-of-person\";s:5:\"value\";s:15:\"color: #10007B;\";}s:7:\"detail.\";a:2:{s:4:\"name\";s:78:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:detail\";s:5:\"value\";s:15:\"color: #186900;\";}s:16:\"component-items.\";a:2:{s:4:\"name\";s:87:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:component-items\";s:5:\"value\";s:15:\"color: #186900;\";}s:13:\"action-items.\";a:2:{s:4:\"name\";s:84:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:action-items\";s:5:\"value\";s:15:\"color: #8A0020;\";}s:24:\"component-items-ordered.\";a:2:{s:4:\"name\";s:87:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:component-items\";s:5:\"value\";s:15:\"color: #186900;\";}s:21:\"action-items-ordered.\";a:2:{s:4:\"name\";s:84:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_contentcss.xlf:action-items\";s:5:\"value\";s:15:\"color: #8A0020;\";}s:13:\"examplestyle.\";a:2:{s:4:\"name\";s:89:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:rte.classes.examplestyle\";s:8:\"requires\";s:19:\"foo-class,bar-class\";}}s:14:\"classesAnchor.\";a:6:{s:13:\"externalLink.\";a:3:{s:5:\"class\";s:13:\"external-link\";s:4:\"type\";s:3:\"url\";s:9:\"titleText\";s:103:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:external_link_titleText\";}s:24:\"externalLinkInNewWindow.\";a:3:{s:5:\"class\";s:24:\"external-link-new-window\";s:4:\"type\";s:3:\"url\";s:9:\"titleText\";s:114:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:external_link_new_window_titleText\";}s:13:\"internalLink.\";a:3:{s:5:\"class\";s:13:\"internal-link\";s:4:\"type\";s:4:\"page\";s:9:\"titleText\";s:103:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:internal_link_titleText\";}s:24:\"internalLinkInNewWindow.\";a:3:{s:5:\"class\";s:24:\"internal-link-new-window\";s:4:\"type\";s:4:\"page\";s:9:\"titleText\";s:114:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:internal_link_new_window_titleText\";}s:9:\"download.\";a:3:{s:5:\"class\";s:8:\"download\";s:4:\"type\";s:4:\"file\";s:9:\"titleText\";s:98:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:download_titleText\";}s:5:\"mail.\";a:3:{s:5:\"class\";s:4:\"mail\";s:4:\"type\";s:4:\"mail\";s:9:\"titleText\";s:94:\"LLL:EXT:rtehtmlarea/Resources/Private/Language/locallang_accessibilityicons.xlf:mail_titleText\";}}}s:8:\"TCEFORM.\";a:2:{s:11:\"tt_content.\";a:6:{s:12:\"imageorient.\";a:2:{s:6:\"types.\";a:1:{s:6:\"image.\";a:1:{s:11:\"removeItems\";s:18:\"8,9,10,17,18,25,26\";}}s:11:\"removeItems\";s:5:\"0,8,9\";}s:5:\"date.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:11:\"categories.\";a:1:{s:8:\"disabled\";s:1:\"0\";}s:14:\"header_layout.\";a:2:{s:9:\"keepItems\";s:5:\"2,3,4\";s:10:\"altLabels.\";a:4:{i:1;s:91:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.header_layout.1\";i:2;s:91:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.header_layout.2\";i:3;s:91:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.header_layout.3\";i:4;s:91:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.header_layout.4\";}}s:7:\"layout.\";a:3:{s:8:\"disabled\";s:1:\"0\";s:9:\"keepItems\";s:3:\"0,1\";s:10:\"altLabels.\";a:1:{i:1;s:96:\"LLL:EXT:templatebootstrap/Resources/Private/Language/Backend.xlf:tt_content.layout.examplelayout\";}}s:12:\"imageborder.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}s:6:\"pages.\";a:12:{s:6:\"alias.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:9:\"abstract.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:9:\"keywords.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:11:\"categories.\";a:1:{s:8:\"disabled\";s:1:\"0\";}s:8:\"doktype.\";a:1:{s:11:\"removeItems\";s:1:\"6\";}s:7:\"layout.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:15:\"backend_layout.\";a:1:{s:11:\"removeItems\";s:2:\"-1\";}s:26:\"backend_layout_next_level.\";a:1:{s:11:\"removeItems\";s:2:\"-1\";}s:9:\"newUntil.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:7:\"author.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:13:\"author_email.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:12:\"lastUpdated.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}}s:12:\"TCAdefaults.\";a:2:{s:11:\"tt_content.\";a:3:{s:13:\"header_layout\";s:1:\"2\";s:9:\"imagecols\";s:1:\"1\";s:11:\"imageorient\";s:1:\"2\";}s:10:\"be_groups.\";a:9:{s:13:\"tables_select\";s:168:\"    pages,sys_category,sys_collection,sys_file,sys_file_collection,sys_file_metadata,\r\n    sys_file_reference,backend_layout,pages_language_overlay,tt_content,sys_note\r\";s:18:\"non_exclude_fields\";s:3089:\"    pages_language_overlay:abstract,pages_language_overlay:author,pages_language_overlay:description,\r\n    pages_language_overlay:author_email,pages_language_overlay:media,pages_language_overlay:hidden,pages_language_overlay:keywords,\r\n    pages_language_overlay:nav_title,pages_language_overlay:shortcut_mode,pages_language_overlay:starttime,\r\n    pages_language_overlay:endtime,pages_language_overlay:subtitle,pages_language_overlay:urltype,pages_language_overlay:doktype,\r\n    pages_language_overlay:url,pages_language_overlay:tx_realurl_pathsegment,\r\n\r\n    sys_category:hidden,sys_category:sys_language_uid,sys_category:starttime,sys_category:endtime,sys_category:l10n_parent,\r\n\r\n    sys_file_metadata:categories,sys_file_metadata:title,\r\n\r\n    sys_file_reference:alternative,sys_file_reference:description,sys_file_reference:crop,sys_file_reference:link,\r\n    sys_file_reference:title,sys_file_reference:autoplay,\r\n    sys_file_collection:hidden,sys_file_collection:sys_language_uid,sys_file_collection:starttime,sys_file_collection:endtime,\r\n    sys_file_collection:l10n_parent,\r\n\r\n    pages:newUntil,pages:abstract,pages:fe_group,pages:alias,pages:author,pages:backend_layout_next_level,pages:backend_layout,\r\n    pages:cache_timeout,pages:cache_tags,pages:categories,pages:module,pages:description,pages:tx_realurl_nocache,pages:author_email,\r\n    pages:url_scheme,pages:tx_realurl_exclude,pages:media,pages:nav_hide,pages:hidden,pages:extendToSubpages,pages:is_siteroot,\r\n    pages:keywords,pages:lastUpdated,pages:layout,pages:l18n_cfg,pages:fe_login_mode,pages:nav_title,pages:no_cache,pages:no_search,\r\n    pages:tx_realurl_pathoverride,pages:shortcut_mode,pages:content_from_pid,pages:tx_realurl_pathsegment,pages:starttime,\r\n    pages:php_tree_stop,pages:endtime,pages:subtitle,pages:target,pages:doktype,\r\n\r\n    tt_content:rowDescription,tt_content:fe_group,tt_content:uploads_description,tt_content:uploads_type,\r\n    tt_content:categories,tt_content:date,tt_content:colPos,tt_content:hidden,\r\n    tt_content:table_enclosure,tt_content:table_delimiter,tt_content:table_caption,tt_content:table_tfoot,tt_content:table_header_position,\r\n    tt_content:image_zoom,tt_content:select_key,tt_content:imagecols,\r\n    tt_content:section_frame,tt_content:imagewidth,tt_content:imageheight,\r\n    tt_content:sectionIndex,tt_content:sys_language_uid,tt_content:layout,tt_content:header_link,\r\n    tt_content:imageorient,tt_content:recursive,tt_content:starttime,tt_content:endtime,tt_content:subheader,\r\n    tt_content:linkToTop,tt_content:l18n_parent,tt_content:header_layout,\r\n    tt_content:pi_flexform;login;sDEF;pages,\r\n\r\n    fe_users:address,fe_users:city,fe_users:company,fe_users:country,fe_users:disable,fe_users:email,fe_users:fax,fe_users:first_name,\r\n    fe_users:felogin_forgotHash,fe_users:image,fe_users:lastlogin,fe_users:last_name,fe_users:lockToDomain,\r\n    fe_users:middle_name,fe_users:name,fe_users:telephone,fe_users:tx_extbase_type,fe_users:felogin_redirectPid,fe_users:starttime,\r\n    fe_users:endtime,fe_users:title,fe_users:TSconfig,fe_users:www,fe_users:zip\r\";s:18:\"explicit_allowdeny\";s:396:\"    tt_content:CType:--div--:ALLOW,tt_content:CType:header:ALLOW,tt_content:CType:textmedia:ALLOW,\r\n    tt_content:CType:table:ALLOW,tt_content:CType:uploads:ALLOW,tt_content:CType:menu:ALLOW,\r\n    tt_content:CType:shortcut:ALLOW,tt_content:CType:list:ALLOW,tt_content:CType:div:ALLOW,tt_content:CType:html:ALLOW,\r\n    tt_content:CType:mailform:ALLOW,tt_content:list_type:indexedsearch_pi2:ALLOW\r\";s:16:\"pagetypes_select\";s:13:\"1,4,3,254,199\";s:13:\"tables_modify\";s:188:\"    pages,sys_category,sys_collection,sys_file,sys_file_collection,sys_file_metadata,sys_file_reference,\r\n    sys_file_storage,backend_layout, pages_language_overlay,sys_domain,tt_content\r\";s:9:\"groupMods\";s:57:\"web_layout,web_list,web_info,file_FilelistList,user_setup\";s:16:\"file_permissions\";s:192:\"    readFolder,writeFolder,addFolder,renameFolder,moveFolder,deleteFolder,recursivedeleteFolder,copyFolder,\r\n    readFile,writeFile,addFile,copyFile,renameFile,replaceFile,moveFile,deleteFile\r\";s:14:\"db_mountpoints\";s:1:\"1\";s:16:\"file_mountpoints\";s:1:\"1\";}}s:8:\"TCEMAIN.\";a:2:{s:12:\"permissions.\";a:4:{s:6:\"userid\";s:1:\"1\";s:7:\"groupid\";s:1:\"1\";s:4:\"user\";s:36:\"show, editcontent, edit, new, delete\";s:5:\"group\";s:36:\"show, editcontent, edit, new, delete\";}s:12:\"linkHandler.\";a:5:{s:5:\"page.\";a:2:{s:7:\"handler\";s:48:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\PageLinkHandler\";s:5:\"label\";s:44:\"LLL:EXT:lang/locallang_browse_links.xlf:page\";}s:5:\"file.\";a:4:{s:7:\"handler\";s:48:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\FileLinkHandler\";s:5:\"label\";s:44:\"LLL:EXT:lang/locallang_browse_links.xlf:file\";s:12:\"displayAfter\";s:4:\"page\";s:9:\"scanAfter\";s:4:\"page\";}s:7:\"folder.\";a:4:{s:7:\"handler\";s:50:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\FolderLinkHandler\";s:5:\"label\";s:46:\"LLL:EXT:lang/locallang_browse_links.xlf:folder\";s:12:\"displayAfter\";s:4:\"file\";s:9:\"scanAfter\";s:4:\"file\";}s:4:\"url.\";a:4:{s:7:\"handler\";s:47:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\UrlLinkHandler\";s:5:\"label\";s:46:\"LLL:EXT:lang/locallang_browse_links.xlf:extUrl\";s:12:\"displayAfter\";s:6:\"folder\";s:9:\"scanAfter\";s:4:\"mail\";}s:5:\"mail.\";a:3:{s:7:\"handler\";s:48:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\MailLinkHandler\";s:5:\"label\";s:45:\"LLL:EXT:lang/locallang_browse_links.xlf:email\";s:12:\"displayAfter\";s:3:\"url\";}}}}s:8:\"sections\";a:0:{}s:5:\"match\";a:0:{}}i:1;s:32:\"b31beac027257eaccec24702754caecf\";}');
/*!40000 ALTER TABLE `cf_cache_hash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_hash_tags`
--

DROP TABLE IF EXISTS `cf_cache_hash_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_hash_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_hash_tags`
--

LOCK TABLES `cf_cache_hash_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_hash_tags` DISABLE KEYS */;
INSERT INTO `cf_cache_hash_tags` VALUES (1,'1062fdc4fae6c932cda8e04241e42873','ident_userTS_TSconfig'),(2,'5ab8edb9df273e87604b97e3b5b3e6f6','ident_PAGES_TSconfig'),(3,'14273b5a17db055e0b1d67ee40919770','ident_TS_TEMPLATE'),(4,'aa2db4f568fd4799b95ecb1d9136fa88','ident_TMPL_CONDITIONS_ALL'),(5,'2c18365758f04f4d2e0170bafa596523','ident_ident_xml2array'),(6,'ee2f1329ecdebc0aa1bdaa21ca466ac8','ident_MENUDATA'),(7,'83e76310cb57635bd93661d4f08e21a6','ident_MENUDATA'),(8,'169aca1993aa78525ae9f1ef7a71dfe2','ident_userTS_TSconfig'),(9,'a2216dd799ebe77d9cf38093419b5752','ident_PAGES_TSconfig');
/*!40000 ALTER TABLE `cf_cache_hash_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_imagesizes`
--

DROP TABLE IF EXISTS `cf_cache_imagesizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_imagesizes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_imagesizes`
--

LOCK TABLES `cf_cache_imagesizes` WRITE;
/*!40000 ALTER TABLE `cf_cache_imagesizes` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_cache_imagesizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_imagesizes_tags`
--

DROP TABLE IF EXISTS `cf_cache_imagesizes_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_imagesizes_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_imagesizes_tags`
--

LOCK TABLES `cf_cache_imagesizes_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_imagesizes_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_cache_imagesizes_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_pages`
--

DROP TABLE IF EXISTS `cf_cache_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_pages`
--

LOCK TABLES `cf_cache_pages` WRITE;
/*!40000 ALTER TABLE `cf_cache_pages` DISABLE KEYS */;
INSERT INTO `cf_cache_pages` VALUES (2,'c4c4b102a189e72a040af8073e16f303',1459579993,'x�\�<\�r\�8��\�Uy�\�&��>(ɶ�\�\�sl\'��b\�\�T	I�H����3~��ںw�_7/v\�\0I��dI�\'�E2\�\�ht7�PH�\��Mv�z\�f\r2*\�W�\�ltm�\�N�A�v�\�6H�U\'\�v}�I��a�\�D�ݮ�\�3\�~ź�y\��P1\�4[@i\�\�㋣\�\�/O��\n����o+��\�\�:=�ڿ\�\�C\�����v\�+�\�\n�\��I8ڷ=jX{\��LC�\r�T�mY���\�H�D\� \�#]�Y�\�\�n\�)������厉�T\�۱\ZVڶ\�\�\��\�J��\� \Z�\�:|k\�j\�\�\�mU*B���˃g[\��\�K\�N@6\�\r��k5��j�\�ܩ�vȰ\�\�mZ\�\"/\0�5\�J*A\"+��\�\0\��ɏ[[\��\�LZ�t ��|��-Գ3|Ѵ*e\�GVDy\�SKqK\�R跀�a\0�XCA�\�#\ZZ�\�¥֑Q�uNB�>��o	\�- \�#�?�\\A�23~OdD�u5!D��G���\�3��\�\�Ѓ��\����<�\�vy4l4V�\�\�+���c�a�X\�:�;(\�V\�\Z\0�1e\�P\\\�n`!�!��9\r�\\D�\��Q�Al(�@\�n��foV�նP�𳧘�\��y�*A\�V���\�*$��㽚ML#�\�\�#\n�Ņm%6�o�e�_\�E\�Fo#.T��yj�\�\�YE��2�sE�ħ����`\'9\"�\�O���\�\�\�\�\�Ol$@�B\��m>\�j\�s�E-�G\�\�\�l9�L(K��Ey\�\�>\������h�\�]i�\�)��\�\�\�@eD̖\0o�\�X�9h4�\�\�s\�\Z+�V�q�\�\�;�N�\�\�\�~]�\�%�<e0I��0s��f>�cJ��P\�0���+�Ƃ�mc\�\n\�P\�P�ߒ�A\�4\�u\�v�\0���\�\�\�\�i�Z�P��}v4.a�i`q\�\Z�\�|>8�A\�j�q(�v�\�z	R��lPq��\�4\�Vc\�q2�$8\Z�����/\���\'\�\�GY��kLŬ6$�;�i�{5�}\�_V�����q\�\�\�Ճ�Y�,)\�\�e(\��B�1�T�X�<\'��䆘�탽��t���\�&#\�nl\�)\�\�^�r��7�`7�\�3>\�W\�3��I\�*a�\�8��O+�\��%a\�A6�ۘ\�6l\�\�+�\�8�)��2-`����hק��\"L&n6�\�Fj�,����#�K�\�ٙvv�>N�1�ח`\�\'bD\�>\Z\�;Sx=&%רIJp\�78q�I��c\�Q\�XY��<Sc��C͔I{\�\�\�\�\n\���c��쏮\�b߂|Ɩ!\�it��o_垐�N�@\�}v�G�5n�;e�><\�Y�a\��-\�\��\�B\�����Hx�\�1��%)VQ\�OB\�m=�\���\Z�\0��P{5/\�\�S���\�;ҥH�5�Ț�g\��#5G��K�s�\"s�9s�a��c\��˖�=LF&�d�(�\�OJ�=�ݤ&�-�ԾO\"	I>�Z!v���\�8\�R��`�E\�\�I\�U��\�\�iJTږ!-˴?\�h�ViV\�۰3>\���\�m%\�#�@1,m�\'-pJ8S�;�\�6��6T\���e�\��mD�\�%�eT���^�*!�|\�\�\�/6g)50�5-��\�r`��H؟���\�za�J�\�t؟*\��c\Z+�}�\�f}#H�ǿ�ٓSu\Z	�|c\�Հ\�2��5��G��\���tA�.�@�A��P�\�R|4�\�i�̳�����_��\�\��!%	�֚w\�)�\�\�ό\�Jk\�\�K�k	 Z$2mL�\�b�\�R\�\�\�^h\�)hݧ�U���\r3\�#\�ޛ\n�-��s\�\��ݪY\�NF�\�a�hȨ\�\�P#c|O\�\'�\�\�\�@X�%\�gt�\�b��\�yȡ��\�\�r��&z]\���\�[R\�Bm���j\�?0���\��\�l\�ͺ�xur�\�\�]@G��B|�ط��\� \�l�\�C�\��}?L}�\�	\�*\nk��9W\�e�B+	<�E\'շ\�j%(�\"ҁ�ĥ�I,i�������\�e^{\�\n\�b�\�\��\�#1�\�9a�\�Ͽ�\�FEО\�_��ݼ\�\�KL�{�\�>4\�iSbI��\�n��\�-\��\�\��\'�f}C��6�Q\�\�py����.�\�l���K7�s�N�T\�TjƮ�\\>�\'e\�*N\�q�\�j�\�\�\�=\nOU��	�O��\�c\�\�a\�d�ҿ{�\�\r\�<!\��\�-��S�إc\�{OB{˺\���(wL�\'̢.�\�Sf0\��E��\�<<��\�1\���\�!�\�\��.	a\�\'`g�O̿_UPD*W��}��\�\�\�\�g�n|}E��\��#2�1x\�\��\\n\�G�\'�W��\�\�(\�\�acZ���\�Cr\Z]\�\�gw�\��+s�\�%\���`�\�~E�M���/1\r\�H?\�\�w�\���\�~ڞ\�\�x\�	�w\�:�7\�q&�\�=�Ǻ���ә�}<\�\'\�\�\��|nw\�͟\0\�Jzd�\�Og��]��]}\�PD\�m�	\�M\�v��b=\�\�\�!iʲ\�$�d\�J6=\�A�\\\�D\'�\�(A1����\�ll\nY{H|Yn�\�xCB��,#�*!y:6���^%2H��,�\�$z�G\�\r4�7�\�\����y�\��ӑ#Yjj�`X!�Jx�\�`	���1\Z\�,��\�I�T$�#���p���~��i_P\�\�\�/�\�\�ґ	.\�C�:	�6<4\�3���+\r}S��\0d���x �Fw@L�S5\�\�\�ME�:\�5c�<#\�	��$B&�~��^q\�/\�[�\�O\�D\��ϓ]��<\�ؕ+\�\\Ù\���;�\0n�\�^^���\�^\���p��ۋXI\�QC�L\�x\�\�9�B\�!�w���`\�K6�ӨSA\��\�A\�\�S\rY�p\���W^E�\�@��W-/AZ]�\�\�0�Y�\\\�\�tc@\�L\�;�[/�v\�sR�\��E�\�\�%���T\�O\��\�(\�T\�{��<Y\�\�[�\'\�\�P\�:�?e�\'�п��c�\�\�1\�(2���\�\��@�\��Ks\�o�vm�\'B���I)7CIL�Q6��\�һzs\�%��L��0ۼ\�ls�4AJ�I\��}�5\�;�1�\��\�w�O�2k\\W}��\�e\�u\�>K\r;�\\�\�\� \�t\�\�Ur\��Ǐb!J\�gM\�\�,�qvqx\��p����w�h\�Z��ӵL1\�n\�\�	:k�mv����\�n���ǿ��b�\�uh�d�9�\�,�P\�\�V\�\�\�6j�\�\�U\�\�\�IN\�X� \n��sS\0a7nng\�`r\0%�T\�H\��x�[\Z���\�\�G\�W\�\'�\����{�\\�ڞ�t��>o�!<<�xT��2�#�]?|A\no|Hc����\�?1oDU��XxwQ��E\'���7Xi�L]\Za�y:<t����\�5\�\�߿��\��\�\��x�ХsCh`��\��a�8\��vu=JJ\�B�1\��v�cH:\�ܴ�ƒ�V��P�\0�\�P\�X�cL\�Y$|�$\\�C�\�_c�zK\� @	�\�f�\��\�=޻NRP,\'�� �L�0��\�|�����lЦ�\�\"�Ln;\'eA\'\��/Q\�(-�bd[v9�N�\�\�\0�+zi�u�jm�%�L9K�\�,�V{.-Ȫ\Z\�\�4w֚y�	s�YZ͕�$[��\���ޔIVX2�v��\�x}��\�\�о\�\�C<��Whv[k!�~�\����r�\Z�\��z\Z��N��\�\�b:�4.�m�-�\�J\\�\\#,D+zz=�\�\�\�[�BKC���q��\�K�\�\'k0¢3\0<-\�Aw ��\Z}�\Z	\�\�92�la�[f�o��͌���ݣE\�h�bG\'\�\"��\�1�or�04\"0+W C�\r��Tf\nѼʾ�~!�_j\��L\�2�t�<\�V\�i��\�\Z\�I�\Z��\��t�*1��n\�@�\���G�\�F\�\Z�KV�Q�\�h��\"�F>s�2��G\�U\�k4\�H�\���\�\�\�lgfݯUL\�^Hp\�RC�g*]-+Iur���$�\"�\Z�\�}@#\� ���ձ\0\�]s��\�D�:�D��jh���\"s\��M\����ٝkY<�T�p\�ga\�^����?\r\�\�S\'ܻ2\�\�\�NXɣ�-�x�\�\�ؚ\�՟��\�\�^\�\�\�\�\�,��\�2>\�k򚏹��Aa1G7F\\̖�aq�\�0I��`\'�O#?\�k���\"y�4�\�{\�]���\�6(\�W\\j�Ot<MD�G�n��u\")r?(�v�\�\�Z���\�J\n�\n��=z5�PJ�\�:y���\��|���\�\�\�\�%\�)�޽\�\�Sk\�5���\�48\�ٴn�\0�\�`�^wt�Æ̽G�qaW-$�\���N؍1@\�z0%fǐ;E�\���9�(��\�gDn\�\"0�ȝk\�!�\�\�\�Ut֚�\�\�\��$T��\�Us�8��ݴ�_\�qA/�s��kO�\�>Ʃ�\ns��֢��\�U���5��e�� �!�\�b�~$\"MP\nkt\��2\�|�\�\�\0IT,���\�T\',JN\�\��1���Q\��\�\��|�4|�;R�K8�b\��x\�i����3}\"�W��ud��\��r�\�/���\'N\�T�>/B\�\���,�8\�8U8��l�\��\�\�h��2�\��\�gH\�cT4\�\�=\��*r\�c��oG\'�P\�vr�v��nV=矶\�yNn6tax��*�����Z��*\�u\�\�\�4OK�M�\�^]\�\�e��dPp1EQ���\�\�/�\�\"\�s�\��\�\���{��I\�\�q�CXgߧ7\�\��TiK:JkR\�\�\�3aS��k\�#�w�m-\�\�XA4M\�G\�ݹ\�\��\0���<�\�\����(wI��N\�\�\�\Z�g_HI�ԒL g�k��t:���?\�#\�]=�Z\�@��\�|\�\�{D���\�\�\�N//P\�Ao����\�\�G&');
/*!40000 ALTER TABLE `cf_cache_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_pages_tags`
--

DROP TABLE IF EXISTS `cf_cache_pages_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_pages_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_pages_tags`
--

LOCK TABLES `cf_cache_pages_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_pages_tags` DISABLE KEYS */;
INSERT INTO `cf_cache_pages_tags` VALUES (2,'c4c4b102a189e72a040af8073e16f303','pageId_1'),(3,'c4c4b102a189e72a040af8073e16f303','pageId_1');
/*!40000 ALTER TABLE `cf_cache_pages_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_pagesection`
--

DROP TABLE IF EXISTS `cf_cache_pagesection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_pagesection` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_pagesection`
--

LOCK TABLES `cf_cache_pagesection` WRITE;
/*!40000 ALTER TABLE `cf_cache_pagesection` DISABLE KEYS */;
INSERT INTO `cf_cache_pagesection` VALUES (1,'1_222419149',1462170793,'x�Ś_o\�8��J��\�)\�{�p��{{\�\�\�\�\���$\�a#��H�	�\�7�%GTCűc塍E�\��Ù!):\��$�wN\���=�\'\�\�\�q\�g	ƌDiJS�Q\�HF2�#d$</\���\�?��\�\�낭7%U\�g!�T5\�|��\�%W�_/E�s\�E��cu\�kQ�Y�v�g?��\"�\���@\�1KQ�\�+RFH������J��\�ij:N^�\�ݲRlt��4�A��0\nB���y~\�\�������U)RZ�Ik�˗�\����\�I�b�c�X��\"NҰH��~��\�-\�d~D���q\��\�\"\�%Y�%�(pĨ[\�\��|/	\�<�r�a?(P�z�\�s�\��I`d\���Ͽ.\�>�}	�B\�	C�M|B\�1�<#\�ۉ		c)�ey��i�QX\�y��\�g�Q�lb?�߄Z\�4�ٕ=\�d�\�\"��4Hp\�\�EFh�$6\�\�	l\�\�og \�A\�\��y\r�\�Ӌß�\�\��@I�j6<\�b|е\�v~\�\�J����*|\�ꥰk�^�\���7R�\����\�d��o��w��Ȯ۷\'�ox\��^\�\�\�Q[5\�,\�W\�\�Kz/\Z\�\�\n�y���_2�V��-��n�KA�[�j\�lz�� �\�)��WM��+&�rq�\�7��K���\�DU�U_TVr[�\�UV69\�\�@\�˥\�\�e-�J-\�\�ދ�(�w،�+^-\�\"�F�@�)#l�U粂q.K\�hzL\�\�\�ݲf�l\�r��\�Z���C}�ت\�\�\�Ь\��bN\�\�j\�T20�����\Z\���]hi�\�`\\\"\�k��~\�\�QYXOp5h��`,�߯��^>qЭ��\�M-��\�x�K�\�\�\�R�}1\�������B��ݨ\')к�\�.�Y45՞a��߈��\�5\�:\r��\����n�~\�\0��N�d�l��m��m�\\�\�x�m��s[?�\�խ	LkZ�~\��\�Z9d\�o\n�o\�\�w�\�\���C�����Ym�h����E\�\�F��m\�\�{Y	�\�`lJ�]Q\�(4j\��\�Q��m\���ӣ���\���j��\�؍b�o\��f�&-y��V\�r>H`\�\\f�\�ُV����3&A\n��W}#�v\�*+%/�\�\��?c\�{���-,\�\�cg\ZrfL�\�}\��%�c��\0�fE�\n×�\�`5\�};W\�D@��\�\�\�\0.H�-8\�O�H^\�\�\�!��\�}T\�*����z6\�gK�A���r\�\�>\�Z\�!�\�8�xȀD\�Ly/2�C��d�ٸ\'(�M�t����C�M�JZ�\Zxv&�y��	�`\�-��ی\�\r���XK)2NK�X3�t\��\�v�(�2�NאY��W#S=���\�d&e�q�}J��\�\��5Hl\r�\�	�jN2\�Qx��9*i�\\AEG\�\"G�]i�Y\�Ӌ\r�%�\�Tُ\�\�(\�#V�+Q�\�\�fn\�\�\�\�)U~\�(�\'%���\�@i\Zɋ�\�Ŋ����	w�<���m�\�]:tjѵ��\�;�\�\�\���\��ξ�\���ƃ\�2͆R��\�̨T\�ft|w�S\��\�\�0o7\n�d�O\�~n��{�\r�/���P �s\�\�a�;\�o\�GsyͲ�TܹalW\�\�\�_cΦ�D�w\�>�D[\�Ͷ\�n$��\�Y�ˑX\��\�$l_��5�R\�܍cy\�䔳�	��9�ɼ\�?\��LGb\�\�$v�\�\��i�򫾉H\��pִ/N�iF\�\����D_�=��d2\�|�)+� ě\�\���Q\�р��-&`\�#�C\�tj�B��.j�\�%x2c��\�\\�A�频!y\�\\\�tzacQ�L��^A����Dӑ4随�L��.�.�u\�$\�\�	4L\n[\�\'M8�\�\�4%��8$\�`N\�s!�T��\�_7\��\�\�(`?�?i\�s�\�<�t\�g�$��\�.\�4��6\�=h{\�$�\�{�\�*�\��\�\�ٞ\�\�\�S�|\�i\'\��E	�\�s��`�#Z�\�Z�ݧ{�\�7\\λ�pp~�;y�V����{FG\�~p>�\�o\�\"�<�i#;l�In�:ƒ�{.�\�>?�\'5L�Y�\�)lG�ޘnԥG���x?�\r\"\�$���%�Aؙ\�k�>��\�H�nܷ	Q�ƈ�CTs�\�F��Is ����\�3�o��N��\��ND�3\�\�(�\�<ܲ\�½b\�0\�\�f\�R�\�z\��\�ޱ�{2�\�r\�E�}z\�nr�}\�\���\�A��\�\�羼E\�����');
/*!40000 ALTER TABLE `cf_cache_pagesection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_pagesection_tags`
--

DROP TABLE IF EXISTS `cf_cache_pagesection_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_pagesection_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_pagesection_tags`
--

LOCK TABLES `cf_cache_pagesection_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_pagesection_tags` DISABLE KEYS */;
INSERT INTO `cf_cache_pagesection_tags` VALUES (1,'1_222419149','pageId_1'),(2,'1_222419149','mpvarHash_222419149');
/*!40000 ALTER TABLE `cf_cache_pagesection_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_rootline`
--

DROP TABLE IF EXISTS `cf_cache_rootline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_rootline` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_rootline`
--

LOCK TABLES `cf_cache_rootline` WRITE;
/*!40000 ALTER TABLE `cf_cache_rootline` DISABLE KEYS */;
INSERT INTO `cf_cache_rootline` VALUES (1,'1__0_0_0',1462170793,'a:1:{i:0;a:29:{s:3:\"pid\";s:1:\"0\";s:3:\"uid\";s:1:\"1\";s:9:\"t3ver_oid\";s:1:\"0\";s:10:\"t3ver_wsid\";s:1:\"0\";s:11:\"t3ver_state\";s:1:\"0\";s:5:\"title\";s:10:\"juli-an.ch\";s:5:\"alias\";s:0:\"\";s:9:\"nav_title\";s:0:\"\";s:5:\"media\";s:0:\"\";s:6:\"layout\";s:1:\"0\";s:6:\"hidden\";s:1:\"0\";s:9:\"starttime\";s:1:\"0\";s:7:\"endtime\";s:1:\"0\";s:8:\"fe_group\";s:0:\"\";s:16:\"extendToSubpages\";s:1:\"0\";s:7:\"doktype\";s:1:\"1\";s:8:\"TSconfig\";s:0:\"\";s:17:\"tsconfig_includes\";s:0:\"\";s:11:\"is_siteroot\";s:1:\"0\";s:9:\"mount_pid\";s:1:\"0\";s:12:\"mount_pid_ol\";s:1:\"0\";s:13:\"fe_login_mode\";s:1:\"0\";s:25:\"backend_layout_next_level\";s:0:\"\";s:22:\"tx_realurl_pathsegment\";s:0:\"\";s:18:\"tx_realurl_exclude\";s:1:\"0\";s:23:\"tx_realurl_pathoverride\";s:1:\"0\";s:10:\"categories\";s:0:\"\";s:30:\"tx_templatebootstrap_moodimage\";s:0:\"\";s:36:\"tx_templatebootstrap_backgroundimage\";s:0:\"\";}}');
/*!40000 ALTER TABLE `cf_cache_rootline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_rootline_tags`
--

DROP TABLE IF EXISTS `cf_cache_rootline_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_rootline_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_rootline_tags`
--

LOCK TABLES `cf_cache_rootline_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_rootline_tags` DISABLE KEYS */;
INSERT INTO `cf_cache_rootline_tags` VALUES (1,'1__0_0_0','pageId_1');
/*!40000 ALTER TABLE `cf_cache_rootline_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_datamapfactory_datamap`
--

DROP TABLE IF EXISTS `cf_extbase_datamapfactory_datamap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_datamapfactory_datamap` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_datamapfactory_datamap`
--

LOCK TABLES `cf_extbase_datamapfactory_datamap` WRITE;
/*!40000 ALTER TABLE `cf_extbase_datamapfactory_datamap` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_datamapfactory_datamap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_datamapfactory_datamap_tags`
--

DROP TABLE IF EXISTS `cf_extbase_datamapfactory_datamap_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_datamapfactory_datamap_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_datamapfactory_datamap_tags`
--

LOCK TABLES `cf_extbase_datamapfactory_datamap_tags` WRITE;
/*!40000 ALTER TABLE `cf_extbase_datamapfactory_datamap_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_datamapfactory_datamap_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_object`
--

DROP TABLE IF EXISTS `cf_extbase_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_object` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_object`
--

LOCK TABLES `cf_extbase_object` WRITE;
/*!40000 ALTER TABLE `cf_extbase_object` DISABLE KEYS */;
INSERT INTO `cf_extbase_object` VALUES (1,'3bcc835c5d8d7866a0ce2dc41464b9a8',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:52:\"TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:24:\"injectEnvironmentService\";s:44:\"TYPO3\\CMS\\Extbase\\Service\\EnvironmentService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:1;}'),(2,'48ee39591354d5b031b745485de55720',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:38:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManager\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(3,'dbac534c00a92e4f7b105be73fba4655',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:44:\"TYPO3\\CMS\\Extbase\\Service\\EnvironmentService\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(4,'a6993d6ce1dd11daab5560826404f359',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:60:\"TYPO3\\CMS\\Extbase\\Configuration\\FrontendConfigurationManager\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:4:{s:21:\"injectFlexFormService\";s:41:\"TYPO3\\CMS\\Extbase\\Service\\FlexFormService\";s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectTypoScriptService\";s:43:\"TYPO3\\CMS\\Extbase\\Service\\TypoScriptService\";s:24:\"injectEnvironmentService\";s:44:\"TYPO3\\CMS\\Extbase\\Service\\EnvironmentService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(5,'7e27373c7241e8ff24a8f03a1b8705d9',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:41:\"TYPO3\\CMS\\Extbase\\Service\\FlexFormService\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(6,'d9a9ae73d3607000daec9828154ef861',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:43:\"TYPO3\\CMS\\Extbase\\Service\\TypoScriptService\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(7,'3316fa0a4297a6ddcca7021a1565a7e9',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:42:\"TYPO3\\CMS\\Fluid\\Core\\Parser\\TemplateParser\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:1:{s:13:\"objectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(8,'e8d805b35474fd740277e82b2633dfdc',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:47:\"TYPO3\\CMS\\Fluid\\Core\\Rendering\\RenderingContext\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:3:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:33:\"injectViewHelperVariableContainer\";s:59:\"TYPO3\\CMS\\Fluid\\Core\\ViewHelper\\ViewHelperVariableContainer\";s:31:\"injectTemplateVariableContainer\";s:57:\"TYPO3\\CMS\\Fluid\\Core\\ViewHelper\\TemplateVariableContainer\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(9,'a640fdb9512138cae8d3d392619d9034',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:59:\"TYPO3\\CMS\\Fluid\\Core\\ViewHelper\\ViewHelperVariableContainer\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(10,'6e9736d7b31f65e8bab9afe208da72f5',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:57:\"TYPO3\\CMS\\Fluid\\Core\\ViewHelper\\TemplateVariableContainer\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:1:{i:0;a:2:{s:4:\"name\";s:13:\"variableArray\";s:12:\"defaultValue\";a:0:{}}}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(11,'ffab3d90bc879cb5f9bc7dad6eeca0fe',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:33:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\Request\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:3:{s:17:\"injectHashService\";s:51:\"TYPO3\\CMS\\Extbase\\Security\\Cryptography\\HashService\";s:26:\"injectConfigurationManager\";s:61:\"TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface\";s:24:\"injectEnvironmentService\";s:44:\"TYPO3\\CMS\\Extbase\\Service\\EnvironmentService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(12,'b50521e5e180c67942dcfb9564f33b28',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:51:\"TYPO3\\CMS\\Extbase\\Security\\Cryptography\\HashService\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(13,'4162aee1fc2f8a1059d3c19ec4daa1c7',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:44:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\Routing\\UriBuilder\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:3:{s:26:\"injectConfigurationManager\";s:61:\"TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface\";s:22:\"injectExtensionService\";s:42:\"TYPO3\\CMS\\Extbase\\Service\\ExtensionService\";s:24:\"injectEnvironmentService\";s:44:\"TYPO3\\CMS\\Extbase\\Service\\EnvironmentService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:1;}'),(14,'161ec4a31f0f6bbc932fcc1ae6d48887',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:42:\"TYPO3\\CMS\\Extbase\\Service\\ExtensionService\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:26:\"injectConfigurationManager\";s:61:\"TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(15,'6c06740645171b100a0605c1774e691c',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:50:\"TYPO3\\CMS\\Extbase\\Mvc\\Controller\\ControllerContext\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:25:\"injectFlashMessageService\";s:44:\"TYPO3\\CMS\\Core\\Messaging\\FlashMessageService\";s:22:\"injectExtensionService\";s:42:\"TYPO3\\CMS\\Extbase\\Service\\ExtensionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(16,'fcd3672a290f64f6875bb8d84ef8362b',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:44:\"TYPO3\\CMS\\Core\\Messaging\\FlashMessageService\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(17,'5ed31dbb2262e355409980a4c85adca5',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:46:\"TYPO3\\CMS\\Fluid\\Core\\Compiler\\TemplateCompiler\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(18,'82775a44b14c5ee1f99fb06534415b03',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:41:\"TYPO3\\CMS\\Fluid\\Core\\Parser\\Configuration\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(19,'f542e57a147c74616c2c14ae45a7da4e',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:46:\"TYPO3\\CMS\\Fluid\\Core\\Parser\\Interceptor\\Escape\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:1:{s:13:\"objectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(20,'434532454ee301095ce48df889c5685c',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:40:\"TYPO3\\CMS\\Fluid\\Core\\Parser\\ParsingState\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:1:{s:17:\"variableContainer\";s:57:\"TYPO3\\CMS\\Fluid\\Core\\ViewHelper\\TemplateVariableContainer\";}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(21,'78b1b7823d2022e2377301c0f55ac4e6',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:47:\"TYPO3\\CMS\\Fluid\\Core\\Parser\\SyntaxTree\\RootNode\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(22,'630b0da604b2c9cabe61e71b441dd0e2',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:47:\"TYPO3\\CMS\\Fluid\\Core\\Parser\\SyntaxTree\\TextNode\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:1:{i:0;a:1:{s:4:\"name\";s:4:\"text\";}}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(23,'0feaa6ecc8d3d05a79d13ef36e099662',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:44:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\LayoutViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(24,'33557becb5fd776e48ac1a4e8877c048',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:26:\"injectConfigurationManager\";s:61:\"TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(25,'4a3a325facbf7a9fc8d188cb22d15301',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:53:\"TYPO3\\CMS\\Fluid\\Core\\Parser\\SyntaxTree\\ViewHelperNode\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:2:{i:0;a:2:{s:4:\"name\";s:10:\"viewHelper\";s:10:\"dependency\";s:50:\"TYPO3\\CMS\\Fluid\\Core\\ViewHelper\\AbstractViewHelper\";}i:1;a:1:{s:4:\"name\";s:9:\"arguments\";}}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(26,'fdd3ab12237a39824038609b9791fa1e',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:45:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\SectionViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(27,'ce1dc7d680fd675ad944884e40914d8c',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:45:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\CObjectViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:3:{s:26:\"injectConfigurationManager\";s:61:\"TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface\";s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(28,'1fb2371d594668a70e77b8c6ad79fab3',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:57:\"TYPO3\\CMS\\Fluid\\Core\\Parser\\SyntaxTree\\ObjectAccessorNode\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:1:{i:0;a:1:{s:4:\"name\";s:10:\"objectPath\";}}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(29,'17466a85af9a994978ee0405320a798b',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:44:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\RenderViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(30,'c7cb88ec1a57cdfb6bb3a8518f770ffe',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:61:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\Format\\HtmlspecialcharsViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(31,'7a6761aed6bbdcf2b0f612bc11c1217c',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:40:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\IfViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(32,'232c19c4ec41c1994aa808ba8c0e088f',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:42:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\ThenViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(33,'d1627dc87083dc420614aa04dcfcf5cc',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:42:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\ElseViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(34,'f4161c7b5ff989cc5937476177f76d76',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:39:\"TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:1;}'),(35,'91beec59b6e878a0a8adfe6fc280848a',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:64:\"TYPO3\\CMS\\Core\\Resource\\OnlineMedia\\Processing\\PreviewProcessing\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(36,'70a5c4d400bfe2489f3b434b43312c35',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:51:\"TYPO3\\CMS\\Frontend\\Aspect\\FileMetadataOverlayAspect\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(37,'93451c7fc73e115c366d9aafadc89b08',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:48:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\Link\\EmailViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:1:{s:3:\"tag\";s:42:\"TYPO3\\CMS\\Fluid\\Core\\ViewHelper\\TagBuilder\";}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(38,'ff9b48c897971beddd71034528f6f90e',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:42:\"TYPO3\\CMS\\Fluid\\Core\\ViewHelper\\TagBuilder\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:2:{i:0;a:2:{s:4:\"name\";s:7:\"tagName\";s:12:\"defaultValue\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:10:\"tagContent\";s:12:\"defaultValue\";s:0:\"\";}}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(39,'698f3592af2e452d429bd1e9cf4e7f28',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:59:\"TYPO3\\CMS\\Extbase\\Configuration\\BackendConfigurationManager\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:4:{s:20:\"injectQueryGenerator\";s:38:\"TYPO3\\CMS\\Core\\Database\\QueryGenerator\";s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectTypoScriptService\";s:43:\"TYPO3\\CMS\\Extbase\\Service\\TypoScriptService\";s:24:\"injectEnvironmentService\";s:44:\"TYPO3\\CMS\\Extbase\\Service\\EnvironmentService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:1;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(40,'a83f638ccc959a3a47ea70bd7ad866a8',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:38:\"TYPO3\\CMS\\Core\\Database\\QueryGenerator\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(41,'5b9bc39514aff3e59f378bd70fcc2a90',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:48:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\Format\\RawViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(42,'3a4cfbd9a9d33ba0ada3c8848d08e7f2',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:48:\"TYPO3\\CMS\\Fluid\\Core\\Parser\\SyntaxTree\\ArrayNode\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:1:{i:0;a:1:{s:4:\"name\";s:13:\"internalArray\";}}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:0:{}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(43,'0a2c329eb2c8346be16fcb86e5609437',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:51:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\FlashMessagesViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:1:{s:3:\"tag\";s:42:\"TYPO3\\CMS\\Fluid\\Core\\ViewHelper\\TagBuilder\";}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(44,'c2e9ae889aae6db5bc680e826e21142d',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:41:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\ForViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(45,'3460f0111910bdbec763a839d29eaf53',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:47:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\TranslateViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}'),(46,'5394aed9549d0cd47a0b4aa0679efbf8',2145909600,'O:44:\"TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\":6:{s:55:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0className\";s:43:\"TYPO3\\CMS\\Fluid\\ViewHelpers\\CountViewHelper\";s:66:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0constructorArguments\";a:0:{}s:59:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectMethods\";a:2:{s:19:\"injectObjectManager\";s:47:\"TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface\";s:23:\"injectReflectionService\";s:46:\"TYPO3\\CMS\\Extbase\\Reflection\\ReflectionService\";}s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0injectProperties\";a:0:{}s:57:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isSingleton\";b:0;s:62:\"\0TYPO3\\CMS\\Extbase\\Object\\Container\\ClassInfo\0isInitializeable\";b:0;}');
/*!40000 ALTER TABLE `cf_extbase_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_object_tags`
--

DROP TABLE IF EXISTS `cf_extbase_object_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_object_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_object_tags`
--

LOCK TABLES `cf_extbase_object_tags` WRITE;
/*!40000 ALTER TABLE `cf_extbase_object_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_object_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_reflection`
--

DROP TABLE IF EXISTS `cf_extbase_reflection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_reflection` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_reflection`
--

LOCK TABLES `cf_extbase_reflection` WRITE;
/*!40000 ALTER TABLE `cf_extbase_reflection` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_reflection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_reflection_tags`
--

DROP TABLE IF EXISTS `cf_extbase_reflection_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_reflection_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_reflection_tags`
--

LOCK TABLES `cf_extbase_reflection_tags` WRITE;
/*!40000 ALTER TABLE `cf_extbase_reflection_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_reflection_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_typo3dbbackend_queries`
--

DROP TABLE IF EXISTS `cf_extbase_typo3dbbackend_queries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_typo3dbbackend_queries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_typo3dbbackend_queries`
--

LOCK TABLES `cf_extbase_typo3dbbackend_queries` WRITE;
/*!40000 ALTER TABLE `cf_extbase_typo3dbbackend_queries` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_typo3dbbackend_queries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_typo3dbbackend_queries_tags`
--

DROP TABLE IF EXISTS `cf_extbase_typo3dbbackend_queries_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_typo3dbbackend_queries_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_typo3dbbackend_queries_tags`
--

LOCK TABLES `cf_extbase_typo3dbbackend_queries_tags` WRITE;
/*!40000 ALTER TABLE `cf_extbase_typo3dbbackend_queries_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_typo3dbbackend_queries_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_typo3dbbackend_tablecolumns`
--

DROP TABLE IF EXISTS `cf_extbase_typo3dbbackend_tablecolumns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_typo3dbbackend_tablecolumns` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_typo3dbbackend_tablecolumns`
--

LOCK TABLES `cf_extbase_typo3dbbackend_tablecolumns` WRITE;
/*!40000 ALTER TABLE `cf_extbase_typo3dbbackend_tablecolumns` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_typo3dbbackend_tablecolumns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_typo3dbbackend_tablecolumns_tags`
--

DROP TABLE IF EXISTS `cf_extbase_typo3dbbackend_tablecolumns_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_typo3dbbackend_tablecolumns_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_typo3dbbackend_tablecolumns_tags`
--

LOCK TABLES `cf_extbase_typo3dbbackend_tablecolumns_tags` WRITE;
/*!40000 ALTER TABLE `cf_extbase_typo3dbbackend_tablecolumns_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_typo3dbbackend_tablecolumns_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_groups`
--

DROP TABLE IF EXISTS `fe_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fe_groups` (
  `tx_extbase_type` varchar(255) NOT NULL DEFAULT '0',
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL DEFAULT '',
  `hidden` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `lockToDomain` varchar(50) NOT NULL DEFAULT '',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `description` text,
  `subgroup` tinytext,
  `TSconfig` text,
  `felogin_redirectPid` tinytext,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_groups`
--

LOCK TABLES `fe_groups` WRITE;
/*!40000 ALTER TABLE `fe_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_session_data`
--

DROP TABLE IF EXISTS `fe_session_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fe_session_data` (
  `hash` varchar(32) NOT NULL DEFAULT '',
  `content` mediumblob,
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`hash`),
  KEY `tstamp` (`tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_session_data`
--

LOCK TABLES `fe_session_data` WRITE;
/*!40000 ALTER TABLE `fe_session_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_session_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_sessions`
--

DROP TABLE IF EXISTS `fe_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fe_sessions` (
  `ses_id` varchar(32) NOT NULL DEFAULT '',
  `ses_name` varchar(32) NOT NULL DEFAULT '',
  `ses_iplock` varchar(39) NOT NULL DEFAULT '',
  `ses_hashlock` int(11) NOT NULL DEFAULT '0',
  `ses_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `ses_tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `ses_data` blob,
  `ses_permanent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ses_id`,`ses_name`),
  KEY `ses_tstamp` (`ses_tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_sessions`
--

LOCK TABLES `fe_sessions` WRITE;
/*!40000 ALTER TABLE `fe_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_users`
--

DROP TABLE IF EXISTS `fe_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fe_users` (
  `tx_extbase_type` varchar(255) NOT NULL DEFAULT '0',
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usergroup` tinytext,
  `disable` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `middle_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `telephone` varchar(20) NOT NULL DEFAULT '',
  `fax` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `lockToDomain` varchar(50) NOT NULL DEFAULT '',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `uc` blob,
  `title` varchar(40) NOT NULL DEFAULT '',
  `zip` varchar(10) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(40) NOT NULL DEFAULT '',
  `www` varchar(80) NOT NULL DEFAULT '',
  `company` varchar(80) NOT NULL DEFAULT '',
  `image` tinytext,
  `TSconfig` text,
  `fe_cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastlogin` int(10) unsigned NOT NULL DEFAULT '0',
  `is_online` int(10) unsigned NOT NULL DEFAULT '0',
  `felogin_redirectPid` tinytext,
  `felogin_forgotHash` varchar(80) DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`username`),
  KEY `username` (`username`),
  KEY `is_online` (`is_online`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_users`
--

LOCK TABLES `fe_users` WRITE;
/*!40000 ALTER TABLE `fe_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `perms_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `perms_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `perms_user` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `perms_group` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `perms_everybody` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `editlock` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `doktype` int(11) unsigned NOT NULL DEFAULT '0',
  `TSconfig` text,
  `is_siteroot` tinyint(4) NOT NULL DEFAULT '0',
  `php_tree_stop` tinyint(4) NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL DEFAULT '',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `urltype` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `shortcut` int(10) unsigned NOT NULL DEFAULT '0',
  `shortcut_mode` int(10) unsigned NOT NULL DEFAULT '0',
  `no_cache` int(10) unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(100) NOT NULL DEFAULT '0',
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `layout` int(11) unsigned NOT NULL DEFAULT '0',
  `url_scheme` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `target` varchar(80) NOT NULL DEFAULT '',
  `media` int(11) unsigned NOT NULL DEFAULT '0',
  `lastUpdated` int(10) unsigned NOT NULL DEFAULT '0',
  `keywords` text,
  `cache_timeout` int(10) unsigned NOT NULL DEFAULT '0',
  `cache_tags` varchar(255) NOT NULL DEFAULT '',
  `newUntil` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text,
  `no_search` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `SYS_LASTCHANGED` int(10) unsigned NOT NULL DEFAULT '0',
  `abstract` text,
  `module` varchar(10) NOT NULL DEFAULT '',
  `extendToSubpages` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) NOT NULL DEFAULT '',
  `author_email` varchar(80) NOT NULL DEFAULT '',
  `nav_title` varchar(255) NOT NULL DEFAULT '',
  `nav_hide` tinyint(4) NOT NULL DEFAULT '0',
  `content_from_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `mount_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `mount_pid_ol` tinyint(4) NOT NULL DEFAULT '0',
  `alias` varchar(32) NOT NULL DEFAULT '',
  `l18n_cfg` tinyint(4) NOT NULL DEFAULT '0',
  `fe_login_mode` tinyint(4) NOT NULL DEFAULT '0',
  `backend_layout` varchar(64) NOT NULL DEFAULT '',
  `backend_layout_next_level` varchar(64) NOT NULL DEFAULT '',
  `tsconfig_includes` text,
  `categories` int(11) NOT NULL DEFAULT '0',
  `tx_realurl_pathsegment` varchar(255) NOT NULL DEFAULT '',
  `tx_realurl_pathoverride` int(1) NOT NULL DEFAULT '0',
  `tx_realurl_exclude` int(1) NOT NULL DEFAULT '0',
  `tx_realurl_nocache` int(1) NOT NULL DEFAULT '0',
  `tx_templatebootstrap_moodimage` varchar(255) NOT NULL DEFAULT '',
  `tx_templatebootstrap_backgroundimage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `parent` (`pid`,`deleted`,`sorting`),
  KEY `alias` (`alias`),
  KEY `determineSiteRoot` (`is_siteroot`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,0,0,0,'',0,0,0,0,0,0,1459576146,256,0,1,1,31,31,0,0,1459576031,1,0,'juli-an.ch',1,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,NULL,0,'',0,'',0,1459576146,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0,'',0,0,0,'','');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_language_overlay`
--

DROP TABLE IF EXISTS `pages_language_overlay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_language_overlay` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `doktype` int(11) unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `nav_title` varchar(255) NOT NULL DEFAULT '',
  `media` int(11) unsigned NOT NULL DEFAULT '0',
  `keywords` text,
  `description` text,
  `abstract` text,
  `author` varchar(255) NOT NULL DEFAULT '',
  `author_email` varchar(80) NOT NULL DEFAULT '',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  `l18n_diffsource` mediumblob,
  `url` varchar(255) NOT NULL DEFAULT '',
  `urltype` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `shortcut` int(10) unsigned NOT NULL DEFAULT '0',
  `shortcut_mode` int(10) unsigned NOT NULL DEFAULT '0',
  `tx_realurl_pathsegment` varchar(255) NOT NULL DEFAULT '',
  `tx_templatebootstrap_moodimage` varchar(255) NOT NULL DEFAULT '',
  `tx_templatebootstrap_backgroundimage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `parent` (`pid`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_language_overlay`
--

LOCK TABLES `pages_language_overlay` WRITE;
/*!40000 ALTER TABLE `pages_language_overlay` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_language_overlay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_be_shortcuts`
--

DROP TABLE IF EXISTS `sys_be_shortcuts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_be_shortcuts` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `module_name` varchar(255) NOT NULL DEFAULT '',
  `url` text,
  `description` varchar(255) NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `sc_group` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_be_shortcuts`
--

LOCK TABLES `sys_be_shortcuts` WRITE;
/*!40000 ALTER TABLE `sys_be_shortcuts` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_be_shortcuts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_category`
--

DROP TABLE IF EXISTS `sys_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_category` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(30) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(11) NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob NOT NULL,
  `title` tinytext NOT NULL,
  `description` text,
  `parent` int(11) NOT NULL DEFAULT '0',
  `items` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `category_parent` (`parent`),
  KEY `category_list` (`pid`,`deleted`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_category`
--

LOCK TABLES `sys_category` WRITE;
/*!40000 ALTER TABLE `sys_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_category_record_mm`
--

DROP TABLE IF EXISTS `sys_category_record_mm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_category_record_mm` (
  `uid_local` int(11) NOT NULL DEFAULT '0',
  `uid_foreign` int(11) NOT NULL DEFAULT '0',
  `tablenames` varchar(255) NOT NULL DEFAULT '',
  `fieldname` varchar(255) NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `sorting_foreign` int(11) NOT NULL DEFAULT '0',
  KEY `uid_local_foreign` (`uid_local`,`uid_foreign`),
  KEY `uid_foreign_tablefield` (`uid_foreign`,`tablenames`(40),`fieldname`(3),`sorting_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_category_record_mm`
--

LOCK TABLES `sys_category_record_mm` WRITE;
/*!40000 ALTER TABLE `sys_category_record_mm` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_category_record_mm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_collection`
--

DROP TABLE IF EXISTS `sys_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_collection` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(30) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(11) NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumtext,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `starttime` int(11) NOT NULL DEFAULT '0',
  `endtime` int(11) NOT NULL DEFAULT '0',
  `fe_group` int(11) NOT NULL DEFAULT '0',
  `title` tinytext,
  `description` text,
  `type` varchar(32) NOT NULL DEFAULT 'static',
  `table_name` tinytext,
  `items` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_collection`
--

LOCK TABLES `sys_collection` WRITE;
/*!40000 ALTER TABLE `sys_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_collection_entries`
--

DROP TABLE IF EXISTS `sys_collection_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_collection_entries` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `uid_local` int(11) NOT NULL DEFAULT '0',
  `uid_foreign` int(11) NOT NULL DEFAULT '0',
  `tablenames` varchar(64) NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `uid_local` (`uid_local`),
  KEY `uid_foreign` (`uid_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_collection_entries`
--

LOCK TABLES `sys_collection_entries` WRITE;
/*!40000 ALTER TABLE `sys_collection_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_collection_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_domain`
--

DROP TABLE IF EXISTS `sys_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_domain` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `domainName` varchar(80) NOT NULL DEFAULT '',
  `redirectTo` varchar(255) NOT NULL DEFAULT '',
  `redirectHttpStatusCode` int(4) unsigned NOT NULL DEFAULT '301',
  `sorting` int(10) unsigned NOT NULL DEFAULT '0',
  `prepend_params` int(10) NOT NULL DEFAULT '0',
  `forced` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `getSysDomain` (`redirectTo`,`hidden`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_domain`
--

LOCK TABLES `sys_domain` WRITE;
/*!40000 ALTER TABLE `sys_domain` DISABLE KEYS */;
INSERT INTO `sys_domain` VALUES (1,1,1459576506,1459576255,1,0,'www.juli-an.ch','',301,256,0,0);
/*!40000 ALTER TABLE `sys_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file`
--

DROP TABLE IF EXISTS `sys_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `last_indexed` int(11) NOT NULL DEFAULT '0',
  `missing` tinyint(4) NOT NULL DEFAULT '0',
  `storage` int(11) NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL DEFAULT '',
  `metadata` int(11) NOT NULL DEFAULT '0',
  `identifier` text,
  `identifier_hash` char(40) NOT NULL DEFAULT '',
  `folder_hash` char(40) NOT NULL DEFAULT '',
  `extension` varchar(255) NOT NULL DEFAULT '',
  `mime_type` varchar(255) NOT NULL DEFAULT '',
  `name` tinytext,
  `sha1` char(40) NOT NULL DEFAULT '',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `creation_date` int(11) NOT NULL DEFAULT '0',
  `modification_date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `sel01` (`storage`,`identifier_hash`),
  KEY `folder` (`storage`,`folder_hash`),
  KEY `tstamp` (`tstamp`),
  KEY `lastindex` (`last_indexed`),
  KEY `sha1` (`sha1`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file`
--

LOCK TABLES `sys_file` WRITE;
/*!40000 ALTER TABLE `sys_file` DISABLE KEYS */;
INSERT INTO `sys_file` VALUES (1,0,1459576444,0,0,0,'5',0,'/typo3conf/ext/templatebootstrap/Resources/Private/LogoSources/logo-default.ai','3afdfb46589113f79b2785c8f975cc8129b62c78','7f56593aef6e2abf463c89a9e30cf86470d354ea','ai','application/pdf','logo-default.ai','a822aecf1ed913aeb87c9145c6887d52ff7808fd',67861,1459572511,1459572511),(2,0,1459576444,0,0,0,'5',0,'/typo3conf/ext/templatebootstrap/Resources/Private/LogoSources/icon-default.ai','e1f2783a4240ca4bc3723b1296959ea5b9da12f7','7f56593aef6e2abf463c89a9e30cf86470d354ea','ai','application/pdf','icon-default.ai','0cee32cde7814ac2adea690895cfe85ddfed99e9',56376,1459572511,1459572511),(3,0,1459576444,0,0,0,'5',0,'/typo3conf/ext/templatebootstrap/Resources/Private/LogoSources/icon-larger.ai','82b3a02f1fdd204f554ee28654fd14b53d742926','7f56593aef6e2abf463c89a9e30cf86470d354ea','ai','application/pdf','icon-larger.ai','4b20df9abd7ab45dac7990dd9531aa7f2e78bb31',75857,1459572511,1459572511);
/*!40000 ALTER TABLE `sys_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_collection`
--

DROP TABLE IF EXISTS `sys_file_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_collection` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(30) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(11) NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumtext,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `starttime` int(11) NOT NULL DEFAULT '0',
  `endtime` int(11) NOT NULL DEFAULT '0',
  `title` tinytext,
  `description` text,
  `type` varchar(30) NOT NULL DEFAULT 'static',
  `files` int(11) NOT NULL DEFAULT '0',
  `storage` int(11) NOT NULL DEFAULT '0',
  `folder` text,
  `recursive` tinyint(4) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_collection`
--

LOCK TABLES `sys_file_collection` WRITE;
/*!40000 ALTER TABLE `sys_file_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_file_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_metadata`
--

DROP TABLE IF EXISTS `sys_file_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_metadata` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(11) NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob NOT NULL,
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(30) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `file` int(11) NOT NULL DEFAULT '0',
  `title` tinytext,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `description` text,
  `alternative` text,
  `categories` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `file` (`file`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `fal_filelist` (`l10n_parent`,`sys_language_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_metadata`
--

LOCK TABLES `sys_file_metadata` WRITE;
/*!40000 ALTER TABLE `sys_file_metadata` DISABLE KEYS */;
INSERT INTO `sys_file_metadata` VALUES (1,0,1459576444,1459576444,1,0,0,'',0,0,0,'',0,0,0,0,0,0,1,NULL,0,0,NULL,NULL,0),(2,0,1459576444,1459576444,1,0,0,'',0,0,0,'',0,0,0,0,0,0,2,NULL,0,0,NULL,NULL,0),(3,0,1459576444,1459576444,1,0,0,'',0,0,0,'',0,0,0,0,0,0,3,NULL,0,0,NULL,NULL,0);
/*!40000 ALTER TABLE `sys_file_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_processedfile`
--

DROP TABLE IF EXISTS `sys_file_processedfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_processedfile` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `storage` int(11) NOT NULL DEFAULT '0',
  `original` int(11) NOT NULL DEFAULT '0',
  `identifier` varchar(512) NOT NULL DEFAULT '',
  `name` tinytext,
  `configuration` text,
  `configurationsha1` char(40) NOT NULL DEFAULT '',
  `originalfilesha1` char(40) NOT NULL DEFAULT '',
  `task_type` varchar(200) NOT NULL DEFAULT '',
  `checksum` char(10) NOT NULL DEFAULT '',
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `combined_1` (`original`,`task_type`,`configurationsha1`),
  KEY `identifier` (`storage`,`identifier`(199))
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_processedfile`
--

LOCK TABLES `sys_file_processedfile` WRITE;
/*!40000 ALTER TABLE `sys_file_processedfile` DISABLE KEYS */;
INSERT INTO `sys_file_processedfile` VALUES (1,1459576444,1459576444,0,1,'',NULL,'a:11:{s:5:\"width\";N;s:6:\"height\";N;s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:600;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";N;s:5:\"frame\";i:0;s:4:\"crop\";s:7:\"0.1,0.1\";}','39088d3b4065023edf44efff4dca07707048dab2','a822aecf1ed913aeb87c9145c6887d52ff7808fd','Image.CropScaleMask','06fc452205',NULL,NULL),(2,1459576444,1459576444,0,2,'',NULL,'a:11:{s:5:\"width\";N;s:6:\"height\";N;s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:100;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";N;s:5:\"frame\";i:0;s:4:\"crop\";s:7:\"0.1,0.1\";}','4546054b0f805a534ebc10a0753b921a2fbbd107','0cee32cde7814ac2adea690895cfe85ddfed99e9','Image.CropScaleMask','3802446c24',NULL,NULL),(3,1459576444,1459576444,0,2,'',NULL,'a:11:{s:5:\"width\";s:3:\"72c\";s:6:\"height\";s:3:\"72c\";s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";s:41:\" -colorspace RGB -background transparent \";s:5:\"frame\";i:0;s:4:\"crop\";s:3:\"0,0\";}','f93a1d2fb80b93cfec162e5a4b7d6b68f9a23b40','0cee32cde7814ac2adea690895cfe85ddfed99e9','Image.CropScaleMask','7c73f434b9',NULL,NULL),(4,1459576444,1459576444,0,2,'',NULL,'a:11:{s:5:\"width\";s:4:\"114c\";s:6:\"height\";s:4:\"114c\";s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";s:41:\" -colorspace RGB -background transparent \";s:5:\"frame\";i:0;s:4:\"crop\";s:3:\"0,0\";}','100bc6cda873586544db7fac13a7e63b43276826','0cee32cde7814ac2adea690895cfe85ddfed99e9','Image.CropScaleMask','72658acf94',NULL,NULL),(5,1459576444,1459576444,0,2,'',NULL,'a:11:{s:5:\"width\";s:4:\"144c\";s:6:\"height\";s:4:\"144c\";s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";s:41:\" -colorspace RGB -background transparent \";s:5:\"frame\";i:0;s:4:\"crop\";s:3:\"0,0\";}','187f9cbf9f7153999bc465189cf0635614828551','0cee32cde7814ac2adea690895cfe85ddfed99e9','Image.CropScaleMask','b0ac54b3bc',NULL,NULL),(6,1459576444,1459576444,0,2,'',NULL,'a:11:{s:5:\"width\";s:4:\"180c\";s:6:\"height\";s:4:\"180c\";s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";s:41:\" -colorspace RGB -background transparent \";s:5:\"frame\";i:0;s:4:\"crop\";s:3:\"0,0\";}','72fad83328de89e0105a65714e490ef70069229f','0cee32cde7814ac2adea690895cfe85ddfed99e9','Image.CropScaleMask','5a3f904c89',NULL,NULL),(7,1459576444,1459576444,0,2,'',NULL,'a:11:{s:5:\"width\";s:3:\"32c\";s:6:\"height\";s:3:\"32c\";s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";s:41:\" -colorspace RGB -background transparent \";s:5:\"frame\";i:0;s:4:\"crop\";s:3:\"0,0\";}','41f21f6c8f18f261a57d8b7fca186a3f0e254557','0cee32cde7814ac2adea690895cfe85ddfed99e9','Image.CropScaleMask','0925b83804',NULL,NULL),(8,1459576444,1459576444,0,2,'',NULL,'a:11:{s:5:\"width\";s:3:\"96c\";s:6:\"height\";s:3:\"96c\";s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";s:41:\" -colorspace RGB -background transparent \";s:5:\"frame\";i:0;s:4:\"crop\";s:3:\"0,0\";}','4b42f54eabae0848c046d2b82a55130a4eae4746','0cee32cde7814ac2adea690895cfe85ddfed99e9','Image.CropScaleMask','a8785b3637',NULL,NULL),(9,1459576444,1459576444,0,2,'',NULL,'a:11:{s:5:\"width\";s:4:\"192c\";s:6:\"height\";s:4:\"192c\";s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";s:41:\" -colorspace RGB -background transparent \";s:5:\"frame\";i:0;s:4:\"crop\";s:3:\"0,0\";}','fe99e1425a9b178e9547a0fa485e7824e41a0169','0cee32cde7814ac2adea690895cfe85ddfed99e9','Image.CropScaleMask','3e29aadd68',NULL,NULL),(10,1459576445,1459576445,0,3,'',NULL,'a:11:{s:5:\"width\";s:4:\"256c\";s:6:\"height\";s:4:\"256c\";s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";s:41:\" -colorspace RGB -background transparent \";s:5:\"frame\";i:0;s:4:\"crop\";s:3:\"0,0\";}','f2ad5e673b41bd05f585d22027e4f734fd9f6845','4b20df9abd7ab45dac7990dd9531aa7f2e78bb31','Image.CropScaleMask','ef17ac603a',NULL,NULL),(11,1459576445,1459576445,0,2,'',NULL,'a:11:{s:5:\"width\";s:1:\"c\";s:6:\"height\";s:1:\"c\";s:13:\"fileExtension\";s:3:\"png\";s:8:\"maxWidth\";i:0;s:9:\"maxHeight\";i:0;s:8:\"minWidth\";i:0;s:9:\"minHeight\";i:0;s:7:\"noScale\";N;s:20:\"additionalParameters\";s:41:\" -colorspace RGB -background transparent \";s:5:\"frame\";i:0;s:4:\"crop\";s:3:\"0,0\";}','84c9c34bdb1379f973139eaf81c159f6bc1d7392','0cee32cde7814ac2adea690895cfe85ddfed99e9','Image.CropScaleMask','4be9a34b86',NULL,NULL);
/*!40000 ALTER TABLE `sys_file_processedfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_reference`
--

DROP TABLE IF EXISTS `sys_file_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_reference` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `sorting` int(10) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(30) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(11) NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob NOT NULL,
  `uid_local` int(11) NOT NULL DEFAULT '0',
  `uid_foreign` int(11) NOT NULL DEFAULT '0',
  `tablenames` varchar(64) NOT NULL DEFAULT '',
  `fieldname` varchar(64) NOT NULL DEFAULT '',
  `sorting_foreign` int(11) NOT NULL DEFAULT '0',
  `table_local` varchar(64) NOT NULL DEFAULT '',
  `title` tinytext,
  `description` text,
  `alternative` tinytext,
  `link` varchar(1024) NOT NULL DEFAULT '',
  `downloadname` tinytext,
  `crop` varchar(4000) NOT NULL DEFAULT '',
  `autoplay` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`),
  KEY `tablenames_fieldname` (`tablenames`(32),`fieldname`(12)),
  KEY `deleted` (`deleted`),
  KEY `uid_foreign` (`uid_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_reference`
--

LOCK TABLES `sys_file_reference` WRITE;
/*!40000 ALTER TABLE `sys_file_reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_file_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_storage`
--

DROP TABLE IF EXISTS `sys_file_storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_storage` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(30) NOT NULL DEFAULT '',
  `description` text,
  `driver` tinytext,
  `configuration` text,
  `is_default` tinyint(4) NOT NULL DEFAULT '0',
  `is_browsable` tinyint(4) NOT NULL DEFAULT '0',
  `is_public` tinyint(4) NOT NULL DEFAULT '0',
  `is_writable` tinyint(4) NOT NULL DEFAULT '0',
  `is_online` tinyint(4) NOT NULL DEFAULT '1',
  `auto_extract_metadata` tinyint(4) NOT NULL DEFAULT '1',
  `processingfolder` tinytext,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_storage`
--

LOCK TABLES `sys_file_storage` WRITE;
/*!40000 ALTER TABLE `sys_file_storage` DISABLE KEYS */;
INSERT INTO `sys_file_storage` VALUES (1,0,1459574662,1459574662,0,0,'fileadmin/ (auto-created)','This is the local fileadmin/ directory. This storage mount has been created automatically by TYPO3.','Local','<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?>\n<T3FlexForms>\n    <data>\n        <sheet index=\"sDEF\">\n            <language index=\"lDEF\">\n                <field index=\"basePath\">\n                    <value index=\"vDEF\">fileadmin/</value>\n                </field>\n                <field index=\"pathType\">\n                    <value index=\"vDEF\">relative</value>\n                </field>\n                <field index=\"caseSensitive\">\n                    <value index=\"vDEF\"></value>\n                </field>\n            </language>\n        </sheet>\n    </data>\n</T3FlexForms>',1,1,1,1,1,1,NULL);
/*!40000 ALTER TABLE `sys_file_storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_filemounts`
--

DROP TABLE IF EXISTS `sys_filemounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_filemounts` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(30) NOT NULL DEFAULT '',
  `description` varchar(2000) NOT NULL DEFAULT '',
  `path` varchar(120) NOT NULL DEFAULT '',
  `base` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `read_only` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_filemounts`
--

LOCK TABLES `sys_filemounts` WRITE;
/*!40000 ALTER TABLE `sys_filemounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_filemounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_history`
--

DROP TABLE IF EXISTS `sys_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_history` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_log_uid` int(11) NOT NULL DEFAULT '0',
  `history_data` mediumtext,
  `fieldlist` text,
  `recuid` int(11) NOT NULL DEFAULT '0',
  `tablename` varchar(255) NOT NULL DEFAULT '',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `history_files` mediumtext,
  `snapshot` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `recordident_1` (`tablename`,`recuid`),
  KEY `recordident_2` (`tablename`,`tstamp`),
  KEY `sys_log_uid` (`sys_log_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_history`
--

LOCK TABLES `sys_history` WRITE;
/*!40000 ALTER TABLE `sys_history` DISABLE KEYS */;
INSERT INTO `sys_history` VALUES (1,0,32,'a:2:{s:9:\"oldRecord\";a:2:{s:7:\"doktype\";s:1:\"4\";s:5:\"title\";s:0:\"\";}s:9:\"newRecord\";a:2:{s:7:\"doktype\";s:1:\"1\";s:5:\"title\";s:10:\"juli-an.ch\";}}','doktype,title',1,'pages',1459576095,NULL,0),(2,0,34,'a:2:{s:9:\"oldRecord\";a:1:{s:6:\"hidden\";s:1:\"1\";}s:9:\"newRecord\";a:1:{s:6:\"hidden\";i:0;}}','hidden',1,'pages',1459576146,NULL,0),(3,0,43,'a:2:{s:9:\"oldRecord\";a:1:{s:10:\"domainName\";s:17:\"www.juli-an.ch.lo\";}s:9:\"newRecord\";a:1:{s:10:\"domainName\";s:14:\"www.juli-an.ch\";}}','domainName',1,'sys_domain',1459576506,NULL,0);
/*!40000 ALTER TABLE `sys_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_language`
--

DROP TABLE IF EXISTS `sys_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_language` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `title` varchar(80) NOT NULL DEFAULT '',
  `flag` varchar(20) NOT NULL DEFAULT '',
  `language_isocode` varchar(2) NOT NULL DEFAULT '',
  `static_lang_isocode` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_language`
--

LOCK TABLES `sys_language` WRITE;
/*!40000 ALTER TABLE `sys_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_lockedrecords`
--

DROP TABLE IF EXISTS `sys_lockedrecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_lockedrecords` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `record_table` varchar(255) NOT NULL DEFAULT '',
  `record_uid` int(11) NOT NULL DEFAULT '0',
  `record_pid` int(11) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `feuserid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`,`tstamp`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_lockedrecords`
--

LOCK TABLES `sys_lockedrecords` WRITE;
/*!40000 ALTER TABLE `sys_lockedrecords` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_lockedrecords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_log`
--

DROP TABLE IF EXISTS `sys_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_log` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `action` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `recuid` int(11) unsigned NOT NULL DEFAULT '0',
  `tablename` varchar(255) NOT NULL DEFAULT '',
  `recpid` int(11) NOT NULL DEFAULT '0',
  `error` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `details` text NOT NULL,
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `details_nr` tinyint(3) NOT NULL DEFAULT '0',
  `IP` varchar(39) NOT NULL DEFAULT '',
  `log_data` text,
  `event_pid` int(11) NOT NULL DEFAULT '-1',
  `workspace` int(11) NOT NULL DEFAULT '0',
  `NEWid` varchar(30) NOT NULL DEFAULT '',
  `request_id` varchar(13) NOT NULL DEFAULT '',
  `time_micro` float NOT NULL DEFAULT '0',
  `component` varchar(255) NOT NULL DEFAULT '',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `message` text,
  `data` text,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `event` (`userid`,`event_pid`),
  KEY `recuidIdx` (`recuid`,`uid`),
  KEY `user_auth` (`type`,`action`,`tstamp`),
  KEY `request` (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_log`
--

LOCK TABLES `sys_log` WRITE;
/*!40000 ALTER TABLE `sys_log` DISABLE KEYS */;
INSERT INTO `sys_log` VALUES (1,0,1,1,0,'',0,0,'User %s logged in from %s (%s)',1459573884,255,1,'127.0.0.1','a:3:{i:0;s:5:\"admin\";i:1;s:9:\"127.0.0.1\";i:2;s:0:\"\";}',-1,-99,'','',0,'',0,NULL,NULL),(2,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459574023,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:5:\"pages\";}',-1,0,'','',0,'',0,NULL,NULL),(3,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459574025,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:3:\"all\";}',-1,0,'','',0,'',0,NULL,NULL),(4,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459574261,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:3:\"all\";}',-1,0,'','',0,'',0,NULL,NULL),(5,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459574262,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:5:\"pages\";}',-1,0,'','',0,'',0,NULL,NULL),(6,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574265,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(7,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574272,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(8,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574273,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(9,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574274,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(10,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574275,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(11,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574277,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(12,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574278,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(13,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574279,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(14,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574280,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(15,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574294,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(16,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/',1459574302,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(17,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/',1459574306,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(18,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/',1459574308,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(19,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574312,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(20,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574317,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(21,0,0,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574331,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(22,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1425389455: Invalid request for route \"/main\" | TYPO3\\CMS\\Backend\\Routing\\Exception\\RouteNotFoundException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\backend\\Classes\\Http\\RouteDispatcher.php in line 49. Requested URL: http://juli-an.ch.lo/typo3/index.php?route=%%2Fmain&token=6faede24a9f874f9ae1cc3878dc878e53740a7d8',1459574391,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(23,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/',1459574572,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(24,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\frontend\\Classes\\Controller\\TypoScriptFrontendController.php in line 1489. Requested URL: http://juli-an.ch.lo/typo3/',1459574635,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(25,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1411840171: The class \"Staempfli\\TemplateBootstrap\\Rendering\\SoundCloudRenderer\" you are trying to register is not available | InvalidArgumentException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\core\\Classes\\Resource\\Rendering\\RendererRegistry.php in line 58. Requested URL: http://juli-an.ch.lo/typo3/index.php?M=tools_ExtensionmanagerExtensionmanager&moduleToken=93b09444abef279dab4ce352f7652cb794da63dc&tx_extensionmanager_tools_extensionmanagerextensionmanager%%5BextensionKey%%5D=templatebootstrap&tx_extensionmanager_tools_extensionmanagerextensionmanager%%5Baction%%5D=toggleExtensionInstallationState&tx_extensionmanager_tools_extensionmanagerextensionmanager%%5Bcontroller%%5D=Action',1459574949,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(26,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1453732574: RealURL was not able to find the root page id for the domain \"juli-an.ch.lo\" | Exception thrown in file D:\\Daten\\Projekte\\julian\\public_html\\typo3conf\\ext\\realurl\\Classes\\EncodeDecoderBase.php in line 213. Requested URL: http://juli-an.ch.lo/',1459575508,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(27,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1453732574: RealURL was not able to find the root page id for the domain \"juli-an.ch.lo\" | Exception thrown in file D:\\Daten\\Projekte\\julian\\public_html\\typo3conf\\ext\\realurl\\Classes\\EncodeDecoderBase.php in line 213. Requested URL: http://juli-an.ch.lo/index.php?id=0&ADMCMD_editIcons=1',1459575662,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(28,0,1,1,1,'pages',0,0,'Record \'%s\' (%s) was inserted on page \'%s\' (%s)',1459576031,1,10,'127.0.0.1','a:4:{i:0;s:10:\"[No title]\";i:1;s:7:\"pages:1\";i:2;s:12:\"[root-level]\";i:3;s:1:\"0\";}',0,0,'NEW56ff5cd086632813168804','',0,'',0,NULL,NULL),(29,0,1,2,1,'pages',0,2,'SQL error: \'%s\' (%s)',1459576031,1,12,'127.0.0.1','a:2:{i:0;s:69:\"Unknown column \'tx_templatebootstrap_backgroundimage\' in \'field list\'\";i:1;s:7:\"pages:1\";}',-1,0,'','',0,'',0,NULL,NULL),(30,0,1,2,1,'pages',0,2,'SQL error: \'%s\' (%s)',1459576031,1,12,'127.0.0.1','a:2:{i:0;s:63:\"Unknown column \'tx_templatebootstrap_moodimage\' in \'field list\'\";i:1;s:7:\"pages:1\";}',-1,0,'','',0,'',0,NULL,NULL),(31,0,1,2,1,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1459576031,1,10,'127.0.0.1','a:2:{i:0;s:10:\"[No title]\";i:1;s:7:\"pages:1\";}',1,0,'','',0,'',0,NULL,NULL),(32,0,1,2,1,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1459576096,1,10,'127.0.0.1','a:2:{i:0;s:10:\"juli-an.ch\";i:1;s:7:\"pages:1\";}',1,0,'','',0,'',0,NULL,NULL),(33,0,1,2,1,'pages',0,2,'SQL error: \'%s\' (%s)',1459576102,1,12,'127.0.0.1','a:2:{i:0;s:69:\"Unknown column \'tx_templatebootstrap_backgroundimage\' in \'field list\'\";i:1;s:7:\"pages:1\";}',-1,0,'','',0,'',0,NULL,NULL),(34,0,1,2,1,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1459576146,1,10,'127.0.0.1','a:2:{i:0;s:10:\"juli-an.ch\";i:1;s:7:\"pages:1\";}',1,0,'','',0,'',0,NULL,NULL),(35,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459576192,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:5:\"pages\";}',-1,0,'','',0,'',0,NULL,NULL),(36,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459576193,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:3:\"all\";}',-1,0,'','',0,'',0,NULL,NULL),(37,0,1,1,1,'sys_domain',0,0,'Record \'%s\' (%s) was inserted on page \'%s\' (%s)',1459576255,1,10,'127.0.0.1','a:4:{i:0;s:17:\"www.juli-an.ch.lo\";i:1;s:12:\"sys_domain:1\";i:2;s:10:\"juli-an.ch\";i:3;s:1:\"1\";}',1,0,'NEW56ff5da17df91954637991','',0,'',0,NULL,NULL),(38,0,1,0,0,'',0,1,'Core: Error handler (BE): PHP Warning: DOMDocument::load(): I/O warning : failed to load external entity &quot;D:/Daten/Projekte/julian/public_html/typo3/sysext/t3editor/Resources/Private/tsref.xml&quot; in D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\t3editor\\Classes\\TypoScriptReferenceLoader.php line 81',1459576287,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(39,0,1,0,0,'',0,1,'Core: Error handler (BE): PHP Warning: DOMDocument::load(): I/O warning : failed to load external entity &quot;D:/Daten/Projekte/julian/public_html/typo3/sysext/t3editor/Resources/Private/tsref.xml&quot; in D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\t3editor\\Classes\\TypoScriptReferenceLoader.php line 81',1459576288,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(40,0,1,1,1,'sys_template',0,0,'Record \'%s\' (%s) was inserted on page \'%s\' (%s)',1459576424,1,10,'127.0.0.1','a:4:{i:0;s:18:\"Julian Artist Page\";i:1;s:14:\"sys_template:1\";i:2;s:10:\"juli-an.ch\";i:3;s:1:\"1\";}',1,0,'NEW56ff5ddec3223693973827','',0,'',0,NULL,NULL),(41,0,1,0,0,'',0,1,'Core: Error handler (BE): PHP Warning: DOMDocument::load(): I/O warning : failed to load external entity &quot;D:/Daten/Projekte/julian/public_html/typo3/sysext/t3editor/Resources/Private/tsref.xml&quot; in D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\t3editor\\Classes\\TypoScriptReferenceLoader.php line 81',1459576425,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(42,0,1,0,0,'',0,1,'Core: Error handler (BE): PHP Warning: DOMDocument::load(): I/O warning : failed to load external entity &quot;D:/Daten/Projekte/julian/public_html/typo3/sysext/t3editor/Resources/Private/tsref.xml&quot; in D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\t3editor\\Classes\\TypoScriptReferenceLoader.php line 81',1459576426,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(43,0,1,2,1,'sys_domain',0,0,'Record \'%s\' (%s) was updated. (Online).',1459576507,1,10,'127.0.0.1','a:2:{i:0;s:14:\"www.juli-an.ch\";i:1;s:12:\"sys_domain:1\";}',1,0,'','',0,'',0,NULL,NULL),(44,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459576516,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:5:\"pages\";}',-1,0,'','',0,'',0,NULL,NULL),(45,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459576517,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:3:\"all\";}',-1,0,'','',0,'',0,NULL,NULL),(46,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459577614,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:5:\"pages\";}',-1,0,'','',0,'',0,NULL,NULL),(47,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459577615,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:3:\"all\";}',-1,0,'','',0,'',0,NULL,NULL),(48,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1245673367: The given class \"Julian\\TemplateBootstrap\\Utility\\PostInstall\\PostInstallFileHandler\" is not a registered object. | TYPO3\\CMS\\Extbase\\SignalSlot\\Exception\\InvalidSlotException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\extbase\\Classes\\SignalSlot\\Dispatcher.php in line 126. Requested URL: http://juli-an.ch.lo/typo3/index.php?M=tools_ExtensionmanagerExtensionmanager&moduleToken=93b09444abef279dab4ce352f7652cb794da63dc&tx_extensionmanager_tools_extensionmanagerextensionmanager%%5Baction%%5D=save&tx_extensionmanager_tools_extensionmanagerextensionmanager%%5Bcontroller%%5D=Configuration',1459577881,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(49,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459578242,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:5:\"pages\";}',-1,0,'','',0,'',0,NULL,NULL),(50,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459578243,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:3:\"all\";}',-1,0,'','',0,'',0,NULL,NULL),(51,0,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1245673367: The given class \"Julian\\TemplateBootstrap\\Utility\\PostInstall\\PostInstallFileHandler\" is not a registered object. | TYPO3\\CMS\\Extbase\\SignalSlot\\Exception\\InvalidSlotException thrown in file D:\\Daten\\Projekte\\julian\\packages\\typo3\\cms\\typo3\\sysext\\extbase\\Classes\\SignalSlot\\Dispatcher.php in line 126. Requested URL: http://juli-an.ch.lo/typo3/index.php?M=tools_ExtensionmanagerExtensionmanager&moduleToken=93b09444abef279dab4ce352f7652cb794da63dc&tx_extensionmanager_tools_extensionmanagerextensionmanager%%5Baction%%5D=save&tx_extensionmanager_tools_extensionmanagerextensionmanager%%5Bcontroller%%5D=Configuration',1459578294,5,0,'127.0.0.1',NULL,-1,0,'','',0,'',0,NULL,NULL),(52,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459578463,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:3:\"all\";}',-1,0,'','',0,'',0,NULL,NULL),(53,0,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',1459578465,3,0,'127.0.0.1','a:2:{i:0;s:5:\"admin\";i:1;s:5:\"pages\";}',-1,0,'','',0,'',0,NULL,NULL);
/*!40000 ALTER TABLE `sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_news`
--

DROP TABLE IF EXISTS `sys_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_news` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` mediumtext,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_news`
--

LOCK TABLES `sys_news` WRITE;
/*!40000 ALTER TABLE `sys_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_note`
--

DROP TABLE IF EXISTS `sys_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_note` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser` int(11) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text,
  `personal` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `category` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_note`
--

LOCK TABLES `sys_note` WRITE;
/*!40000 ALTER TABLE `sys_note` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_refindex`
--

DROP TABLE IF EXISTS `sys_refindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_refindex` (
  `hash` varchar(32) NOT NULL DEFAULT '',
  `tablename` varchar(255) NOT NULL DEFAULT '',
  `recuid` int(11) NOT NULL DEFAULT '0',
  `field` varchar(40) NOT NULL DEFAULT '',
  `flexpointer` varchar(255) NOT NULL DEFAULT '',
  `softref_key` varchar(30) NOT NULL DEFAULT '',
  `softref_id` varchar(40) NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `workspace` int(11) NOT NULL DEFAULT '0',
  `ref_table` varchar(255) NOT NULL DEFAULT '',
  `ref_uid` int(11) NOT NULL DEFAULT '0',
  `ref_string` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`hash`),
  KEY `lookup_rec` (`tablename`,`recuid`),
  KEY `lookup_uid` (`ref_table`,`ref_uid`),
  KEY `lookup_string` (`ref_string`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_refindex`
--

LOCK TABLES `sys_refindex` WRITE;
/*!40000 ALTER TABLE `sys_refindex` DISABLE KEYS */;
INSERT INTO `sys_refindex` VALUES ('39f6a0ac7110d9a4bf28c5b5f8a48abe','sys_domain',1,'domainName','','substitute','0',-1,0,0,'_STRING',0,'www.juli-an.ch');
/*!40000 ALTER TABLE `sys_refindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_registry`
--

DROP TABLE IF EXISTS `sys_registry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_registry` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entry_namespace` varchar(128) NOT NULL DEFAULT '',
  `entry_key` varchar(128) NOT NULL DEFAULT '',
  `entry_value` blob,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `entry_identifier` (`entry_namespace`,`entry_key`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_registry`
--

LOCK TABLES `sys_registry` WRITE;
/*!40000 ALTER TABLE `sys_registry` DISABLE KEYS */;
INSERT INTO `sys_registry` VALUES (1,'core','formProtectionSessionToken:1','s:64:\"9f5d8bad0943ed6afa5de8c947127258860df590f24c0a8b72b96b9dbc88f562\";'),(2,'extensionDataImport','typo3conf/ext/realurl/ext_tables_static+adt.sql','i:1;'),(3,'extensionDataImport','typo3conf/ext/templatebootstrap/ext_tables_static+adt.sql','i:1;');
/*!40000 ALTER TABLE `sys_registry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_template`
--

DROP TABLE IF EXISTS `sys_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_template` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `sitetitle` varchar(255) NOT NULL DEFAULT '',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `root` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `clear` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `include_static_file` text,
  `constants` text,
  `config` text,
  `nextLevel` varchar(5) NOT NULL DEFAULT '',
  `description` text,
  `basedOn` tinytext,
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `includeStaticAfterBasedOn` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `static_file_mode` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `parent` (`pid`,`deleted`,`hidden`,`sorting`),
  KEY `roottemplate` (`deleted`,`hidden`,`root`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_template`
--

LOCK TABLES `sys_template` WRITE;
/*!40000 ALTER TABLE `sys_template` DISABLE KEYS */;
INSERT INTO `sys_template` VALUES (1,1,0,0,0,'',0,0,0,0,0,0,1459576424,256,1459576424,1,'Julian Artist Page','',0,0,0,1,0,'EXT:templatebootstrap/Configuration/TypoScript','\r\n','\r\n','','','',0,0,0,0);
/*!40000 ALTER TABLE `sys_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tt_content`
--

DROP TABLE IF EXISTS `tt_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tt_content` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `editlock` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `CType` varchar(255) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT '',
  `rowDescription` text,
  `bodytext` mediumtext,
  `image` int(11) unsigned NOT NULL DEFAULT '0',
  `imagewidth` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `imageorient` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `imagecols` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `imageborder` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `media` int(11) unsigned NOT NULL DEFAULT '0',
  `layout` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `cols` int(11) unsigned NOT NULL DEFAULT '0',
  `records` text,
  `pages` text,
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `colPos` int(11) unsigned NOT NULL DEFAULT '0',
  `subheader` varchar(255) NOT NULL DEFAULT '',
  `fe_group` varchar(100) NOT NULL DEFAULT '0',
  `header_link` varchar(1024) NOT NULL DEFAULT '',
  `image_zoom` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `header_layout` varchar(30) NOT NULL DEFAULT '0',
  `menu_type` varchar(30) NOT NULL DEFAULT '0',
  `list_type` varchar(255) NOT NULL DEFAULT '',
  `select_key` varchar(80) NOT NULL DEFAULT '',
  `sectionIndex` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `linkToTop` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `file_collections` text,
  `filelink_size` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `filelink_sorting` varchar(10) NOT NULL DEFAULT '',
  `target` varchar(30) NOT NULL DEFAULT '',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `recursive` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `imageheight` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  `pi_flexform` mediumtext,
  `accessibility_title` varchar(30) NOT NULL DEFAULT '',
  `accessibility_bypass` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `accessibility_bypass_text` varchar(30) NOT NULL DEFAULT '',
  `l18n_parent` int(11) NOT NULL DEFAULT '0',
  `l18n_diffsource` mediumblob,
  `selected_categories` text,
  `category_field` varchar(64) NOT NULL DEFAULT '',
  `table_caption` varchar(255) DEFAULT NULL,
  `table_delimiter` smallint(6) unsigned NOT NULL DEFAULT '0',
  `table_enclosure` smallint(6) unsigned NOT NULL DEFAULT '0',
  `table_header_position` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `table_tfoot` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `header_position` varchar(6) NOT NULL DEFAULT '',
  `image_compression` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `image_effects` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `image_noRows` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `section_frame` int(11) unsigned NOT NULL DEFAULT '0',
  `spaceAfter` smallint(5) unsigned NOT NULL DEFAULT '0',
  `spaceBefore` smallint(5) unsigned NOT NULL DEFAULT '0',
  `table_bgColor` int(11) unsigned NOT NULL DEFAULT '0',
  `table_border` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `table_cellpadding` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `table_cellspacing` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `bullets_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `uploads_description` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `uploads_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `assets` int(11) unsigned NOT NULL DEFAULT '0',
  `categories` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `parent` (`pid`,`sorting`),
  KEY `language` (`l18n_parent`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tt_content`
--

LOCK TABLES `tt_content` WRITE;
/*!40000 ALTER TABLE `tt_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `tt_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_extensionmanager_domain_model_extension`
--

DROP TABLE IF EXISTS `tx_extensionmanager_domain_model_extension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_extensionmanager_domain_model_extension` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `extension_key` varchar(60) NOT NULL DEFAULT '',
  `repository` int(11) unsigned NOT NULL DEFAULT '1',
  `version` varchar(15) NOT NULL DEFAULT '',
  `alldownloadcounter` int(11) unsigned NOT NULL DEFAULT '0',
  `downloadcounter` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `description` mediumtext,
  `state` int(4) NOT NULL DEFAULT '0',
  `review_state` int(4) NOT NULL DEFAULT '0',
  `category` int(4) NOT NULL DEFAULT '0',
  `last_updated` int(11) unsigned NOT NULL DEFAULT '0',
  `serialized_dependencies` mediumtext,
  `author_name` varchar(255) NOT NULL DEFAULT '',
  `author_email` varchar(255) NOT NULL DEFAULT '',
  `ownerusername` varchar(50) NOT NULL DEFAULT '',
  `md5hash` varchar(35) NOT NULL DEFAULT '',
  `update_comment` mediumtext,
  `authorcompany` varchar(255) NOT NULL DEFAULT '',
  `integer_version` int(11) NOT NULL DEFAULT '0',
  `current_version` int(3) NOT NULL DEFAULT '0',
  `lastreviewedversion` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `versionextrepo` (`extension_key`,`version`,`repository`),
  KEY `index_extrepo` (`extension_key`,`repository`),
  KEY `index_versionrepo` (`integer_version`,`repository`),
  KEY `index_currentversions` (`current_version`,`review_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_extensionmanager_domain_model_extension`
--

LOCK TABLES `tx_extensionmanager_domain_model_extension` WRITE;
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_extension` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_extension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_extensionmanager_domain_model_repository`
--

DROP TABLE IF EXISTS `tx_extensionmanager_domain_model_repository`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_extensionmanager_domain_model_repository` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `description` mediumtext,
  `wsdl_url` varchar(100) NOT NULL DEFAULT '',
  `mirror_list_url` varchar(100) NOT NULL DEFAULT '',
  `last_update` int(11) unsigned NOT NULL DEFAULT '0',
  `extension_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_extensionmanager_domain_model_repository`
--

LOCK TABLES `tx_extensionmanager_domain_model_repository` WRITE;
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_repository` DISABLE KEYS */;
INSERT INTO `tx_extensionmanager_domain_model_repository` VALUES (1,0,'TYPO3.org Main Repository','Main repository on typo3.org. This repository has some mirrors configured which are available with the mirror url.','https://typo3.org/wsdl/tx_ter_wsdl.php','https://repositories.typo3.org/mirrors.xml.gz',1346191200,0);
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_repository` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_impexp_presets`
--

DROP TABLE IF EXISTS `tx_impexp_presets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_impexp_presets` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `user_uid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `public` tinyint(3) NOT NULL DEFAULT '0',
  `item_uid` int(11) NOT NULL DEFAULT '0',
  `preset_data` blob,
  PRIMARY KEY (`uid`),
  KEY `lookup` (`item_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_impexp_presets`
--

LOCK TABLES `tx_impexp_presets` WRITE;
/*!40000 ALTER TABLE `tx_impexp_presets` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_impexp_presets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_realurl_pathcache`
--

DROP TABLE IF EXISTS `tx_realurl_pathcache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_realurl_pathcache` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `rootpage_id` int(11) NOT NULL DEFAULT '0',
  `mpvar` tinytext,
  `pagepath` text,
  `expire` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `pathq1` (`rootpage_id`,`pagepath`(32),`expire`),
  KEY `pathq2` (`page_id`,`language_id`,`rootpage_id`,`expire`),
  KEY `expire` (`expire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_realurl_pathcache`
--

LOCK TABLES `tx_realurl_pathcache` WRITE;
/*!40000 ALTER TABLE `tx_realurl_pathcache` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_realurl_pathcache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_realurl_uniqalias`
--

DROP TABLE IF EXISTS `tx_realurl_uniqalias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_realurl_uniqalias` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `tablename` varchar(255) NOT NULL DEFAULT '',
  `field_alias` varchar(255) NOT NULL DEFAULT '',
  `field_id` varchar(60) NOT NULL DEFAULT '',
  `value_alias` varchar(255) NOT NULL DEFAULT '',
  `value_id` int(11) NOT NULL DEFAULT '0',
  `lang` int(11) NOT NULL DEFAULT '0',
  `expire` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `tablename` (`tablename`),
  KEY `bk_realurl01` (`field_alias`(20),`field_id`,`value_id`,`lang`,`expire`),
  KEY `bk_realurl02` (`tablename`(32),`field_alias`(20),`field_id`,`value_alias`(20),`expire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_realurl_uniqalias`
--

LOCK TABLES `tx_realurl_uniqalias` WRITE;
/*!40000 ALTER TABLE `tx_realurl_uniqalias` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_realurl_uniqalias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_realurl_uniqalias_cache_map`
--

DROP TABLE IF EXISTS `tx_realurl_uniqalias_cache_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_realurl_uniqalias_cache_map` (
  `alias_uid` int(11) NOT NULL DEFAULT '0',
  `url_cache_id` varchar(255) NOT NULL DEFAULT '',
  KEY `check_existence` (`alias_uid`,`url_cache_id`(6))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_realurl_uniqalias_cache_map`
--

LOCK TABLES `tx_realurl_uniqalias_cache_map` WRITE;
/*!40000 ALTER TABLE `tx_realurl_uniqalias_cache_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_realurl_uniqalias_cache_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_realurl_urlcache`
--

DROP TABLE IF EXISTS `tx_realurl_urlcache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_realurl_urlcache` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `rootpage_id` int(11) NOT NULL DEFAULT '0',
  `original_url` text,
  `speaking_url` text,
  `request_variables` text,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `pathq1` (`rootpage_id`,`original_url`(32)),
  KEY `pathq2` (`rootpage_id`,`speaking_url`(32))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_realurl_urlcache`
--

LOCK TABLES `tx_realurl_urlcache` WRITE;
/*!40000 ALTER TABLE `tx_realurl_urlcache` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_realurl_urlcache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_rsaauth_keys`
--

DROP TABLE IF EXISTS `tx_rsaauth_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_rsaauth_keys` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `key_value` text,
  PRIMARY KEY (`uid`),
  KEY `crdate` (`crdate`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_rsaauth_keys`
--

LOCK TABLES `tx_rsaauth_keys` WRITE;
/*!40000 ALTER TABLE `tx_rsaauth_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_rsaauth_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_rtehtmlarea_acronym`
--

DROP TABLE IF EXISTS `tx_rtehtmlarea_acronym`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_rtehtmlarea_acronym` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `term` varchar(255) NOT NULL DEFAULT '',
  `acronym` varchar(255) NOT NULL DEFAULT '',
  `static_lang_isocode` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_rtehtmlarea_acronym`
--

LOCK TABLES `tx_rtehtmlarea_acronym` WRITE;
/*!40000 ALTER TABLE `tx_rtehtmlarea_acronym` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_rtehtmlarea_acronym` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-02  8:38:40
